\section{Numerical examples}\label{sec:examples}
%\section{Application: Sub-optimal peak power minimization by penalized relaxation}
This section illustrates the theoretical developments of Section \ref{sec:minSDR} by means of numerical experiments. Specifically, we solve a problem introduced in \cite{heidari_optimization_2009}, showing that our approach reaches the optimum solution for the in-phase settings, but also provides a better design for the out-of-phase scenario. Our second example illustrates the applicability of the method to larger problems. The final example illustrates the influence of multiple harmonics on the design.

All presented numerical examples have been implemented in a Python code available at \cite{TODO}, with the optimization programs modeled in \textbf{PICOS} \cite{sagnol_picos_2022}, a Python interface that calls the convex optimization tools \textbf{CVXOPT} \cite{andersen_cvxopt_2020} and the optimizer \MTc{Mosek?}. The optimization problems were solved on a standard laptop, equipped with the 6 processors AMD Ryzen 5 4500U and $16$ GB of RAM.

%\MTc{From my point of view, this section should exploit the results of Section 2 and solve some ``real'' problems. If there is something novel in theory, I would move it to Section 2. Otherwise, I would structure it (structural) problem-wise, not (theoretical) result-wise. E.g., I would consider 3.1 to be devoted to the problem from Heideri et al, and thus describe that problem (figure of boundary conditions, parameters) at the very beginning of that section. The more specific settings such as examples of the matrices involved would be described in individual subsections: 3.1.1 would be reproducing the in-phase setting, 3.1.2 can be without the in-phase assumption. Also, I think there is no need to explain some theoretical things again--it would suffice to do a reference to equations etc.}
%\MTc{Why not use $L(a)$ as you did before? If the reason is $\omega$ (or $\lambda$?), I would just say that we add the subscript $\omega$ to highlight the dependence on the actual $\omega$ or $\lambda$, i.e., $L_\omega (a )$.}
Before the examples, let us summarize the theoretical results presented before to construct the peak power minimization under single frequency load, with or without in phase assumption. For the following discussion, we will use the shorthand notation $K_\lambda(a)=-\lambda^2M(a)+K(a)$. The construction of minimization problem for the multiple frequencies loads situations is reported in the appendix \ref{appendix:peak_power_minimization_multi}.

By the semidefinite certificate, we have shown that the peak power minimization is equivalent to the following problem:
\begin{equation}
	\begin{split}
		\underset{a,v_R,v_I,\theta,Q^0,Q^1}{\min} & \theta\\
		\text{ s.t. } & \left\{\begin{array}{l}
			a_i\geq 0, q^Ta\leq m, (-\omega^2M(a)+K(a))\succeq 0\\
			K_\omega(a)v_R=\omega f_I\\
			K_\omega(a)v_I=-\omega f_R\\
			P_j(\theta,v_R,v_I,Q^j)\succeq 0,\forall j=0,1\\
		\end{array}\right.
	\end{split}
\end{equation}

Where $P_j$ are:
\begin{equation}
	\begin{split}
		j=0:&\begin{pmatrix}
			\theta -q^0_{11}-q^0_{22}& -q^0_{12} & \frac{1}{4}((f_R^Tv_R-f_I^Tv_I)-i(f_R^Tv_I+f_I^Tv_R))\\
			-\overline{q^0_{12}} & q^0_{11} & q^0_{12}\\
			\frac{1}{4}((f_R^Tv_R-f_I^Tv_I)+i(f_R^Tv_I+f_I^Tv_R)) &\overline{q^0_{12}} & q^0_{22}
		\end{pmatrix}\succeq 0\\
		j=1:&\begin{pmatrix}
			\theta -q^1_{11}-q^1_{22}& -q^1_{12} & -\frac{1}{4}((f_R^Tv_R-f_I^Tv_I)-i(f_R^Tv_I+f_I^Tv_R))\\
			-\overline{q^1_{12}} & q^1_{11} & q^1_{12}\\
			-\frac{1}{4}((f_R^Tv_R-f_I^Tv_I)+i(f_R^Tv_I+f_I^Tv_R)) &\overline{q^1_{12}} & q^1_{22}
		\end{pmatrix}\succeq 0\\
	\end{split}
\end{equation}
By the independence of peak power of the non-physical part of the solution, recall Appendix~\ref{appendix:B}, we can replace $v_R$ and $v_I$ by the use of pseudo-inverse of $K_\omega(a)$. Next, we introduce a two-column matrix $F=(\omega f_I ~ -\omega f_R)$ and real $X\in\mathbb{S}^2$:
\begin{equation}\label{eqn:compliance_matrix}
	X=F^TK_\omega(a)^\dagger F=\omega^2\begin{pmatrix}f_I^TK_\omega(a)^\dagger f_I & -f_IK_\omega(a)^\dagger f_R \\ 
	-f_R^TK_\omega(a)^\dagger f_I & f_R^TK_\omega(a)^\dagger f_R
	\end{pmatrix}.
\end{equation}
Then, the LMI $P_j$ becomes:
\begin{equation}
	\begin{split}
		j=0:&\begin{pmatrix}
			\theta -q^0_{11}-q^0_{22}& -q^0_{12} & \frac{1}{4\omega}((X_{11}-X_{00})+i(X_{01}+X_{10}))\\
			-\overline{q^0_{12}} & q^0_{11} & q^0_{12}\\
			\frac{1}{4\omega}((X_{11}-X_{00})-i(X_{01}+X_{10})) &\overline{q^0_{12}} & q^0_{22}
		\end{pmatrix}\succeq 0\\
		j=1:&\begin{pmatrix}
			\theta -q^1_{11}-q^1_{22}& -q^1_{12} & -\frac{1}{4\omega}((X_{11}-X_{00})+i(X_{01}+X_{10}))\\
			-\overline{q^1_{12}} & q^1_{11} & q^1_{12}\\
			-\frac{1}{4\omega}((X_{11}-X_{00})-i(X_{01}+X_{10})) &\overline{q^1_{12}} & q^1_{22}
		\end{pmatrix}\succeq 0\\
	\end{split}
\end{equation}
We have thus the following equivalent problem to the peak power minimization:
\begin{equation}
	\begin{split}
		\underset{a,\theta,Q^0,Q^1,X}{\min} & \theta\\
		\text{ s.t. } & \left\{\begin{array}{l}
			a_i\geq 0, q^Ta\leq m, (-\omega^2M(a)+K(a))\succeq 0\\
			f_I,f_R\in\range{K_\omega(a)}\\
			X=F^TK_\omega(a)^\dagger F\\
			P_j(\theta,X,Q^j)\succeq 0,\forall j=0,1\\
		\end{array}\right.
	\end{split}
\end{equation}
We write the equivalent reformulation with trace equality before giving the convex relaxation
\begin{equation}\label{eqn:relax_single_harmonic}
	\begin{split}
		\underset{a,\theta,Q^0,Q^1,X}{\min} & \theta\\
		\text{ s.t. } & \left\{\begin{array}{l}
			a_i\geq 0, q^Ta\leq m\\
			\begin{pmatrix}
				X & F^T\\
				F & K_\omega(a)
			\end{pmatrix}\succeq 0\\
            \tr\{X-F^TL(a)^\dagger F\}=0\\
			P_j(\theta,X,Q^j)\succeq 0,\forall j=0,1\\
		\end{array}\right.
	\end{split}
\end{equation}
Now we consider the relaxation proposed in \ref{sec:conv_relax},
\begin{equation}\label{eqn:relax_single_harmonic2}
	\begin{split}
		\underset{a,\theta,Q^0,Q^1,X}{\min} & \theta\\
		\text{ s.t. } & \left\{\begin{array}{l}
			a_i\geq 0, q^Ta\leq m\\
			\begin{pmatrix}
				X & F^T\\
				F & K_\omega(a)
			\end{pmatrix}\succeq 0\\
			P_j(\theta,X,Q^j)\succeq 0,\forall j=0,1\\
		\end{array}\right.
	\end{split}
\end{equation}
And the penalization method consists of solving
\begin{equation}\label{eqn:pen_relax_single_harmonic}
	\begin{split}
		\underset{a,\theta,Q^0,Q^1,X}{\min} ~&~ \theta+\eta \tr{X}\\
		\text{ s.t. } & \left\{\begin{array}{l}
			a_i\geq 0, q^Ta\leq m\\
			\begin{pmatrix}
				X & F^T\\
				F & K_\omega(a)
			\end{pmatrix}\succeq 0\\
			P_j(\theta,X,Q^j)\succeq 0,\forall j=0,1\\
		\end{array}\right.
	\end{split}
\end{equation}
for $\eta\geq 0$.
\subsection{$21$-element problem by Heidari~\emph{et al.}}\label{sec:examples_heidari}
As the first illustration, we consider the problem introduced in \cite{heidari_optimization_2009}: a ground structure containing $21$ finite elements and $12$ nodes, see Fig.~\ref{fig:heidari_bc}. In this problem, all horizontal and vertical bars share the length of $1$, whereas the diagonal ones are $\sqrt{2}$ long. All elements are made of the same material, with the Young modulus $E=25,000$ and the density $\rho=1.0$. Furthermore, we bound the total structural weight from above by $m=1$.

\begin{figure}[!htbp]
\centering
\includegraphics{data/heidari_bc}
\caption{Boundary conditions and ground structure of the $21$-element truss structure introduced in \cite{heidari_optimization_2009}.}\label{fig:heidari_bc}
\end{figure}

Kinematic boundary conditions consist of fixed supports at the top nodes, preventing their horizontal and vertical movements. For the dynamic boundary conditions, we consider loads acting at the bottom nodes, with the horizontal forces (denoted in green color in Fig.~\ref{fig:heidari_bc}) $f_I(t) = \frac{1}{2} \sin(\omega t)$ and the vertical forces (drawn in orange in Fig.~\ref{fig:heidari_bc}) being $f_R(t)=\frac{1}{2} \cos(\omega t)$. For both these loading functions, we assume the same angular frequency $\omega=15$. The time-dependent force magnitudes appear visualized in Fig.~\ref{fig:powers}a. Notice that the resultant of the components $f_R$ and $f_I$ is always a force of magnitude $1/2$, modeling thus an unbalanced rotating load with the period of $T=\frac{2\pi}{15}\approx0.42$.

\begin{figure}[!htbp]
\centering
\includegraphics[width=\linewidth]{data/heidari_powers}
\caption{(a) Time-varying loads $f_R(t)$ and $f_I(t)$ and the powers associated with the (b) optimal designs under in-phase loads, and (c) the same designs under out-of-phase loads. Figure (d) shows the power of a design optimized for out-of-phase loads.}
\label{fig:powers}
\end{figure}

\subsubsection{In-phase loads}
First, we optimize the peak power of the loads $f_R(t)$ and $f_I(t)$ independently. For this setting, the relaxation with penalty \eqref{eqn:lag_relax_drop} is exact, as was shown in \cite{heidari_optimization_2009}. Thus, globally-optimal solutions can be found by solving a single SDP problem, see Figs.~\ref{fig:in_phase} and \ref{fig:in_phase2} for their topology and Fig.~\ref{fig:powers}b for the optimal time-varying powers $p_R(t)$ and $p_I(t)$. The corresponding optimal peak powers are $0.0036$ and $0.0241$, respectively.

\begin{figure}[!htbp]
\centering
\subfigure[]{
         \includegraphics[height=4cm]{data/heideri_inphase}
        \label{fig:in_phase}
    }%
\hfill\subfigure[]{
         \includegraphics[height=4cm]{data/heideri_inphase2}
        \label{fig:in_phase2}
    }%
\hfill\subfigure[]{
         \includegraphics[height=4cm]{data/heidari_outphase2}
        \label{fig:out_phase2}
    }
\caption{Optimized topology for the (a) in-phase loads $f_R$, (b) in-phase loads $f_I$, and (c) out-of-phase loads $f_R$ and $f_I$ acting according to Fig.~\ref{fig:heidari_bc}.}
\end{figure}

\subsubsection{Not-in-phase loads}
Next, we consider the setting of the loads $f_I(t)$ and $f_R(t)$ acting concurrently. The first, naive option, would be to investigate the performance of the structures designed for the in-phase loads $f_R(t)$ or $f_I(t)$ only. Not surprisingly, however, the performance of these designs is far from optimal because they fail to suppress the power by exploiting the interaction between the loads, see Fig.~\ref{fig:powers}c. \MTrev{}{A Similar disadvantage also appears for the worst-case peak power minimization formulation by Heidari \textit{et al.} \cite{heidari_optimization_2009}, in which a solution is searched over the worst-case from all unit forces. The worst-case configuration is indeed the loads acting in phase.} As is shown in Fig.~\ref{fig:powers}d, the peak power for the not-in-phase configuration can be considerably reduced by enabling this interaction.

To achieve this, we apply the convex relaxation \eqref{eqn:relax_single_harmonic2}. The optimal value of the objective function is equal to $9.29\times 10^{-13}$ which is a lower bound of the minimal peak power. However, the actual peak power of the design given by the convex relaxation is equal to $1.85$, which is even worse than that of the uniform truss, $0.056$. Nevertheless, the performance of the relaxation design is still better than that of the inphase designs, recall Fig.~\ref{fig:powers}c. For the relaxed solution, we also observe that the total mass constraint is not active: the total mass of the solution of the relaxation is $0.026$, which means that we have used only $2\%$ of the available mass \MTc{I think that the relaxation can be improved by enforcing the weight equality. This is because: Let $a$ satisfy $K(a) - \omega^2 M(a) \succeq 0$ and let $q^T a < m$. Then, for $\frac{m}{q^Ta}\ge\alpha>1$ it holds that
\begin{enumerate}
    \item $\alpha q^T a \le m$ (weight constraint satisfied)
    \item $\inf_{u \in \mathbb{R}^{n}\setminus Ker(M(a))} \frac{u^TK(a)u}{u^TM(a)u} = \inf_{u \in \mathbb{R}^{n}\setminus Ker(M(a))} \frac{\alpha u^TK(a)u}{\alpha u^TM(a)u} = \inf_{u \in \mathbb{R}^{n}\setminus Ker(M(a))} \frac{u^TK(\alpha a)u}{u^TM(\alpha a)u}$ (eigenvalue constraint satisfied)
    \item $F^T [K(\alpha a) - \omega^2 M(\alpha a)]^\dagger F = F^T [\alpha K(a) - \alpha \omega^2 M(a)]^\dagger F = \frac{1}{\alpha}F^T [K(a) - \omega^2 M(a)]^\dagger F \prec F^T [K(a) - \omega^2 M(a)]^\dagger F$ (peak power decreased)
\end{enumerate}%
I.e., if we up-scale the cross-section areas to utilize all material available, we decrease the peak power by the same factor. Because we can do this for any $a$, $q^Ta = m$ must hold at the optimum.}. Also the optimal $X$ is different with $F^TK_\omega(a)^\dagger F$ at the optimal solution:
\begin{equation}
	X = \begin{pmatrix}
		107.39 & 5.46\times 10^{-15}\\
		5.46\times 10^{-15} & 107.39
	\end{pmatrix}, F^TK_\omega(a)^\dagger F=\begin{pmatrix}
		77.09 & 23.59\\
		23.59 & 26.74
	\end{pmatrix}
\end{equation}
Clearly, we have $X\succ F^TK_\omega(a)^\dagger F$.

Furthermore, we implement the penalized relaxation \eqref{eqn:lag_relax_drop} with $\eta=1$, chosen arbitrarily. It converges to a solution described in Table \ref{tab:relpoint}.
\begin{table}[!htbp]
\centering
	\begin{tabular}{|c|c|}
		\hline
		actual peak power & $0.043$\\\hline
		total mass & $1$\\\hline
		$X$ & $\begin{pmatrix}1.56 & 0.37 \\ 0.37 & 0.50\end{pmatrix}$\\\hline
		$F^TK_\omega(a)^\dagger F$ & is equal to $X$\\\hline
		$\theta$ & $0.043$\\
		\hline
	\end{tabular}
    \caption{Solution of the penalized relaxation.}
    \label{tab:relpoint}
\end{table}
We confirm there numerically that $\theta$ is equal to the peak power whenever there is equality $X=F^TK_\omega(a)^\dagger F$. And the design $a$ corresponds to the truss in Figure \ref{fig:pen_single_harmonic}. The line width illustrates the actual value of $a$. As we can see in the example of one arbitrarily chosen $\eta$, we hope to improve the performance of peak power (compared to uniform truss) by choosing the best $\eta$. 

\textcolor{black}{
Finally, we have performed a grid search for 30 different values of $\eta$ ranging from $10^{-12}$ to $10$. In Figure \ref{fig:good_eta}, the optimal design of $\eta=0.002$ is plotted, which corresponds to a peak power of $5\times 10^{-8}$, however, it is very close to the optimal solution of $\eta=1$.} \MTc{To Shen: Which design did you send me for rendering? Taking the one you sent seems to have the peak power approx. $2\times10^{-5}$. Maybe I did something wrongly.}

\textcolor{black}{
We plot the actual peak power of the optimal solution as soon as the total mass of the optimal design of penalized relaxation against $\eta$. In Figure \ref{fig:single_load_peak_power}, the top figure shows the peak power of the optimal design at different $\eta$. The horizontal red line corresponds to the peak power of the uniform truss. As we can see in the figure, the peak power can be as low as $10^{-7}$ for $\eta$ between $10^{-3}$ and $10^{-1}$. The total mass constraint is active for large enough $\eta$. When $\eta$ is small; however, it tends to use less mass than what is available for the structure, and thus the peak power of the solution of penalized relaxation increases.
}

\SKc{Would be interesting to see more examples to see how often for some $\eta$ there is exactness}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.75\linewidth]{data/single_load_type2.png}
	\caption{Peak power and total mass as $\eta$ ranged from $10^{-12}$ to $10$}\label{fig:single_load_peak_power}
\end{figure}

\begin{figure}[H]
    \centering
    \subfigure[Optimal design of the penalized relaxation $\eta=1$.]{
         \includegraphics[width=0.4\textwidth]{data/pen_single_harmonic.png}
        \label{fig:pen_single_harmonic}
    }
    \hfill
    \subfigure[Optimal design of the penalized relaxation $\eta=0.002$.]{
         \includegraphics[width=0.4\textwidth]{data/good_eta_single_harmonic.png}
        \label{fig:good_eta}
    }
    \caption{Optimal solutions of convex relaxation with penalty}
\end{figure}
In Figure \ref{fig:trace_diff}, we could observe that for large enough $\eta$, there is a tight bound $X=F^TL(a)^\dagger F$. The difference of traces can be considered as $0$ for large enough $\eta$, however, the transition is ambiguous. It suggests that the relaxation with penalty \eqref{eqn:pen_relax_single_harmonic} as soon as in the general case \eqref{eqn:lag_relax_drop} can converge to a sub-optimal solution that is feasible for the equivalent reformulation of \eqref{eqn:general_minimization} with trace equality constraint.
\begin{figure}[H]
    \centering
    \includegraphics[width=0.75\textwidth]{data/trace_diff.png}
    \caption{the difference $\tr\{X-F^TK_{\omega}(a)^\dagger F\}$ of the optimal solution as $\eta$ ranged from $10^{-12}$ to $10$}
    \label{fig:trace_diff}
\end{figure}

\subsection{Cantilever beam problem}
The current method can be applied to solve the Cantilever beam problem, which consists of finding the optimal topology of a cantilever beam under a given loads that minimizes the peak power. This problem can be solved in such a way that :\begin{itemize}
    \item we discretize the beam into a uniform grid and we connect each pair of the nodes by a truss
    \item we optimize the peak power with respect to the cross section areas of the trusses.
\end{itemize}
More precisely, we consider the discret structure in the figure \ref{fig:full_beam} and a not-in-phased load $f$ applied to the upper right corner of the beam. The cantilever beam has $1$ height and $2$ width and is discretized into a uniform grid of $4\times 7$ nodes. The fully connected ground structure has $28\times 27/2=378$ trusses and thus $a$ in this context has dimension $378$. The trusses are made of a material of Young's Modulus $E=25000$ and density $\rho=1$. The total mass is less than or equal to $1$. We consider the convex relaxation with penalty coefficient $\eta=1$ chosen arbitrarily for instance, the minimization converges to the design shown in figure \ref{fig:cantilever_pen_1}.

\begin{figure}[H]
    \centering
    \subfigure[fully connected ground structure]{
         \includegraphics[width=0.45\textwidth]{data/full.png}
        \label{fig:full_beam}
    }
    \hfill
    \subfigure[Optimal design of the penalized relaxation $\eta=1$.]{
         \includegraphics[width=0.45\textwidth]{data/cantilever_single_harmonic.png}
        \label{fig:cantilever_pen_1}
    }
    \caption{Cantilever beam problem}
\end{figure}

The convex relaxation with penalty shows good scalability and the design has most of the cross section areas being $0$. The question of finding the ``best" eta is not addressed in the current study.

\subsection{Peak power minimization under multiple frequency loads}
The convex relaxation method is capable of finding optimal design under time varying load that is composed of several harmonic frequencies. The theoretical construction of the minimization problem remains the same as in the single frequency situation and is presented in the appendix \ref{appendix:peak_power_minimization_multi}. For numerical experiment, we start with the Heidari's truss problem as shown in the section \ref{sec:examples_heidari}. This time we add one more time varying load of frequency $2\omega$. The time varying load is composed by two complex components:
\begin{equation}
    f(t)=2\Re\{c_1(f)e^{i\omega t}+c_2(f)e^{2i\omega t}\}
\end{equation}
For the show case, we consider $c_2(f)=\frac{1}{2}c_1(f)$. To find an initial feasible design, we consider the following minimization problem:
\begin{equation}
    \begin{split}
        \underset{a,X,\theta,Q_1,Q_2}{\min} ~&~ 0\\
        \text{ s.t } & \left\{\begin{array}{l}
            a_i\geq0, a^Tq = m, -2^2\omega^2M(a)+K(a)\succeq 0\\
            \begin{pmatrix}
                X & F^* \\
                F & L_{2,\omega}(a)
            \end{pmatrix}\succeq 0\\
            P_j(\theta, \mathcal{C}X, Q_j)\succeq 0,\forall j\in\{1,2\}\\
        \end{array}\right.
    \end{split}
\end{equation}
Such minimization problem has a solution if and only if the constraint set is feasible. Notice that we have replaced the mass upper bound by an equality constraint since the mass upper bound constraint of the optimal solution of the peak power minimization tends to be active. The initial design has the power function as in the figure \ref{fig:power_function_multi}. The peak power of the initial design is equal to $18.8$.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.75\textwidth]{data/opt_power_function.png}
    \caption{power delivered to the structure by the time varying load }
    \label{fig:power_function_multi}
\end{figure}

We solve the convex relaxation with penalty coefficient $\eta=10$.
\begin{equation}
    \begin{split}
        \underset{a,X,\theta,Q_1,Q_2}{\min} ~&~ \theta+\eta\tr\{X\}\\
        \text{ s.t } & \left\{\begin{array}{l}
            a_i\geq0, a^Tq = m, -2^2\omega^2M(a)+K(a)\succeq 0\\
            \begin{pmatrix}
                X & F^* \\
                F & L_{2,\omega}(a)
            \end{pmatrix}\succeq 0\\
            P_j(\theta, \mathcal{C}X, Q_j)\succeq 0,\forall j\in\{1,2\}\\
        \end{array}\right.
    \end{split}
\end{equation}
At the optimal design of $\eta=10$, we obtain the power function represented by the orange curve in the figure \ref{fig:power_function_multi}. The peak power of the optimal design $\eta=10$ is equal to $1.03$. At the optimal design $\eta=10$, the mass upper bound constraint is active and the matrix equality
\begin{equation}
    X=F^*L_{2,\omega}(a)^\dagger F.
\end{equation}
is achieved.
%\MTc{Next, I would present more about the relaxation}



%\subsection{Multiple harmonic loads}
