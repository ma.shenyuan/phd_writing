

\newpage
\appendix
\section{Semidefinite representability of peak power under harmonic loads}\label{appendix:A}
\subsection{Positivity certificate of trigonometric polynomial}
The reader can find the main results of this subsection in the book \cite{dumitrescu_positive_2017}, whose focus is brought to their application in signal processing such as filter design. We are interested in the so called trigonometric polynomial in the following discussion.
\begin{definition}[Trigonometric polynomial]
A trigonometric polynomial $f$ is a real valued function with one variable in $\mathbb{T}$. It takes the form:
\begin{equation}
	f(z)=\sum_k c_k z^{-k}
\end{equation}
with finitely many non zeros coefficients $c_k$ for $k\in\mathbb{Z}$. Being real valued for all $z\in\mathbb{T}$, the sequence of coefficients must satisfy the symmetry condition $\forall k\in\mathbb{Z}, c_{-k}=c_k^*$. The degree of $f$ is the largest index $k$ of non zeros coefficient $c_k$.
\end{definition}

A \textit{positive} trigonometric polynomial is one that is positive $f(z)\geq 0, \forall z\in\mathbb{T}$. We want to obtain a tractable characterization of the positivity of a trigonometric polynomial. For univariate trigonometric polynomial, a powerful characterization by sum-of-squares (SOS) is available.
\begin{definition}[SOS trigonometric polynomial]
A SOS trigonometric polynomial $f$ is such that there exists a complex polynomial $h(z)=\sum_{k=0}^N h_kz^k$ satisfying:
\begin{equation}
	\forall z, f(z)=h(z)h(z)^*
\end{equation}
\end{definition}
Being SOS, such polynomial is trivially positive. However, the converse is true for univariate trigonometric polynomials. The reader can find this result in \cite[Theorem 1.1]{dumitrescu_positive_2017}. The proof relies on a careful study of position of roots of a univariate polynomial.
\begin{theorem}[SOS certificate of positivity]\label{thrm:pos_sos}
	A trigonometric polynomial is positive \textit{iff} it's SOS.
\end{theorem}

The next theorem will provide a link between the SOS characterization and PSD matrices. It has been shown that testing if a trigonometric polynomial is SOS is equivalent to the existence of a PSD matrix representation of the polynomial. Let's consider the following vector $\psi$:
\begin{equation}
	\psi(z)=(1,z,\dots,z^{N})^T
\end{equation}
containing all the canonical basis polynomial up to degree $N$. A trigonometric polynomial has a so called Gram matrix representation.
\begin{definition}
	Consider a degree $N$ trigonometric polynomial $f$, a Hermitian matrix $Q\in\mathbb{S}^{N+1}$ is a Gram matrix of $f$ if \begin{equation}
		\forall z, f(z)=\psi(z)^*Q\psi(z)
	\end{equation}
\end{definition}
There exists a family of constant matrices that establishes a link between $Q$ and the coefficient of $f$. To see this, we note that:
\begin{equation}
	\psi(z)^*Q\psi(z)=\tr{\psi(z)\psi(z)^*Q}
\end{equation}
However, 
\begin{equation}
	\psi(z)\psi(z)^*=
		\begin{pmatrix}
			1&z^{-1}&z^{-2}&\dots&z^{-N}\\
			z&1&z^{-1}&\ddots\\
			z^2&z&1&\ddots\\
			\vdots&\ddots&\ddots&\ddots&z^{-1}\\
			z^N&&&z&1
		\end{pmatrix}
\end{equation}
By defining matrices $\Lambda_k$ with Toeplitz structure:
\begin{equation}
	(\Lambda_k)_{i,j}=1\iff j-i=k
\end{equation}
And $(\Lambda_k)_{i,j}=0$ otherwise, we see that $\psi(z)\psi(z)^*=\sum_{k=-N}^{N}\Lambda_kz^{-k}$ thus by identifying coefficients, we see that:
\begin{equation}
	\forall z,f(z)=\sum_kc_kz^{-k}=\sum_k\tr{\Lambda_kQ}z^{-k}\iff \forall k, c_k=\tr{\Lambda_kQ}
\end{equation}
The Gram matrix $Q$ of a trigonometric polynomial is not unique. 
\begin{example}
	Let's consider the degree $2$ trigonometric polynomial $$f(z)=c_{-2}z^2+c_{-1}z+c_0+c_1z^{-1}+c_2z^{-2}$$ and we look for a Gram matrix representation $Q=\begin{pmatrix}
		q_{00}&q_{01}&q_{02}\\
		\overline{q}_{01}&q_{11}&q_{12}\\
		\overline{q}_{02}&\overline{q}_{12}&q_{22}
	\end{pmatrix}$. $Q$ must verify:
	\begin{equation}
		\begin{split}
			&k=-2:\tr{\begin{pmatrix}
				0&0&0\\
				0&0&0\\
				1&0&0
			\end{pmatrix}Q}=q_{02}=c_{-2}\\
			&k=-1:\tr{\begin{pmatrix}
				0&0&0\\
				1&0&0\\
				0&1&0
			\end{pmatrix}Q}=q_{01}+q_{12}=c_{-1}\\
		\end{split}
	\end{equation}
	And so on. Any Gram matrix of $f$ must have the following form:
	\begin{equation}
		Q = \begin{pmatrix}c_0&c_{-1}&c_{-2}\\c_{1}&0&0\\c_{2}&0&0\end{pmatrix}+\begin{pmatrix}
			-q_{11}-q_{22} & -q_{12} & 0\\
			-\overline{q}_{12} & q_{11} & q_{12} \\
			0 & \overline{q}_{12} & q_{22}
		\end{pmatrix}
	\end{equation}
	with arbitrary $q_{11},q_{22},q_{12}$.
\end{example}
\begin{example}\label{example:gram}
	More generally for a degree $N$ trigonometric polynomial $f$, by taking arbitrary $Q\in\mathbb{S}^N$ and by considering $N\times N$ Toeplitz matrices $\Lambda_k$, any Gram matrix of $f$ has the form:
	\begin{equation}
		\begin{pmatrix}
			c_0 & \begin{matrix}c_{-1} & \dots & c_{-N}\end{matrix}\\
			\begin{matrix} c_1 \\ \vdots \\ c_N\end{matrix}& 0
		\end{pmatrix}+\begin{pmatrix}
			-\tr{\Lambda_0 Q}& \begin{matrix}-\tr{\Lambda_{-1} Q} & \dots & -\tr{\Lambda_{-N}Q} \end{matrix}\\
			\begin{matrix}-\tr{\Lambda_{1}Q}\\ \vdots\\-\tr{\Lambda_N Q}\end{matrix}&Q
		\end{pmatrix}
	\end{equation}
	Note that $\Lambda_N$ and $\Lambda_{-N}$ are in fact zero matrix.
\end{example}
Finally, we can state without proof the main theorem of this subsection.
\begin{theorem}[semidefinite certificate of positivity]\label{thrm:pos_sdp}
	A trigonometric polynomial is SOS \textit{iff} there exists one Gram matrix $Q$ that is positive semidefinite. 
\end{theorem}
A proof of this theorem can be found in \cite[Theorem 2.5]{dumitrescu_positive_2017}.

By the computation in the example \ref{example:gram}, we only need to consider the right bottom corner of the Gram matrix as variable. Thus we conclude that a degree $N$ trigonometric polynomial is SOS \textit{iff} there exists $Q\in\mathbb{S}^N$ such that:
	\begin{equation}
		\begin{pmatrix}
			c_0 & \begin{matrix}c_{-1} & \dots & c_{-N}\end{matrix}\\
			\begin{matrix} c_1 \\ \vdots \\ c_N\end{matrix}& 0
		\end{pmatrix}+\begin{pmatrix}
			-\tr{\Lambda_0 Q}& \begin{matrix}-\tr{\Lambda_{-1} Q} & \dots & -\tr{\Lambda_{-N}Q} \end{matrix}\\
			\begin{matrix}-\tr{\Lambda_{1}Q}\\ \vdots\\-\tr{\Lambda_N Q}\end{matrix}&Q
		\end{pmatrix} \succeq 0
	\end{equation}
Furthermore, we can observe that the set of complex coefficients $c$ such that the associated trigonometric polynomial is positive is a LMI shadow.

\subsection{Semidefinite representability of peak power functional}\label{sec:sdr_peak_power}
We will show in this subsection that the peak power is SDr with respect to the Fourier coefficients of the nodal velocities $v$. We recall that the peak power is a function defined as follow:
\begin{equation}
	p[c(v)]=\underset{t\in[0,\frac{2\pi}{\omega}]}{\max}\left\{\left\vert\sum_{n,m}c_n(f)^Tc_m(v) e^{i(n+m)\omega t}\right\vert\right\}
\end{equation}
By collecting coefficients of the complex exponential, we note that:
\begin{equation}
	\sum_{n,m}c_n(f)^Tc_m(v) e^{i(n+m)\omega t}=\sum_{k=-2N}^{2N}\sum_{n=-N}^{N}c_{k-n}(f)^Tc_{n}(v)e^{ik\omega t}
\end{equation}
For convenience, the inner summation is over $-N$ to $N$, we have extended by $0$ the sequence $\{c_n(f)\}$ for $|n|$ strictly larger than $N$. Let's reparameterize $z=e^{-i\omega t}$ then let's define $q_k = \sum_{n=-N}^{N}c_{k-n}(f)^Tc_{n}(v)$, we note that:
\begin{equation}
	p[c(v)]=\underset{z\in\mathbb{T}}{\max}\left\vert q(z) \right\vert
\end{equation}
where $q(z)=\sum_{k=-2N}^{2N}q_kz^{-k}$. We can show that $q_0=0$, since the average power over one period delivered by the harmonic load is equal to $0$.

Let's consider $(\theta,c(v))$ in the epigraph of $p[.]$, then there is:
\begin{equation}
	\theta\geq p[c(v)]\iff \theta\pm q \text{ are positive trigonometric polynomials}
\end{equation}
By the theorem \ref{thrm:pos_sdp} and using the notation of \ref{example:gram}, since the coefficients of $\theta\pm q$ depend linearly on $\theta$ and $c(v)$, $(\theta,c(v))$ is in the epigraph of peak power function \textit{iff} it's in the intersection of two LMI shadows which is also an LMI shadow. The peak power function is thus SDr. More precisely, $\theta\pm q$ are positive \textit{iff} the following two LMI holds:
\begin{subequations}
    \begin{equation}
	\exists Q_1\in\mathbb{S}^{2N}, \begin{pmatrix}
			\theta & \begin{matrix}-q_{-1} & \dots & -q_{-2N}\end{matrix}\\
			\begin{matrix} -q_1 \\ \vdots \\ -q_{2N}\end{matrix}& 0
		\end{pmatrix}+\begin{pmatrix}
			-\tr{Q_1}& \begin{matrix}-\tr{\Lambda_{-1} Q_1} & \dots & 0 \end{matrix}\\
			\begin{matrix}-\tr{\Lambda_{1}Q_1}\\ \vdots\\0\end{matrix}&Q_1
		\end{pmatrix} \succeq 0
    \end{equation}
    \begin{equation}
    	\exists Q_2\in\mathbb{S}^{2N}, \begin{pmatrix}
    			\theta & \begin{matrix}q_{-1} & \dots & q_{-2N}\end{matrix}\\
    			\begin{matrix} q_1 \\ \vdots \\ q_{2N}\end{matrix}& 0
    		\end{pmatrix}+\begin{pmatrix}
    			-\tr{Q_2}& \begin{matrix}-\tr{\Lambda_{-1} Q_2} & \dots & 0 \end{matrix}\\
    			\begin{matrix}-\tr{\Lambda_{1}	Q_2}\\ \vdots\\0\end{matrix}&Q_2
    		\end{pmatrix} \succeq 0
    \end{equation}
\end{subequations}
Finally we need to replace the expression $q_k=\sum_{n=-N}^{N} c_{k-n}(f)^Tc_n(v)$.
\begin{example}[single harmonic $N=1$]
	We take Hermitian matrices $Q_1,Q_2\in\mathbb{S}^2$ and $f(t)=c_{-1}(f)e^{-i\omega t}+c_1(f)e^{i\omega t}$:
	\begin{equation}
		\begin{split}
			q_{-2}&=c_{-1}(f)^Tc_{-1}(u)\\
			q_{-1}&=c_{-1}(f)^Tc_{0}(u)+c_{0}(f)^Tc_{-1}(u)=0\\
			q_{1}&=c_{1}(f)^Tc_{0}(u)+c_{0}(f)^Tc_{1}(u)=0\\
			q_{2}&=c_{1}(f)^Tc_{1}(u)
		\end{split}
	\end{equation}
\end{example}
\begin{example}[double harmonic $N=2$]
	We shall take $Q_1,Q_2\in\mathbb{S}^4$ and $$f(t)=c_{-2}(f)e^{-2i\omega t}+c_{-1}(f)e^{-i\omega t}+c_{1}(f)e^{i\omega t}+c_{2}(f)e^{2i\omega t}$$
	Thus:
	\begin{equation}
		\begin{split}
			q_{-4}&=c_{-2}(f)^Tc_{-2}(u)+c_{-3}(f)^Tc_{-1}(u)+c_{-4}(f)^Tc_{0}(u)+c_{-5}(f)^Tc_{1}(u)+c_{-6}(f)^Tc_{2}(u)\\
				&=c_{-2}(f)^Tc_{-2}(u)\\
			q_{-3}&=c_{-1}(f)^Tc_{-2}(u)+c_{-2}(f)^Tc_{-1}(u)+c_{-3}(f)^Tc_{0}(u)+c_{-4}(f)^Tc_{1}(u)+c_{-5}(f)^Tc_{2}(u)\\
				&=c_{-1}(f)^Tc_{-2}(u)+c_{-2}(f)^Tc_{-1}(u)\\
			q_{-2}&=c_{0}(f)^Tc_{-2}(u)+c_{-1}(f)^Tc_{-1}(u)+c_{-2}(f)^Tc_{0}(u)+c_{-3}(f)^Tc_{1}(u)+c_{-4}(f)^Tc_{2}(u)\\
				&=c_{-1}(f)^Tc_{-1}(u)\\
			q_{-1}&=c_{1}(f)^Tc_{-2}(u)+c_{0}(f)^Tc_{-1}(u)+c_{-1}(f)^Tc_{0}(u)+c_{-2}(f)^Tc_{1}(u)+c_{-3}(f)^Tc_{2}(u)\\
				&=c_{1}(f)^Tc_{-2}(u)+c_{-2}(f)^Tc_1(u)\\
		\end{split}
	\end{equation}
	Then by symmetry condition:
		\begin{equation}
		\begin{split}
			q_{4}&=c_{2}(f)^Tc_{2}(u)\\
			q_{3}&=c_{1}(f)^Tc_{2}(u)+c_{2}(f)^Tc_{1}(u)\\
			q_{2}&=c_{1}(f)^Tc_{1}(u)\\
			q_{1}&=c_{-1}(f)^Tc_{2}(u)+c_{2}(f)^Tc_{-1}(u)\\
		\end{split}
	\end{equation}	
\end{example}
\section{Independence of peak power of non-physical solution}\label{appendix:B}
To state the independence of peak power with non-physical solution, we need to introduce the generalized eigenvalue problem related to $K(a)$ and $M(a)$. The generalized eigenvalue problem consists of finding $\lambda$ and $w$ such that:
\begin{equation}\label{eqn:eigenproblem}
    K(a)w=\lambda M(a)w
\end{equation}
By the finite element construction of $M(a)$ and $K(a)$, we can show that $\ker{M(a)}\subset\ker{K(a)}$ (see \cite{achtziger_structural_2008}). Any solution $w$ in the null space of $M(a)$ is thus a trivial solution of the eigenproblem. Thus we define the so called well-defined eigenproblem:
\begin{definition}[well-defined eigenproblem]
    The well-defined solution of \eqref{eqn:eigenproblem} is a pair of $(\lambda,w)$ such that:
    \begin{equation}
        K(a)w=\lambda M(a)w
    \end{equation}
    and $w\notin\ker{M(a)}$
\end{definition}
For any feasible $a$, we can show that there is a minimal solution $\lambda_{\min}(a)>0$ of well defined eigenproblem. The following proposition is true for $\lambda_{\min}(a)$:
\begin{proposition}[LMI characterization of minimal solution]
    \begin{equation}
        \forall\omega, \omega^2<\lambda_{\min}(a)\iff -\omega^2M(a)+K(a)\succeq 0
    \end{equation}
\end{proposition}
This is consequence of the proposition 2.2(c) of \cite{achtziger_structural_2008}.

Going back to the peak power minimization and we recall that $(a,c(v))$ is feasible if and only if
\begin{equation}
    \begin{split}
        a_i\geq0, a^Tq\leq m, -N^2\omega^2M(a)+K(a)\succeq 0\\
        (-k^2\omega^2M(a)+K(a))c_k(v)=ik\omega c_k(f),\forall k
    \end{split}
\end{equation}
By the LMI constraint, each of the $n^2\omega^2$ is strictly less than $\lambda_{\min}(a)$. For a fixed $n$, let's consider the solution of \begin{equation}
    K_{n\omega}(a)c_n(v)=in\omega c_n(f)
\end{equation}
where $K_{n\omega}(a)$ stands for the dynamic stiffness matrix $-n^2\omega^2M(a)+K(a)$. It must have the form $c_n(v)=inK_{n\omega}(a)^\dagger c_n(f)+c_0$ for any $c_0\in\ker{K_{n\omega}(a)}$. If $c_0\notin \ker{M(a)}$ then $n^2\omega^2$ would be a well-defined solution of the generalized eigenvalue problem, however this is not possible since $n^2\omega^2\leq N^2\omega^2<\lambda_{\min}(a)$.

What we have shown is in fact that $\ker{K_{n\omega}}(a)=\ker{M(a)}$ for all $n$. By the orthogonal complementarity the range of $\ker{K_{n\omega}}(a)$ is the same for all $n$. Finally, if all the $c_n(f)$ are in the range of $K_{n\omega}(a)$, 
\begin{equation}
    \forall m,c_m(f)^Tc_n(v)=c_m(f)^TK_{n\omega}(a)^\dagger c_n(f)
\end{equation}
The peak power is thus independent of the non-physical solution, since all its semidefinite representation is independent of the non-physical solution.

\section{Convex relaxation of the peak power minimization}\label{appendix:peak_power_minimization_multi}

\SKc{I put the computations here because they are a little bit messy. Later we decide where to put it. The computation can be easier in this section, I will do it again.}

We recall the peak power minimization under equilibrium equation constraint
\begin{equation}\label{eqn:pp_appendix_C}\tag{$\mathcal{P}_{\text{pp}}$}
    \begin{split}
        \underset{a,c(v),\theta,Q_1,Q_2}{\min} ~&~ \theta\\
        \text{ s.t } & \left\{\begin{array}{l}
            a_i\geq0, a^Tq\leq m, -N^2\omega^2M(a)+K(a)\succeq 0\\
            (-k^2\omega^2M(a)+K(a))c_k(v)=ik\omega c_k(f),\forall k=-N,\dots,N\\
            P_j(\theta, c(v), Q_j)\succeq 0,\forall j\in\{1,2\}\\
        \end{array}\right.
    \end{split}
\end{equation}
The LMIs defined by matrix valued function $P_j$ are consequences of the SDP certificates of the positivity of trigonometric polynomial. Moreover, they are:
\begin{subequations}
    \begin{equation}
        j=1:\begin{pmatrix}
		\theta-\tr\{Q_1\} & \left(q_{-k}-\tr\{\Lambda_{-k}Q_1\}\right)_{k=1,\dots,2N}\\
		  * & Q_2
    	\end{pmatrix}\succeq 0
    \end{equation}
    \begin{equation}
        j=2:\begin{pmatrix}
		\theta-\tr\{Q_2\} & \left(-q_{-k}-\tr\{\Lambda_{-k}Q_2\}\right)_{k=1,\dots,2N}\\
		* & Q_2
    	\end{pmatrix}\succeq 0
    \end{equation}
\end{subequations}
The coefficients $q_k$ are:
\begin{equation}
    \forall k\in\{-2N,\dots,2N\},q_k=\sum_{n=-N}^{N}c_{k-n}(f)^Tc_{n}(v)
\end{equation}
with the convention that $c_m(f)=0$ if $|m|>N$. Thanks to the symmetry condition $c_{-k}(v)=\overline{c_k(v)}$, we only need to consider equilibrium equations $(-k^2\omega^2M(a)+K(a))c_k(v)=ik\omega c_k(f)$ for $k\in\{1,\dots,N\}$. Thus,
\begin{equation}
    \begin{split}
        q_k&=\sum_{n=-N}^{N}c_{k-n}(f)^Tc_{n}(v)\\
		&=\sum_{n=1}^{N}c_{n-k}(f)^*c_{n}(v)+c_{n}(v)^*c_{k+n}(f)\\
		&=\sum_{n=1}^{N}c_{n-k}(f)^*K_{n\omega}(a)^\dagger in\omega c_{n}(f)+(in\omega c_{n}(f))^*K_{n\omega}(a)^\dagger c_{k+n}(f)
    \end{split}
\end{equation}
Let's define $Dc(f)=\begin{pmatrix}
    i\omega c_1(f)^T & \dots & iN\omega c_N(f)^T
\end{pmatrix}^T$, a column block vector such that each block corresponds to the vector $in\omega c_n(f)$. Similarly, we consider $c(f)$ a column block vector with block $c_n(f)$. Using the shifting operator $T_kc(f)$ such that the $n$-th block of $T_kc(f)$ is $c_{n+k}(f)$, we can see $q_k$ as:
\begin{equation}
    q_k = (T_{-k}c(f))^*L_{N,\omega}(a)^\dagger Dc(f)+(Dc(f))^*L_{N,\omega}(a)^\dagger T_{k}c(f)
\end{equation}
where $L_{N,\omega}(a)=\diag\{K_\omega(a),\dots,K_{N\omega}(a)\}$. By the convention $c_m(f)$ is zero for $m>N$, we see that $T_kc(f)=0$ for any $k\geq N$. Thus we define the matrix $F$ such that:
\begin{equation}
    F = \begin{pmatrix}
        T_{N-1}c(f)&\dots&T_{1}c(f)&Dc(f)&T_{-1}c(f)&\dots&T_{-2N}c(f)
    \end{pmatrix}
\end{equation}
If $X=F^*L_{N,\omega}(a)^\dagger F$ then there are constant matrices $C_k$ such that:
\begin{equation}
    q_k = \tr\{C_kX\}
\end{equation}
By introducing the Hermitian variable $X$ of size $3N\times 3N$, the peak power minimization problem \eqref{eqn:pp_appendix_C} is equivalent to :
\begin{equation}
    \begin{split}
        \underset{a,X,\theta,Q_1,Q_2}{\min} ~&~ \theta\\
        \text{ s.t } & \left\{\begin{array}{l}
            a_i\geq0, a^Tq\leq m, -N^2\omega^2M(a)+K(a)\succeq 0\\
            c_k(f)\in\range\{-k^2\omega^2M(a)+K(a)\},\forall k\in\{1,\dots,N\}\\
            X=F^*L_{N,\omega}(a)^\dagger F\\
            P_j(\theta, \mathcal{C}X, Q_j)\succeq 0,\forall j\in\{1,2\}\\
        \end{array}\right.
    \end{split}
\end{equation}
where $\mathcal{C}$ is the linear operator $X\mapsto \left(\tr\{C_kX\}\right)_{k=-2N,\dots,2N}$. Finaly, using the techniques presented in sections \ref{sec:conv_relax} and \ref{sec:lag_relax}, the previous problem is at first equivalent to :
\begin{equation}
    \begin{split}
        \underset{a,X,\theta,Q_1,Q_2}{\min} ~&~ \theta\\
        \text{ s.t } & \left\{\begin{array}{l}
            a_i\geq0, a^Tq\leq m, -N^2\omega^2M(a)+K(a)\succeq 0\\
            \begin{pmatrix}
                X & F^* \\
                F & L_{N,\omega}(a)
            \end{pmatrix}\succeq 0\\
            X=F^*L_{N,\omega}(a)^\dagger F\\
            P_j(\theta, \mathcal{C}X, Q_j)\succeq 0,\forall j\in\{1,2\}\\
        \end{array}\right.
    \end{split}
\end{equation}
And the Lagrange relaxation is :
\begin{equation}
    \begin{split}
        \underset{a,X,\theta,Q_1,Q_2}{\min} ~&~ \theta+\eta\tr\{X\}-\eta\tr\{F^*L_{N,\omega}(a)^\dagger F\}\\
        \text{ s.t } & \left\{\begin{array}{l}
            a_i\geq0, a^Tq\leq m, -N^2\omega^2M(a)+K(a)\succeq 0\\
            \begin{pmatrix}
                X & F^* \\
                F & L_{N,\omega}(a)
            \end{pmatrix}\succeq 0\\
            P_j(\theta, \mathcal{C}X, Q_j)\succeq 0,\forall j\in\{1,2\}\\
        \end{array}\right.
    \end{split}
\end{equation}
when $\eta=0$ we find the convex relaxation and the convex relaxation with penalty is:
\begin{equation}
    \begin{split}
        \underset{a,X,\theta,Q_1,Q_2}{\min} ~&~ \theta+\eta\tr\{X\}\\
        \text{ s.t } & \left\{\begin{array}{l}
            a_i\geq0, a^Tq\leq m, -N^2\omega^2M(a)+K(a)\succeq 0\\
            \begin{pmatrix}
                X & F^* \\
                F & L_{N,\omega}(a)
            \end{pmatrix}\succeq 0\\
            P_j(\theta, \mathcal{C}X, Q_j)\succeq 0,\forall j\in\{1,2\}\\
        \end{array}\right.
    \end{split}
\end{equation}
