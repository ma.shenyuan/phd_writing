\section{Minimization of SDr function under equilibrium}\label{sec:minSDR}
\MTc{There should be an intro paragraph stating what is inside this section}

Let us develop the framework of minimization of a semi-definite representable (SDr) function under the equilibrium
equation in more detail.

\subsection{Schur's complement lemma}
Schur's complement of a block matrix is an essential tool to transform the non-linear constraints of a special structure into LMI constraints. Let us consider a block matrix $M=\begin{pmatrix}A & B^T \\ B & C\end{pmatrix}$. Schur's complement lemma relates the positive-(semi)definiteness of $M$ to the positive-(semi)definiteness of its blocks. Let us first assume that the block $C$ is invertible.
\begin{lemma}[Schur's complement lemma with $C$ invertible \cite{wolkowicz_handbook_2000}]
	Consider the block matrix $M$ as defined above with the matrix $C$ invertible. Then, $M\succeq 0$ if and only if the following two conditions hold:
	\begin{equation}
		\left\{\begin{array}{l}
		C\succeq 0\\
		A - B^TC^{-1}B\succeq 0
		\end{array}\right.
	\end{equation}
\end{lemma}
In the case when $C$ is not invertible, we can use the Moore-Penrose pseudo-inverse instead of the inverse. However, additional conditions for $B$ are now required, see \cite[Appendix A.5]{boyd_convex_2004}:
\begin{lemma}[Generalized Schur's complement lemma \cite{boyd_convex_2004}]
	Consider a block matrix. $M\succeq 0$ iff the following conditions hold:
	\begin{equation}
		\left\{\begin{array}{l}
		C\succeq 0\\
		(I-CC^\dagger)B=0\\
		A - B^TC^{\dagger}B\succeq 0
		\end{array}\right.
	\end{equation}
\end{lemma}
The second condition $(I-CC^\dagger)B=0$ means that all the column vectors of $B$ are in the range space of $C$. The use of Schur's complement lemma is essential for obtaining equivalent (convex) reformulations of problems under the equilibrium equation $L(a)u=f$, see \cite{tyburec_global_2021}, for example. The Generalized Schur's complement lemma also allows us to conclude that $B$'s columns are in the range space of $C$, once there exists indeed a matrix $A$ of an adapted size such that:
\begin{equation}
    \begin{pmatrix}A & B^T \\ B & C\end{pmatrix}\succeq 0
\end{equation}

The (Generalized) Schur's complement lemma still holds whenever $A$ and $C$ are Hermitian and $B^T$ is replaced by $B^*$, this property is used to constructed the constraints for peak power minimization since we use complex Fourier coefficients.

\subsection{Semidefinite representable function}
In \eqref{eqn:general_minimization}, we want to deal with the case in which $p$ is a semidefinite representable function.
\begin{definition}[SDr function, {\cite[Lecture 4.2]{ben-tal_lectures_2001}}]
Let $p$ be a convex function, it is semidefinite representable (SDr) if and only if its epigraph is an LMI shadow. Namely, there are linear matrix-valued functions $P_j$, $j \in \{0,1,2\}$, such that
	\begin{equation}
		\forall \theta \in \mathbb{R},\forall u\in\mathbb{R}^n,\theta\geq p[u]\iff \exists v\in\mathbb{R}^m, P_0(\theta)+P_1(u)+P_2(v)\succeq 0
	\end{equation}
\end{definition}
Whenever $p$ is SDr, the minimization problem \eqref{eqn:general_minimization} is, after adding the variables $\theta$ and $v$, equivalent to
\begin{equation}\label{eqn:SDr_minimization}
	\begin{split}
		\underset{a,u,\theta,v}{\min} ~&~ \theta\\
		\text{ s.t } & \left\{\begin{array}{l}
			a_i\geq0,m-q^Ta\geq 0, L(a)\succeq 0\\
			L(a)u=f\\
			P_0(\theta)+P_1(u)+P_2(v)\succeq 0
		\end{array}\right.
	\end{split}
\end{equation}
\begin{example}[Compliance minimization]
	Compliance minimization is formulated as:
	\begin{equation}\tag{$\mathcal{P}_{\text{compl}}$}\label{eqn:compl_minimization}
		\begin{split}
			\underset{a,u}{\min} ~&~ f^Tu\\
			\text{ s.t } & \left\{\begin{array}{l}
				a_i\geq 0,q^Ta\leq m, K(a)\succeq 0\\
				K(a)u=f
			\end{array}\right.
		\end{split}
	\end{equation}
	The compliance $u\mapsto f^Tu$ is SDr, since it is linear in $u$. $\theta\geq f^Tu$ is already an LMI, with a one-dimensional matrix. Thus \eqref{eqn:compl_minimization} is equivalent to:
    \begin{equation}
		\begin{split}
			\underset{a,u, \theta}{\min} ~&~ \theta\\
			\text{ s.t } & \left\{\begin{array}{l}
				a_i\geq 0,q^Ta\leq m, K(a)\succeq 0\\
				K(a)u=f\\
                \theta\geq f^T u
			\end{array}\right.
		\end{split}
	\end{equation}
\end{example}
\begin{example}[Peak power minimization under single harmonic load \cite{heidari_optimization_2009}]
	We assume that the load varying over time has one frequency component $f(t)=\cos(\omega t)f_R+\sin(\omega t)f_I$. The nodal velocities $v(t)=\cos(\omega t)v_R+\sin(\omega t)v_I$ satisfy the ODE:
	\begin{equation}
		M(a)\ddot{v}+K(a)v=\dot{f}
	\end{equation}
	and at the steady state\MTrev{}{,} $v_R$ and $v_I$ satisfy:
	\begin{equation}
		\left\{\begin{array}{l}
			(-\omega^2M(a)+K(a))v_R=\omega f_I\\
			(-\omega^2M(a)+K(a))v_I=-\omega f_R\\
		\end{array}\right.
	\end{equation}
	It has been shown in \cite{heidari_optimization_2009} that
	\begin{equation}
		f(t)^Tv(t)=\frac{1}{4}((f_R^Tv_R-f_I^Tv_I)-i(f_R^Tv_I+f_I^Tv_R))e^{i2\omega t}+\frac{1}{4}((f_R^Tv_R-f_I^Tv_I)+i(f_R^Tv_I+f_I^Tv_R))e^{-i2\omega t}
	\end{equation}    
	
    Consider $(\theta,v)$ in the epigraph of the peak power function $\theta\geq \underset{t}{\max}{|f(t)^Tv(t)|}$. Now letting $z=e^{i\omega t}$, we can write $f^Tv$ as a trigonometric polynomial in $z$. The epigraph condition is equivalent to the positivity of polynomials $\theta\pm f^Tv$. By Theorem \eqref{thrm:pos_sdp},  iff $\exists Q^0=\begin{pmatrix}q^0_{00} & q^0_{01}\\\overline{q^0_{01}} & q^0_{11}\end{pmatrix}, Q^1=\begin{pmatrix}q^1_{00} & q^1_{01}\\\overline{q^1_{01}} & q^1_{11}\end{pmatrix}$:
	\begin{equation}
		\begin{pmatrix}
			\theta -q^0_{11}-q^0_{22}& -q^0_{12} & \frac{1}{4}((f_R^Tv_R-f_I^Tv_I)-i(f_R^Tv_I+f_I^Tv_R))\\
			-\overline{q^0_{12}} & q^0_{11} & q^0_{12}\\
			\frac{1}{4}((f_R^Tv_R-f_I^Tv_I)+i(f_R^Tv_I+f_I^Tv_R)) &\overline{q^0_{12}} & q^0_{22}
		\end{pmatrix}\succeq 0
	\end{equation}
	\begin{equation}
		\begin{pmatrix}
			\theta -q^1_{11}-q^1_{22}& -q^1_{12} & -\frac{1}{4}((f_R^Tv_R-f_I^Tv_I)-i(f_R^Tv_I+f_I^Tv_R))\\
			-\overline{q^1_{12}} & q^1_{11} & q^1_{12}\\
			-\frac{1}{4}((f_R^Tv_R-f_I^Tv_I)+i(f_R^Tv_I+f_I^Tv_R)) &\overline{q^1_{12}} & q^1_{22}
		\end{pmatrix}\succeq 0
	\end{equation}
As a consequence, the minimization of the peak power under a single harmonic load reads as
\begin{equation}
	\begin{split}
		\underset{a,v_R,v_I,\theta,Q^0,Q^1}{\min} & \theta\\
		\text{ s.t. } & \left\{\begin{array}{l}
			a_i\geq 0, q^Ta\leq m, (-\omega^2M(a)+K(a))\succeq 0\\
			(-\omega^2M(a)+K(a))v_R=\omega f_I\\
			(-\omega^2M(a)+K(a))v_I=-\omega f_R\\
			P_j(\theta,v_R,v_I,Q^j)\succeq 0,\forall j\in \{0,1\}\\
		\end{array}\right.
	\end{split}
\end{equation}
$P_j$ are the two linear matrix functions providing a semidefinite certificate.	
\end{example}

\begin{example}[Peak power minimization]
	Peak power minimization problem:
	\begin{equation}
		\begin{split}
			\underset{a,c(v)}{\min} ~&~ p[c(v)]\\
			\text{ s.t } & \left\{\begin{array}{l}
				a_i\geq0, a^Tq\leq m, -N^2\omega^2M(a)+K(a)\succeq 0\\
				(-k^2\omega^2M(a)+K(a))c_k(v)=ik\omega c_k(f),\forall k=-N,\dots,N
			\end{array}\right.
		\end{split}
	\end{equation}
	The peak power $p[c(v)]=\underset{t\in[0,\frac{2\pi}{\omega }]}{\max}\{\left\lvert f(t)^Tv(t)\right\lvert\}$ is SDr thanks to the semidefinite certificate of positivity of trigonometric polynomials. The proof can be found in Appendix \ref{appendix:A}, Theorem \eqref{thrm:pos_sdp}. The peak power minimization is equivalent to
	\begin{equation}\tag{$\mathcal{P}_{\text{pp}}$}\label{eqn:peak_power_min_SDr}
		\begin{split}
			\underset{a,c(v),\theta,Q}{\min} ~&~ \theta\\
			\text{ s.t } & \left\{\begin{array}{l}
				a_i\geq0, a^Tq\leq m, -N^2\omega^2M(a)+K(a)\succeq 0\\
				(-k^2\omega^2M(a)+K(a))c_k(v)=ik\omega c_k(f),\forall k=-N,\dots,N\\
				P(\theta, c(v), Q)\succeq 0\\
			\end{array}\right.
		\end{split}
	\end{equation}
	The matrix function $P$ is obtained by the SDP certificate.
\end{example}
By the mean of the SDr function, the difficulty of the minimization problem of the form \eqref{eqn:general_minimization} is now concentrated in the equilibrium constraint. All the other constraints can be treated efficiently using existing SDP solvers. 

\SKc{Perhaps, actually, for the $L(a)$ of interest we do have uniqueness, as $L(a)$ is a sum of an elliptic and a monotone operator. Worth investigating. This would simplify things. }
For the following development, we define a so-called ``physical'' feasible point of the equilibrium equation. We note that if a particular pair of variables $(a,u)$ is feasible for the constraint $L(a)u=f$, then $f$ is in the range of $L(a)$, and $u$ must be of the form $u=L(a)^\dagger f+u_0$, where $L(a)^\dagger$ is the Moore-Penrose pseudo-inverse of $L(a)$ and $u_0$ is in the null space of $L(a)$. We call $L(a)^\dagger f$ (resp. $u_0$) the physical (resp. non-physical) part of $u$. Notice that since $f$ is in the range of $L(a)$, $(t, L(a)^\dagger f)$ is feasible for the equilibrium condition. Thus we make the following assumption:
\begin{assumption}\label{assumption:independance1}
	Assume that for any feasible $(a,u)$ of the constraint $L(a)u=f$, $p[u]$ is independent of the non-physical part of $u$, i.e., $p[u]=p[L(a)^\dagger f]$.
\end{assumption}
\begin{example}
    For compliance minimization, this assumption is satisfied by the symmetry of $K(a)$. If $f$ is in the range of $K(a)$ then $f$ is orthogonal to $u_0$ thus $p[u]=f^Tu=f^TK(a)^\dagger f$. We can reformulate the compliance minimization as a linear SDP problem:
	\begin{equation}
		\begin{split}
			\underset{a,\theta}{\min} ~&~ \theta\\
			\text{ s.t } & \left\{\begin{array}{l}
				a_i\geq 0,q^Ta\leq m\\
				\begin{pmatrix}
					\theta & f^T \\
					f & K(a)
				\end{pmatrix}\succeq 0
			\end{array}\right.
		\end{split}
	\end{equation}
	Thanks to Schur's complement lemma,
	\begin{equation}
		\left\{\begin{array}{l}
			K(a)\succeq 0\\
			f\in\range{K(a)}\\
			\theta\geq f^TK(a)^\dagger f
		\end{array}\right.\iff \begin{pmatrix}
			\theta & f^T \\
			f & K(a)
		\end{pmatrix}\succeq 0
	\end{equation}
\end{example}
Because $p$ is independent of the non-physical solution, its epigraph is independent of non-physical solution for any feasible $(a,u)$ as well. By the SDP representability of $p$, the LMI representation of the epigraph should also be independent of the non-physical solution. We recall that:
\begin{equation}
	\theta\geq p[u] \iff \exists v, P_0(\theta)+P_1(u)+P_2(v)\succeq 0.
\end{equation} 
Then, we can express the linear matrix function $P_1(u)$ as
\begin{equation}
	P_1(u)=\sum_j (v_j^Tu) \tilde{P}_j
\end{equation}
with a family of vectors $v_j$ and matrices $\tilde{P}_j$ of an appropriate size. We assume that the following condition holds%A sufficient condition \MTrev{so that}{for} $P_1$ \MTrev{is}{to be} independent of \MTrev{}{the }non-physical solution is \MTrev{}{as follows}:

\MTc{Should this really be an assumption and, e.g., not a lemma?}
\begin{assumption}\label{assumption:independance2}
	Assume that $v_j$ lies in the range of $L(a)$ whenever $f$ is in the range of $L(a)$.
\end{assumption}
\begin{lemma}
    The assumption \eqref{assumption:independance2} is sufficient for \eqref{assumption:independance1}.
\end{lemma}
\begin{proof}
    Let $p$ be an SDr function such that \eqref{assumption:independance2} holds, then for any $(a,u)$ feasible for $L(a)u=f$. By the symmetry of $L(a)$, if $v_j$ is in the range of $L(a)$ then $u_0$ is orthogonal to $v_j$ and thus
    \begin{equation}
    	P_1(u)=\sum_j (v_j^TL(a)^\dagger f) \tilde{P}_j.
    \end{equation}
    Thus the LMI representation of $p$ is independent of the non-physical solution which implies that the $p$ is independent of the non-physical solution.
\end{proof}

Returning to the peak power minimization problem, by a careful study of the generalized eigenvalue problem
 of free-vibrations \cite{achtziger_structural_2008},  we can show that the peak power function is indeed independent of the non-physical part of $(a,c(v))$ whenever it is feasible and whenever the highest driving frequency of the load is below the smallest free-vibration eigenfrequency of the structure, i.e., $-N^2\omega^2M(a)+K(a)\succeq 0$. For the proof, we refer the reader to Appendix \ref{appendix:B}.

\MTc{Somewhat strangely, the engineering community (at least at our faculty) distinguishes between ``natural frequency'' and ``free-vibration eigenfrequency''. The latter is connected to a numerical model, while the former is related to an experimentally-determined frequency of a real structure.}

Assumptions \ref{assumption:independance1} and \ref{assumption:independance2} are not restrictive, since it would be questionable to optimize an objective function that depends on the non-physical part of the solution of $L(a)u=f$. In the context of topology optimization of discrete structures, the non-physical part of $L(a)u=f$ would consist of the displacements or the velocities of nodes without any attached member.

\MTc{The above is clearly correct. However, although I didn't look into the appendix proof yet, it seems strange to me that the condition for the load frequency being below resonance is required. For the other case, I can not imagine how the free-nodes could affect the solution (they should not - we can add any number of such nodes). However it is, this does not affect anything :-)}

\subsection{A convex relaxation}\label{sec:conv_relax}
Let us now consider an SDr function and a minimization problem of the form \eqref{eqn:general_minimization}. In addition, let assumptions \ref{assumption:independance1} and \ref{assumption:independance2} hold. In what follows, we propose its convex relaxation.

First, by the semidefinite representability of the objective function, we introduce slack variables $\theta$ and $v$ and rewrite \eqref{eqn:general_minimization} as:
\begin{equation}
	\begin{split}
		\underset{a,u,\theta,v}{\min} ~&~ \theta\\
		\text{ s.t } & \left\{\begin{array}{l}
			a_i\geq0,m-q^Ta\geq 0, L(a)\succeq 0\\
			L(a)u=f\\
			P_0(\theta)+P_1(u)+P_2(v)\succeq 0.\\
		\end{array}\right.
	\end{split}
\end{equation}
Because of Assumption \eqref{assumption:independance1}, we can eliminate $u$ from the optimization variables since only the physical part of the equation $L(a)u=f$ matters. However, we need to keep in mind that $f$ must be in the range of $L(a)$, 
\begin{equation}
	\begin{split}
		\underset{a,\theta,v}{\min} ~&~ \theta\\
		\text{ s.t } & \left\{\begin{array}{l}
			a_i\geq0,m-q^Ta\geq 0, L(a)\succeq 0\\
			f\in\range{L(a)}\\
			P_0(\theta)+P_1(L(a)^\dagger f)+P_3(v)\succeq 0.\\
		\end{array}\right.
	\end{split}
\end{equation}
After exploiting Assumption \eqref{assumption:independance2}, we observe that the linear matrix function $P_2$ evaluated at $L(a)^\dagger f$ is
\begin{equation}
	P_1(L(a)^\dagger f) = \sum_{j}(v_j^TL(a)^\dagger f) \tilde{P}_j.
\end{equation} 
Further, we consider a matrix $F = (f~v_j)$ whose column vectors are $f$ and $v_j$ for all (finitely many) $j$. Then, there exist constant matrices $C_j$ such that $v_j^TL(a)^\dagger f = \tr\{C_j^T(F^TL(a)^\dagger F)\}$. To derive $C_j$, we note that
\begin{equation}
	v_j^TL(a)^\dagger f = (Fe_{j+1})^TL(a)^\dagger (Fe_0) = \tr\left\{e_0^Te_{j+1} F^TL(a)^\dagger F\right\}
\end{equation}
with $e_i$ denoting vectors of the canonical basis. Consequently, we can symmetrize the last expression and obtain $C_j$. After introducing a variable $X$ and a constraint $X=F^TL(a)^\dagger F$, we obtain the optimization problem
\begin{equation}
	\begin{split}
		\underset{a,\theta,v,X}{\min} ~&~ \theta\\
		\text{ s.t } & \left\{\begin{array}{l}
			a_i\geq0,m-q^Ta\geq 0, L(a)\succeq 0\\
			f\in\range{L(a)}\\
			X=F^TL(a)^\dagger F\\
			P_0(\theta)+\sum_j\tr\{C_j^T X\} \tilde{P}_j+P_3(v)\succeq 0\\
		\end{array}\right.
	\end{split}
\end{equation}
The range constraint can now be replaced because $X=F^TL(a)^\dagger F$ is equivalent to
\begin{equation}
    X=F^TL(a)^\dagger F\iff\left\{\begin{array}{l}
         X=F^TL(a)^\dagger F\\
         X\succeq F^TL(a)^\dagger F
    \end{array}\right.
\end{equation}
,by adding redundantly $X\succeq F^TL(a)^\dagger F$ as constraint. \MTc{This sentence needs a rewrite - equivalent to itself?}
Combined with Assumption \eqref{assumption:independance2} and thanks to Schur's complement lemma, we have the equivalent formulation:
\begin{equation}
	\begin{split}
		\underset{a,\theta,v,X}{\min} ~&~ \theta\\
		\text{ s.t } & \left\{\begin{array}{l}
			a_i\geq0,m-q^Ta\geq 0\\
			\begin{pmatrix}
			    X & F^T\\
                F & L(a)
			\end{pmatrix}\succeq 0\\
			X=F^TL(a)^\dagger F\\
			P_0(\theta)+\sum_j\tr\{C_j^T X\} \tilde{P}_j+P_3(v)\succeq 0\\
		\end{array}\right.
	\end{split}
\end{equation}
The only non-convex and nonlinear constraint is the equality $X=F^TL(a)^\dagger F$. By the second LMI constraint, we observe that for any feasible $a$ and $X$, there must be $X\succeq F^TL(a)^\dagger F$. Thus for any feasible $a$ and $X$, the matrix equality is equivalent to the trace constraint:
\begin{equation}
    X=F^TL(a)^\dagger F\iff \tr\{X-F^TL(a)^\dagger F\}=0
\end{equation}
\MTc{It should be possible to write this reversely, i.e., $\mathrm{Tr}(L(a) - F X^\dagger F^T)=0$. The benefit is the (usually) small size of $X$, and it would be possible to analytically express the pseudo-inverse in terms of entries of $X$, especially if $X$ is a $2\times2$ matrix. Instead of (2.25), we can then express a PO problem, where only nonlinear terms come from $X$. This doesn't have to be investigated here, I am just noting this not to forget.}

Thus finally we get the equivalent formulation of \eqref{eqn:general_minimization}:
\begin{equation}
	\begin{split}
		\underset{a,\theta,v,X}{\min} ~&~ \theta\\
		\text{ s.t } & \left\{\begin{array}{l}
			a_i\geq0,m-q^Ta\geq 0\\
			\begin{pmatrix}
			    X & F^T\\
                F & L(a)
			\end{pmatrix}\succeq 0\\
			\tr\{X-F^TL(a)^\dagger F\}=0\\
			P_0(\theta)+\sum_j\tr\{C_j^T X\} \tilde{P}_j+P_3(v)\succeq 0\\
		\end{array}\right.
	\end{split}
\end{equation}
The convex relaxation consists of removing the trace equality constraint:
\begin{equation}\label{eqn:convex_relaxation}
	\begin{split}
		\underset{a,\theta,v,X}{\min} ~&~ \theta\\
		\text{ s.t } & \left\{\begin{array}{l}
			a_i\geq0,m-q^Ta\geq 0\\
			\begin{pmatrix}
			    X & F^T\\
                F & L(a)
			\end{pmatrix}\succeq 0\\
			P_0(\theta)+\sum_j\tr\{C_j^T X\} \tilde{P}_j+P_3(v)\succeq 0\\
		\end{array}\right.
	\end{split}
\end{equation}
\begin{remark}
	We observe that :
	\begin{itemize}
		\item The convex relaxation does not require linearity of $L(a)$, i.e., it applies to both truss and frame settings. The relaxation requires the PSD-ness of $L(a)$. Under the assumption of linearity of $L(a)$, the relaxation is indeed a convex SDP problem, and can thus be solved efficiently by existing software.
		\item The relaxation converges to a statically-admissible design, i.e., the design obtained by solving the relaxation is capable of carrying the structural loads, which holds because the second LMI constraint secures that $f$ is in the range of $L(a)$ for any feasible point of the convex relaxation.
		\item If $X=F^TL(a)^\dagger F$ holds at the optimal solution of the convex relaxation, then we can extract the optimal solution of \eqref{eqn:general_minimization} from that of the convex relaxation.
%        \item \MTc{Not sure if to place this here, but it also holds that when $\omega \rightarrow 0$, the dynamic optimization problem is reduced to static/compliance minimization (at least when $X \in \mathbb{R}^{1\times1}$}
	\end{itemize}
\end{remark}
\subsection{Link between the convex relaxation and Lagrange relaxation}\label{sec:lag_relax}
Let us now take another look at the equivalent formulation with trace equality:
\begin{equation}
	\begin{split}
		\underset{a,\theta,v,X}{\min} ~&~ \theta\\
		\text{ s.t } & \left\{\begin{array}{l}
			a_i\geq0,m-q^Ta\geq 0\\
			\begin{pmatrix}
			    X & F^T\\
                F & L(a)
			\end{pmatrix}\succeq 0\\
			\tr\{X-F^TL(a)^\dagger F\}=0\\
			P_0(\theta)+\sum_j\tr\{C_j^T X\} \tilde{P}_j+P_3(v)\succeq 0\\
		\end{array}\right.
	\end{split}
\end{equation}
By the second LMI \MTc{To make referencing simpler, I suggest using the subequations environment.}, if $a$ and $X$ are feasible, then one side of the equality must hold \MTc{This sentence is unclear to me. Shouldn't be there two inequalities instead of one?}:
\begin{equation}
    \tr\{X-F^TL(a)^\dagger F\} \geq 0
\end{equation}
Thus we can replace the equality by $\tr\{X-F^TL(a)^\dagger F\} \leq 0$, the following optimization stays equivalent \MTc{Again, I don't see this (most probably my problem though:-)). Can this be somewhat expanded?}to \eqref{eqn:general_minimization}:
\begin{equation}\label{eqn:before_lag_trace_ineq}
	\begin{split}
		\underset{a,\theta,v,X}{\min} ~&~ \theta\\
		\text{ s.t } & \left\{\begin{array}{l}
			a_i\geq0,m-q^Ta\geq 0\\
			\begin{pmatrix}
			    X & F^T\\
                F & L(a)
			\end{pmatrix}\succeq 0\\
			\tr\{X-F^TL(a)^\dagger F\}\leq 0\\
			P_0(\theta)+\sum_j\tr\{C_j^T X\} \tilde{P}_j+P_3(v)\succeq 0\\
		\end{array}\right.
	\end{split}
\end{equation}
The Lagrange relaxation consists of moving the inequality constraint into the objective and penalizes it with a positive weight $\eta$: 
\begin{equation}\label{eqn:lag_relax}
	\begin{split}
		\underset{a,\theta,v,X}{\min} ~&~ \theta+\eta\tr\{X-F^TL(a)^\dagger F\}\\
		\text{ s.t } & \left\{\begin{array}{l}
			a_i\geq0,m-q^Ta\geq 0\\
			\begin{pmatrix}
			    X & F^T\\
                F & L(a)
			\end{pmatrix}\succeq 0\\
			P_0(\theta)+\sum_j\tr\{C_j^T X\} \tilde{P}_j+P_3(v)\succeq 0\\
		\end{array}\right.
	\end{split}
\end{equation}
\MTrev{The idea of Lagrange relaxation is to relax the optimization problem's constraint by penalizing the objective if the variables violate the original constraints.}{where the penalty factor $\eta$ is increased until the original constraint $\tr\{X-F^TL(a)^\dagger F\} \}\leq 0$ becomes feasible.} Let's denote by $L(\eta)$ the optimal value depending on $\eta$. For one $\eta^*$, suppose that $(a^*,\theta^*,v^*,X^*)$ is the corresponding solution, then for any $(a,\theta,v,X)$ satisfying the constraint of \eqref{eqn:before_lag_trace_ineq}, we have:
\begin{equation}
    \theta\geq\theta+\underbrace{\eta^*\tr\{X-F^TL(a)^\dagger F\}}_{\leq 0}\geq \theta^*+\eta^*\tr\{X^*-F^TL(a^*)^\dagger F\}
\end{equation}
Thus $\underset{\eta\geq 0}{\max}~L(\eta)$ provides a lower bound of the problem \eqref{eqn:before_lag_trace_ineq}.

We note that the convex relaxation \eqref{eqn:convex_relaxation} corresponds to the Lagrange relaxation with weight $\eta=0$. The Lagrange relaxation, however, stays a hard problem for the presence of $\tr\{F^TL(a)^\dagger F\}$. We propose to solve the following relaxation, by simply removing $\tr\{F^TL(a)^\dagger F\}$:
\begin{equation}\label{eqn:lag_relax_drop}
	\begin{split}
		\underset{a,\theta,v,X}{\min} ~&~ \theta+\eta\tr\{X\}\\
		\text{ s.t } & \left\{\begin{array}{l}
			a_i\geq0,m-q^Ta\geq 0\\
			\begin{pmatrix}
			    X & F^T\\
                F & L(a)
			\end{pmatrix}\succeq 0\\
			P_0(\theta)+\sum_j\tr\{C_j^T X\} \tilde{P}_j+P_3(v)\succeq 0\\
		\end{array}\right.
	\end{split}
\end{equation}
This is a linear SDP problem. 

\MTc{Returning to the comment by Didier on Thursday, $\eta\tr\{X-F^TL(a)^\dagger F\}$ is equivalent to $\eta\tr\{X\}$ - $\eta\tr\{F^TL(a)^\dagger F\}$. For the first term, one wants as low the trace of $X$ as possible. For the second term and due to the negative sign, we would like to have the maximum trace of the pseudo-inverse (weighted by $F$), which can be roughly approximated by minimizing the trace of $L(a)$. Consequently, we may have $\eta\tr\{X\} + \eta\tr\{L(a)\}$ in the objective. I don't know whether there is any good in that, however, because the term $\tr\{L(a)\}$ has a similar effect as the volume/weight function $q^Ta$. Because of the penalty, the relaxed solution may then tend to have a very low weight (not desired, we expect the opposite for the optimum points). To conclude this reasoning, I think you can also justify the only present $X$ by this.}