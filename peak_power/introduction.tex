\section{Introduction}
Designing lightweight yet stiff vibrating structures has been a long-standing objective in structural optimization \cite{Kang2006}. This contribution specifically addresses the optimization of structures under harmonic oscillations, which hold great significance in diverse applications. These applications include rotating machinery in household appliances, construction machinery for cars and ships \cite{Liu2015rev}, and the support structures of wind turbines \cite{Liu2015}. In this study, we focus on the optimization of truss structures, leveraging their excellent stiffness-to-weight ratio \cite{Liu2008,Tyburec2019}.

Structural optimization was pioneered in Michell's seminal work \cite{Michell_1904} in 1904, which demonstrated that the optimal distribution of mass for trusses, under a single loading condition, aligns with the principal stresses. Because the principal stresses are not straight in general, optimum designs may involve an infinite number of bars. To circumvent this, Dorn \emph{et al.} \cite{Dorn1964} discretized the design domain into the so-called ground structure, containing a fixed and finite number of optimized elements.

Relying on the ground structure discretization, many theoretical and applied results have been presented in the literature. These include convex formulations for topology optimization of trusses under single \cite{Dorn1964} or multiple \cite{Achtziger_1992, Vandenberghe_1996,Lobo_1998,Ben_Tal_2001} loading conditions, or possibly under the worst-case loading \cite{BenTal1997}. In dynamics, it has been shown that the constraint for the lower bound on the fundamental free-vibration eigenfrequencies is convex \cite{Ohsaki1999}, also allowing for an efficient maximization of the fundamental free-vibration eigenfrequency \cite{achtziger_structural_2008}. 

In 2009, Heidari \emph{et al.} \cite{heidari_optimization_2009} observed that similar to the convex semidefinite programming formulation developed for the static setting \cite{Vandenberghe_1996}, it is also possible to derive a convex semidefinite program for the peak-power minimization under harmonic oscillations. The reformulation, however, requires that only a single-frequency in-phase load is present, with the driving frequency strictly below the lowest resonance frequency of the structure itself.

Surveying the more applied literature on continuum topology optimization under harmonic oscillations surprisingly reveals that the same restricting assumptions have been used in this area as well. In particular, the predominant objective is to minimize the dynamic compliance function \cite{Ma1993}, which is physically meaningful only when the driving frequency of the load is below resonance \cite{silva_critical_2019}. Otherwise, the optimization converges to a disconnected material distribution at anti-resonance. To the best of our knowledge, this setting is not theoretically resolved yet.

The second typical assumption of single-frequency loads acting only in phase can partially be justified because it represents the worst-case situation \cite{heidari_optimization_2009}. On the other hand, such designs are hardly optimal for not-in-phase settings such as unbalanced rotating loads. To the best of our knowledge, the only author that has explored this setting is Liu \textit{et al.}~\cite{Liu2015}, who considered minimization of the energy loss per cycle due to the structural damping, and minimization of the displacement amplitude. The latter case was handled by aggregating samples over the cycle, with the number of samples balancing computational efficiency with the accuracy of the approximation. Nevertheless, convergence to a local solution can be hoped for at best.

As was pointed out in \cite{Venini2016}, real-world applications require multiple-frequency loads. In this direction, Liu \textit{et al.} \cite{Liu2015rev} minimized the integral of displacement amplitudes over a non-uniformly discretized frequency range, whereas Zhang \textit{et al.} \cite{Zhang2015} introduced an aggregation scheme for minimizing the worst-case dynamic compliance for multiple frequencies. We are, however, not aware of a publication that would consider multiple-frequency harmonic loads acting on a structure concurrently.

%\MTc{I think the above is sufficient. Nevertheless, there are two types of methods - time-domain and frequency-domain. The time domain methods can consider any kind of loads, but because of this they usually do not consider harmonic loads. The frequency-domain approaches have only been used with single harmonics, although multiple harmonics would allow for solving more real-world problems, approaching the scope of time-domain approaches.}

\subsection{Aims and contributions}
Inspired by the theoretical results by Heidari \textit{et al.} \cite{heidari_optimization_2009}, in this study, we develop a unifying framework for the minimization of compliance and peak power functions, while avoiding the assumptions for single-frequency and in-phase loads.

Our procedure inherently relies on the notion of semi-definite representable (SDr) functions \cite[Lecture 4.2]{ben-tal_lectures_2001}, which are convex functions whose epigraph can be represented by the projection of a linear matrix inequality (LMI) feasibility set, the so-called LMI shadow. Minimization of such functions under linear constraints can be equivalently reformulated into linear semidefinite programming (SDP), and hence convex, problems. 

We show that for compliance such representation is immediate due to the linearity. For the peak power, however, the SDP representation is not trivial, it relies on the positivity certificate of the trigonometric polynomials. Such certificates have already been extensively studied in the signal processing community for filter design \cite{dumitrescu_positive_2017}. 
%In this contribution, we mostly follow the method developed in \cite{dumitrescu_positive_2017}.

Using the notion of the SDr function, we propose convex relaxations of the minimization involving an SDr function and the equilibrium equation as a constraint. Minimization problems subject to an equilibrium equation are in general non-convex due its bilinear nature. In compliance minimization, one can exploit the problem structure and eliminate state variables from the minimization in order to obtain a linear SDP problem. 

After proposing the convex relaxation, we discuss its link to the Lagrange relaxation of a constrained minimization problem. The convex relaxation corresponds to the Lagrange relaxation with a penalty coefficient that equals $0$. Based on this observation, we propose a convex relaxation with a penalty term to generate sub-optimal solutions to the minimization problem.

At first, we use the convex relaxation method for single-frequency harmonic loads. The inherent novelty there is allowing the forces to act out of phase. Numerical experiments show that although the relaxation alone is not tight, a penalization term added to the objective function secures convergence to a sub-optimal solution.

After showing that the peak power under load with multiple harmonics is SDr, we use the relaxation technique of the current article to approximate its solution. \MTc{This should be expanded a bit}

This article is structured as follows. In Section \ref{sec:notation}, we explain the necessary notation and definitions. These are used in Section \ref{sec:formulation} to formalize the optimization problem we consider in this manuscript, and also to present a relaxation procedure to solve the problem in Section \ref{sec:minSDR}. Section \ref{sec:examples} then illustrates our method with two examples. Finally, Section \ref{sec:discussion} summarizes our developments and provides an outlook on related future research.

\section{Notations}\label{sec:notation}

In this section, we introduce the necessary notations. In particular, $\mathbb{R}$ and $\mathbb{C}$ represent the real and complex fields, respectively. $\mathbb{T}$ denotes the set of complex numbers of the modulus $1$, i.e., the set of all complex numbers $z=x + iy$ for which it holds that $\lvert z \rvert = \sqrt{x^2 + y^2}=1$ and $i^2 = -1$. $\mathbb{S}^N$ is the set of $N\times N$ symmetric or Hermitian matrices according to the context. For a complex number $z$, $\overline{z}$ is its complex conjugate while for a complex vector or matrix, $x^*=\overline{x}^T$ stands for the transpose conjugate.

We define the Frobenius scalar product $\langle A,B\rangle=\tr\{A^TB\}$, where $A,B\in\mathbb{S}^N$. When $A,B$ are real, it holds that $\tr\{A^TB\}=\sum_{i,j}A_{i,j}B_{i,j}$ and $\tr\{A^TB\}=\sum_{i,j}\overline{A_{i,j}}B_{i,j}$ applies to the Hermitian case. Thus, the set $\mathbb{S}^N$ with Frobenius scalar product can be identified as a Euclidean vector space.

Let $\mathbb{S}_+^N$ be the set of positive semidefinite (PSD) matrices and we define the partial order $X\succeq 0 \iff X\in\mathbb{S}_+^N$. A real symmetric (resp. Hermitian) matrix $A$ is PSD iff $x^TAx\geq 0,\forall x$ (resp. $x^*Ax\geq 0$). 

For a Hermitian matrix $A$, let $\Re{A}$ and $\Im{A}$ denote the matrices obtained by taking the real and imaginary part of $A$'s entries element-wise. Then, 
\begin{equation}
    A\succeq 0\iff\begin{pmatrix}\Re{A}&-\Im{A}\\\Im{A}&\Re{A}\end{pmatrix}\succeq 0.
\end{equation}
The latter matrix 
%$\begin{pmatrix}\Re{A}&-\Im{A}\\\Im{A}&\Re{A}\end{pmatrix}$ 
is real PSD. Thus, any PSD Hermitian matrix is equivalent to a real symmetric PSD matrix of a larger size.
%\MTc{I would write the equivalence as a stand-alone equation}

Let $F$ be an affine map from $\mathbb{R}^n$ (or $\mathbb{C}^n$) to the set of real symmetric matrices $\mathbb{S}^N$, a linear matrix inequality (LMI) is defined as $F(x)\succeq 0$. The feasible set $\{x, F(x)\succeq 0\}$ of the LMI is called a spectrahedron and it is convex and closed, see, e.g., \cite{boyd_convex_2004}.

%\MTc{I am thinking about the notation here. Why to have $t$ and $a$ for the same thing? Also, by $t$ one usually denotes time, which may be useful in the section with multiple harmonics (approximating $f(t)$ with the Fourier decomposition). If I did not miss anything, I would stick to $a$ instead of $t$ in the whole text.}

\section{Optimization problem formulation}\label{sec:formulation}

Here, we consider optimization problems occurring in the context of topology optimization of discrete structures with time-varying loads. 

\paragraph{Variables of the optimization problem} We can distinguish two types of variables:
%in the minimization problem \eqref{eqn:general_minimization}.
\begin{itemize}
    \item The \textit{design variables} $a\in\mathbb{R}^m$ represent the vector of parameters of the structure, e.g., cross-section areas, (pseudo)densities, etc.
    \item The \textit{state variables} $u\in\mathbb{R}^n$ describes the physics, e.g., displacements, velocities, temperature, etc.
\end{itemize}
% . In the context of this contribution, it incorporates the following block-diagonal matrices:

%\MTc{I would personally present the following section reversely. Start from the ODE, show that we can write it in the form of L(a)u = f, with explaining what is the form of the L(a) matrix and f.}

\paragraph{Inputs}
Let $f$ represent the load of the structure. 
We assume here that the time-varying load $f$ contains $N$ harmonic components of base angular frequency $\omega$, described by a sequence of complex vector-valued Fourier coefficients $c_k(f)\in\mathbb{C}^d,\forall k=-N,\dots,N$. $f$ is thus a periodic function:
\begin{equation}
	f(t)=\sum_{k=-N}^{N}c_k(f)e^{ik\omega t}.
\end{equation}
$f$ is real-valued, implying that the complex Fourier coefficients of $f$ satisfy the symmetric condition $c_{-k}(f)=\overline{c_k(f)}$, where the complex conjugation is taken entry-wise. We also assume that there is no constant component in the load, thus $c_0(f)=0$. By the finite element discretization, the nodal velocity $v$ is the solution of the following ordinary differential equation (ODE)\SKc{Show explicitly via the form of $L(a)$}:
\begin{equation}
	M(a)\ddot{v}+K(a)v=\dot{f}
\end{equation}
We can thus define the ordinary differential operator $L(a)$:
\begin{equation}
    L(a)v = M(a)\ddot{v}+K(a)v
\end{equation}
The steady-state solution of the ODE is a periodic function with $N$ harmonic components as well, satisfying:
\begin{equation}
	(-k^2\omega ^2M(a)+K(a))c_k(v)=ik\omega c_k(f),\forall k.
\end{equation}

\paragraph{Equilibrium equation} The equilibrium equation relates the state variables and design variables, such that $L(a)u=f$:
\begin{equation}
    L(a)v=\dot{f} \iff (-k^2\omega^2M(a)+K(a))c_k(v)=ik\omega  c_k(f),\forall k.
\end{equation}
\SKc{~\eqref{...} above \textcolor{red}{state formally}.}
By using the equilibrium equation as a constraint in the minimization \eqref{eqn:general_minimization}, we automatically eliminate the variables $a$ for which the design does not allow to carry the load, i.e., $f\notin\range{L(a)}$.

The matrix $M(a)$ and $K(a)$ are called the mass and the stiffness matrix. They depend linearly on the design variables $a$:
\begin{equation}
	\begin{split}
		M(a)=\sum_{i=1}^m a_iM_i\\
		K(a)=\sum_{i=1}^m a_iK_i
	\end{split},
\end{equation}
where $M_i$ and $K_i$ are PSD mass and stiffness matrices of individual finite elements. The process of computing $M_i$ and $K_i$ to create $M(a)$ and $K(a)$ is called ``assembling''. The elemental matrices $M_i$ and $K_i$ are usually sparse, but the assembled matrices can be dense.

We assume that the highest harmonic frequency $N\omega$ is less than or equal to the smallest non-singular free-vibration angular eigenfrequency. It is shown in \cite{achtziger_structural_2008} that this is equivalent to:
\begin{equation}
    -N^2\omega^2M(a)+K(a)\succeq 0
\end{equation}

\paragraph{Further constraints on design variables} 
When $a\in\mathbb{R}^m$ represents the vector of the cross-section areas of individual truss elements, a single LMI constraint %$G(a)\succeq 0$ collects all 
can collect a number of constraints on the design variables $a$:
\begin{itemize}
	\item $a_i\geq 0, \forall i=1,\dots,m$, securing non-negative cross-section areas,
	\item $m-q^Ta\ge 0$, providing an upper bound for structural mass, with $q\in\mathbb{R}^m$ being the constant vector of the element weight contributions,
	\item\label{constraint_fv} $-\lambda^2M(a)+K(a)\succeq 0$, ensuring that the smallest non-singular free-vibration angular eigenfrequency is at least $\lambda$, see, e.g., \cite{achtziger_structural_2008}.
\end{itemize}

\paragraph{Objective function} In this study, we consider the peak power $p[u]$ as the objective function. It is the maximum value of the instant power delivered to the structure by the load $f(t)$, i.e.,
\begin{equation}
	p[c(v)]=\underset{t\in[0,\frac{2\pi}{\omega }]}{\max}\{\left\lvert f(t)^Tv(t)\right\lvert\}.
\end{equation}

\paragraph{Peak power minimization} To summarize, we aim at solving the following minimization problem:
\begin{equation}\label{eqn:peak_power_min}
	\begin{split}
		\underset{a,c(v)}{\min} ~&~ p[c(v)]\\
		\text{ s.t } & \left\{\begin{array}{l}
			a_i\geq0, a^Tq\leq m, -N^2\omega^2M(a)+K(a)\succeq 0\\
			(-k^2\omega^2 M(a)+K(a))c_k(v)=ik\omega c_k(f),\forall k=-N,\dots,N
		\end{array}\right.
	\end{split}
\end{equation}
This could be seen as a particular instance of a broader class of problems:
%In this contribution, we investigate the solution to minimization problems that are formalized as:
\begin{equation}\label{eqn:general_minimization}
	\begin{split}
		\underset{a,u}{\min} ~&~ p[u]\\
		\text{ s.t } & \left\{\begin{array}{l}
			G(a)\succeq 0\\
			L(a)u=f
		\end{array}\right.
	\end{split}
\end{equation}
which could be seen as a unifying framework of compliance and peak power minimizations, namely, the minimization of a semi-definite representable (SDr) function under the equilibrium equation. 

%which is a particular instance of \eqref{eqn:general_minimization}.

%The peak power minimization was the subject of a previous study by Heideri \emph{et al.} \cite{heidari_optimization_2009} for the case of in-phase loads under the same driving frequency. In this setting, the optimization problem is equivalent to a linear semidefinite program (SDP), which resembles one arising in the context of the compliance minimization, replacing the stiffness matrix $K(a)$ by the dynamics stiffness matrix $L(a)$.

%Inspired by this resemblance, in this study, we propose a unifying framework of compliance and peak power minimizations, namely, the minimization of a semi-definite representable (SDr) function under the equilibrium equation. A SDr function \cite[Lecture 4.2]{ben-tal_lectures_2001} is a convex function whose epigraph can be represented by the projection of a linear matrix inequality feasibility set, so-called LMI shadow. Minimization of such function under linear constraints can be reformulated into a linear SDP problem equivalently. We show that for compliance such representation is immediate due to the linearity. For the peak power, however, the SDP representation is not trivial. We provide the proof in Appendix \ref{appendix:A}, using the positivity certificate of the trigonometric polynomials. Such certificates have already been extensively studied in the signal processing community for filter design \cite{dumitrescu_positive_2017}, in the system control \textcolor{red}{citation needed !}. In this contribution, we mostly follow the method developed in \cite{dumitrescu_positive_2017}.

%Using the notion of the SDr function, we propose a way to obtain convex relaxation of the minimization involving a SDr function and the equilibrium equation as a constraint. Minimization problems subject to an equilibrium equation is in general non-convex due to the lack of convexity of feasibility set of vectors satisfying the equilibrium equations. In compliance minimization, one can exploit the problem's structure carefully to eliminate state varibles from the minimization in order to obtain a linear SDP problem. 

%After proposing the convex relaxation, we discuss its link to the Lagrange relaxation of a constrained minimization problem. The convex relaxation corresponds to the Lagrange relaxation with penalty coefficient that equals to $0$. Based on this observation, we propose a convex relaxation with a penalty term to generate sub-optimal solutions of the minimization problem \eqref{eqn:general_minimization}.

%At first, we use the convex relaxation method for single-frequency harmonic loads. The inherent novelty there is allowing the forces to act out of phase. Numerical experiments show that although the relaxation alone is not very tight, a penalization term added to the objective function secures convergence to a sub-optimal solution.

%After showing that the peak power under load with multiple harmonics is SDr, we use the relaxation technique of the current article to approximate its solution.
%\MTc{The last paragraph should be enhanced.}
