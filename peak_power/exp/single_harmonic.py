import picos as pc
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt

from truss import var, display #my package

E = 2.5e4 # young module
rho = 1. #density
m = 1. #total mass
omega = 15. #angular frequency of the loads

x = np.array([0,3]+[0,1,2,3]*2+[0,3])
y = np.array([0]*2+[-1]*4+[-2]*4+[-3]*2)
nodes = var.nodes(x,y)
nodes.set_kinematic([0,1])
n1=[0,0,1,1,2,2,2,3,3,4,4,4,4,5,6,6,7,7,8,8,9]
n2=[2,3,4,5,3,6,7,4,7,5,7,8,9,9,7,10,8,10,9,11,11]
elements = var.elements(n1,n2)

t = var.truss(nodes, elements)
t.set_density(rho)
t.set_module_young(E)

#build time varying loads (without in-phase assumption)
f_R_all = np.zeros(2*nodes.Nn)
# ~ f_R_all[12:] = np.random.normal(size=2*nodes.Nn-12)
# ~ f_R_all /= np.linalg.norm(f_R_all)

f_R_all[21:24:2]=0.75
f_R = pc.Constant(f_R_all[nodes.dof])

f_I_all = np.zeros(2*nodes.Nn)
# ~ f_I_all[12:] = np.random.normal(size=2*nodes.Nn-12)
# ~ f_I_all /= np.linalg.norm(f_I_all)

# ~ f_I_all[:] = f_R_all[:]
f_I_all[20:24:2]=0.5*np.array([1,-1])
f_I = pc.Constant(f_I_all[nodes.dof])

#assemble matrices K and M as picos expression
t.assemble()

display.plot_truss(t, forces = [f_R_all, f_I_all], filename="ground_structure.png")

#this function computes the peak power at a given configuration of the truss
def get_peak_power(f_R,f_I, K_omega, omega):
	Kinv = np.linalg.inv(K_omega)
	c1 = (f_R+f_I).T@Kinv@(f_R-f_I)
	c2 = f_R.T@Kinv@f_I

	return omega*np.sqrt(0.25*c1**2+c2**2)

#get optimization variables and create expressions for later use
a = t.a
theta = pc.RealVariable("theta")
X = pc.SymmetricVariable("X",2)
Q1 = pc.HermitianVariable("Q1",2)
Q2 = pc.HermitianVariable("Q2",2)

#build auxiliary expressions
F = (omega*f_I & -omega*f_R)
K_omega = -omega**2*t.M + t.K
total_mass = (a | t.lengths*t.rho)

c2 = -1/(4*omega)*((X[1,1]-X[0,0])+1j*(X[0,1]+X[1,0]))
c_2 = -1/(4*omega)*((X[1,1]-X[0,0])-1j*(X[0,1]+X[1,0]))
#build LMI
LMI1 = (X & F.T ) // (F & K_omega)
LMI2 = (theta-pc.trace(Q1) & -Q1[0,1] & c2) // (((-Q1[1,0]) // (c_2)) & Q1)
LMI3 = (theta-pc.trace(Q2) & -Q2[0,1] & -c2) // (((-Q2[1,0]) // (-c_2)) & Q2)

#we compute the peak power of the uniform truss under given load
t.a.value = np.ones(t.elements.Ne)
total_mass_np = total_mass.np # being an expression of a, we can compute its value once a is specified
t.a.value /= total_mass_np #rescale to verify total mass constraint
peak_power_uniform = get_peak_power(f_R.np,f_I.np, K_omega.np, omega)
print(f"Peak power of uniform truss: {peak_power_uniform: e}")
print(f"total mass : {total_mass.np : e}")

#define minimization problem
peak_power_min = pc.Problem()
peak_power_min.set_objective("min",theta)

cons = [peak_power_min.add_constraint(a>=0)]
cons += [peak_power_min.add_constraint(total_mass==m)]
cons += [peak_power_min.add_constraint(LMI1>>0)]
cons += [peak_power_min.add_constraint(LMI2>>0)]
cons += [peak_power_min.add_constraint(LMI3>>0)]
cons += [peak_power_min.add_constraint(Q1>>0)]
cons += [peak_power_min.add_constraint(Q2>>0)]
#print(peak_power_min)

sol = peak_power_min.solve(solver="cvxopt")
actual_peak_power = get_peak_power(f_R.np,f_I.np, K_omega.np, omega)
print(f"peak power of the solution of convex relaxation : {actual_peak_power:e}")
print(f"total mass : {total_mass.np : e}")
print(f"lower bound : {theta.np:e}")
print(X.np)
print(F.np.T@np.linalg.inv(K_omega.np)@F.np)

display.plot_truss(t,forces = [f_R_all, f_I_all], filename="relaxation_single_harmonic.png")

pen_peak_power_min = pc.Problem()
eta = 1.
pen_peak_power_min.set_objective("min",theta+eta*pc.trace(X))
for c in cons:
	pen_peak_power_min += c

sol = pen_peak_power_min.solve(solver="cvxopt")
actual_peak_power = get_peak_power(f_R.np,f_I.np, K_omega.np, omega)
print(f"peak power of the solution of penalized relaxation : {actual_peak_power:e}")
print(f"total mass : {total_mass.np : e}")
print(f"lower bound : {theta.np:e}")
print(f"solution :{sol.primals}")
print(f"objective value:{sol.value:e}")
print(X.np)
print(F.np.T@np.linalg.inv(K_omega.np)@F.np)
display.plot_truss(t, forces = [f_R_all, f_I_all], filename="pen_single_harmonic.png")

N = 30
grid = np.logspace(-12,1,N)
actual_peak_power = []
tmass = []
a_good = []
peak_power_good = []
trace_good = []

eta_success = []

for eta in grid:
	pen_peak_power_min.set_objective("min",theta+eta*pc.trace(X))
	try:
		sol = pen_peak_power_min.solve(solver="cvxopt")
	except KeyboardInterrupt:
		exit()
	except:
		print(f"solution failed with eta={eta :e}")
		continue
		
	eta_success.append(eta)		
	pp = get_peak_power(f_R.np,f_I.np, K_omega.np, omega)
	actual_peak_power.append(pp)
	tmass.append(total_mass.value)
	trace_good.append(np.trace(X.np-F.np.T@np.linalg.inv(K_omega.np)@F.np))
	a_good.append(t.a.np)
		
eta_success = np.array(eta_success)
actual_peak_power = np.array(actual_peak_power)

id_eta = np.argmin(actual_peak_power)


fig, axs = plt.subplots(2, sharex=True)
axs[0].scatter(eta_success,actual_peak_power)
axs[0].set_yscale("log")
axs[0].set_xscale("log")
axs[0].axhline(peak_power_uniform, color="r")
axs[0].set_ylabel("peak power")
axs[1].scatter(eta_success,tmass)
axs[1].set_xscale("log")
axs[1].set_xlabel("eta")
axs[1].set_ylabel("total mass")
plt.savefig("single_load.png")

fig,axs = plt.subplots()
axs.scatter(eta_success,trace_good)
axs.set_xscale("log")
axs.set_yscale("log")
axs.set_xlabel("eta")
axs.set_ylabel("difference of trace")
plt.show()

t.a.value = a_good[id_eta]
print(actual_peak_power[id_eta])
print(trace_good[id_eta])
print(grid[id_eta])
np.savetxt("a", t.a.np)
display.plot_truss(t,forces = [f_R_all, f_I_all], filename="good_eta_single_harmonic.png")
