\section{Preliminary}
In this section, we will recall all the results that will be used later in order to reformulate the main problem (\ref{eq:main}) into POP.
\subsection{Positive certificat of trigonometric polynomial}
In this sub-section, we introduce the so called trigonometric polynomial and one way to certify it's \textit{(strict) positivity} over the complex $d$-torus. The general case being stated, however, we will only focus on the special case of univariate trigonometric polynomial, where, the same positivity certificate is stronger than the general case, such that it certifies the \textit{non-negativity}. The reader familiar with Lasserre's hierarchy will certainly recall the Putinar's positivstellensatz when reading this subsection, it is indeed equivalent to the main theorem of this subsection, however, we would like to preserve the complex structure in our application. The general SOS-certificat is proven in the paper \cite{megretski_positivity_2003}  using fundamental results of functional analysis and the univariate case is treated extensively in the book \cite{dumitrescu_positive_2017} by Dumitrescu. Discussions about the Lasserre's hierarchy in the context of trigonometric polynomial as well as its convergent rate can be found, for example, in the paper \cite{bach_exponential_2023}.

Let's first define the trigonometric polynomial. We recall that a Laurent polynomial is a polynomial that allows variables $x$ and their inverse $x^{-1}$.
\begin{definition}[Trigonometric polynomial]
A $d$-variate trigonometric polynomial is a $d$-variate Laurent polynomial over complex field $\mathbb{C}$, whose value is real over the complex $d$-torus $\mathbb{T}^d=\set{z\in\mathbb{C}^d\mid\forall i,|z_i|=1}$. If $p(z)=\sum_{k}c_kz^k$ a such polynomial, then the sequence of coefficients most satisfy the symmetry condition: $c_k=\overline{c_{-k}}$ if $\overline{z}$ denotes the complex conjugate of $z$.
\end{definition}
The main theorem of \cite{megretski_positivity_2003} is:
\begin{theorem}[Positive trigonometric polynomial]
A trigonometric polynomial $p$ is strictly positive on $\mathbb{T}^d$ iff there are the Laurent complex polynomials $V_i$ such that:
\begin{equation}
	p(z)=\sum_i\abs{V_i(z)}^2
\end{equation} 
\end{theorem}
It seems that the number of polynomial $V_i$ and their degrees are generally not known. However when $d=1$, a trigonometric polynomial is \textit{nonnegative} iff it is SOS and the degree of $V_i$ can be exactly the degree of $p$. In $d=2$, the previous theorem can certify also the \textit{nonnegativity} of $p$ but we don't know about the degree of $V_i$.

A univariate trigonometric polynomial can be expressed by a quadratic form and by the Gram matrix of this quadratic form. We recall the following definition/theorem (see theorem 2.3 of \cite{dumitrescu_positive_2017}):
\begin{theorem}[Gram matrix representation]\label{trm:gram matrix}
Let $p(z)=\sum_{k=-N}^{N}c_kz^k$ be trigonometric polynomial and let $\psi(z)=(1,\dots,z^N)^T$ be the vector composed by basis monomials, there exists a Hermitian matrix $Q$ of dimension $(N+1)\times(N+1)$ such that$$
	\forall z,p(z)=\psi(z)^*Q\psi(z)
$$
For $l=-N,\dots,N$, let $\Lambda_{l,N+1}$ be a family of real matrices of dimension $(N+1)\times(N+1)$ and $\paren{\Lambda_{l,N+1}}_{i,j}=1$ iff $i-j=l$ and zero otherwise, the coefficient $c_k$ and $Q$ are related such that:$$
	\forall l,\tr\paren{\Lambda_{l,N+1}Q}=c_l
$$
Such matrix $Q$ is called a Gram matrix representation of $p$.
\end{theorem}
In particular, $\Lambda_{0,N+1}$ is the identity matrix of dimension $N+1$. Let $N=2$, we have in particular:
\begin{equation}
	\Lambda_{-1,3}=\begin{pmatrix}
		0&1&0\\
		0&0&1\\
		0&0&0
	\end{pmatrix},\Lambda_{1,3}=\begin{pmatrix}
		0&0&0\\
		1&0&0\\
		0&1&0
	\end{pmatrix}
\end{equation}
The family of matrix $\Lambda_{l,N+1}$ enjoys the so called Toepliz structure.

Finally, the nonnegativeness and Gram matrices of a polynomial $p$ are related.
\begin{theorem}[sdp Gram matrix]\label{trm:sdp gram}
$p$ is nonnegative on $\mathbb{T}$ iff there exists one semidefinite positive Gram matrix $Q$.
\end{theorem}

The multivariate case is also treated in the book \cite{dumitrescu_positive_2017} and the author has generalized the theorem \ref{trm:gram matrix} and \ref{trm:sdp gram} by using the Kronecker products of $\Lambda_{l,N+1}$.

\subsection{Discrete Fourier Transform}\label{sc:dft}
We recall the definition of discrete Fourier Transform (DFT) that is a isometric linear transform with nice properties. The DFT acts on the complex vectors in $\mathbb{C}^N$ and gives another complex vectors of same dimension. Related to our application, we state the DFT with vectors of odd dimension that are indexed from $-N$ to $N$.
\begin{definition}[Discrete Fourier Transform]
Let $x=(x_n)_{n=-N,\dots,N}\in\mathbb{C}^{2N+1}$, its DFT $\hat{x}$ is defined as $\forall n$:
\begin{equation}
	\hat{x}_n=\sum_{m=-N}^N x_m\exp\paren{\frac{-2i\pi nm}{2N+1}}
\end{equation}
\end{definition}
The inverse DFT is :
\begin{definition}[inverse Discrete Fourier Transform]
Let $\hat{x}=(\hat{x}_n)_{n=-N,\dots,N}\in\mathbb{C}^{2N+1}$, its inverse DFT $x$ is defined as $\forall n$:
\begin{equation}
	x_n=\frac{1}{2N+1}\sum_{m=-N}^N x_m\exp\paren{\frac{2i\pi nm}{2N+1}}
\end{equation}
\end{definition}
Defined as previously, the DFT is an isometry from $\mathbb{C}^{2N+1}$ to $\mathbb{C}^{2N+1}$. Furthermore, it relates nicely the (circular) convolution products of two vectors to their DFT.
\begin{definition}[Convolution product]
Let's $x,y$ be $\mathbb{C}^{2N+1}$ vectors, their convolution product is $\forall n\in\set{-N,\dots,N}$:
\begin{equation}
	\paren{x*y}_n=\sum_{m=-N}^{N}x_{n-m}y_{m}
\end{equation}
If $n-m$ goes out of the range $\set{-N,\dots,N}$, we extend the vector $x$ or $y$ by periodicity.
\end{definition}
The DFT of $x*y$ is the \textit{element-wise} product of $\hat{x},\hat{y}$, in fact, using inverse DFT, we note that:
\begin{equation}
	\begin{split}
		(x*y)_n=&\sum_{m}x_{n-m}y_{m}=\frac{1}{(2N+1)^2}\sum_{m,p,q}\hat{x}_p\hat{y}_q\exp\paren{\frac{2i\pi((n-m)p+mq)}{2N+1}}\\
		&=\frac{1}{(2N+1)^2}\sum_{p,q}\hat{x}_p\hat{y}_q\exp\paren{\frac{2i\pi np}{2N+1}}\sum_{m}\exp\paren{\frac{2i\pi m(q-p)}{2N+1}}
	\end{split}
\end{equation}
However, we recall that $\sum_{m}\exp\paren{\frac{2i\pi m(q-p)}{2N+1}}=2N+1$ iff $p=q$ and it equals to $0$ if $p\neq q$. We will use this property repeatly throughout this paper. Finally,
\begin{equation}\label{eq:conv-DFT}
	(x*y)_n=\frac{1}{2N+1}\sum_{p}\hat{x}_p\hat{y}_p\exp\paren{\frac{2i\pi np}{2N+1}}
\end{equation}
We notice that $x*y$ is the inverse DFT of the element-wise product of $\hat{x}$ and $\hat{y}$. Thus the DFT $x*y$ is the element-wise product.

In fact this "duality" between element-wise product and convolution product extends beyond the usual product. Let's take $x$ and $y$ from $\mathcal{U}^{2N+1}$ and $\mathcal{V}^{2N+1}$ defined by some abstract spaces $\mathcal{U}$ and $\mathcal{V}$ for which supports multiplication by a complex number and addition between them. Let's define a bilinear pairing $<.,.>:(\mathcal{U},\mathcal{V})\rightarrow\mathcal{W}$ where $\mathcal{W}$ is a space similar to $\mathcal{U}$ and $\mathcal{V}$. We can define the convolution product between $x*y\in\mathcal{W}^{2N+1}$ as :
\begin{equation}
	(x*y)_n=\sum_{m}<x_{n-m},y_m>
\end{equation}  
This is well defined since we can add elements from $\mathcal{W}$. Now using inverse DFT (made possible because we can multiply by complex number and we can make addition) and the bilinearity, we have:
\begin{equation}
	(x*y)_n=\frac{1}{(2N+1)^2}\sum_{m,p,q}<\hat{x}_{p},\hat{y}_q>\exp\paren{\frac{2i\pi(n-m)p+mq}{2N+1}}
\end{equation}
And the same arguments follow, which allows us to prove finally that the DFT of $x*y$ is the element-wise pairing of $\hat{x}$ and $\hat{y}$.

In our application, we will have the following situations (\ref{sc:PMI rf}):
\begin{itemize}
	\item $\mathcal{U}=\mathcal{V}=\mathbb{C}^d$ and $<u,v>=\sum_{n}u_n^Tv_n\in\mathbb{C}$. So we will be dealing with families of complex vectors and the pairing is similar to inner product (but without conjugation).
	\item $\mathcal{U}$ contains complex matrices and $\mathcal{V}$ complex vectors and $<M,v>=Mv$ in the sense of matrix-vector product.
	\item $\mathcal{U}=\mathbb{C}$ and $\mathcal{V}$ complex vectors
\end{itemize}

\subsection{Schur's complement lemma}
Schur's complement lemma is useful to transform some expression involving the matrix inverse into PMI or LMI without matrix inverse. In general, the explicit inverse of matrices is not available numerically and even if they are available, they introduces polynomial with large degrees. The Schur's complement lemma reads:
\begin{lemma}[Schur]\label{lm:Schur}
Let $M=\begin{pmatrix}A & B^T \\ B & C\end{pmatrix}$ be a block matrix where $A,B,C$ have adapted dimension and $A$ and $C$ are real symmetric. Let $(.)^\dagger$ denotes the Moore-Penrose pseudo inverse of a matrix, then the following two conditions are equivalent:
\begin{itemize}
	\item $M\succeq 0$
	\item $C\succeq 0$, $A-B^TC^\dagger B\succeq 0$ and the column vectors of $B$ are in the range of $C$
\end{itemize} 
\end{lemma}
We can notice the pseudo inverse of $C$ in the second condition is removed and we only need to assess the SDP-ness of $M$ which is defined by blocks $A,B,C$
