\section{POP reformulation}
\subsection{Solving the matrix differentiable equation}
Being a linear system of ordinary differentiable equation, the solution $v$ of (\ref{eq:mde}) is periodic and has the same number of harmonics, thus $v(t)=\sum_{n=-N}^Nc_n(v)e^{int}$. Using (\ref{eq:mde}), we obtain:
\begin{equation}
    \left\{\begin{array}{ccc}
         K(a)c_0(v)&=&0  \\
         (-n^2M(a)+K(a))c_n(v)&=&-inc_n(f) 
    \end{array}\right.
\end{equation}
We can check handily that $(c_n(v))_n$ satisfies the symmetry condition. By the definite positiveness of each of the $-n^2M(a)+K(a)$, $c_n(v)$ are all well defined. We compute now the power received by structure:
\begin{equation}\label{eqn:power}
        f(t)^Tv(t)=\sum_{n,m}c_n(f)^Tc_m(v)e^{i(n+m)t}
\end{equation}
In particular, for $n+m=0$, by Plancherel's Theorem,
$$
\sum_{n=-N}^N c_n(f)c_{-n}(v)=\sum_{n=-N}^Nc_n(f)^T\overline{c_n(v)}=\frac{1}{2\pi}\int_{0}^{2\pi}f(t)^Tv(t)\mathrm{d}t.
$$
We can show that this integral is $0$ by using the matrix differential equation. In addition, we consider only the solution of (\ref{eq:mde}) where $c_0(v)=0$. In fact, if this term is non zero, there is a constant velocity at each nodal points and thus the structure is in rigid body motion that we don't consider. By using the reparameterization $z=e^{it}\in\mathbb{T}$ which is in the set of unit complex number, we obtain:
\begin{equation}
f(t)^Tv(t)=\sum_{m+n\neq0}c_n(f)^Tc_m(v)z^{n+m}
\end{equation}
One can check that the summation is real, indeed, if $c_n(f)^Tc_m(v)z^{n+m}$ is in the summation, it's conjugate also by the symmetry condition.

\subsection{Minimizing $C$ as min-max problem and its semi-infinite programming reformulation}
We recall that the main optimization problem to solve is :
\begin{equation}\label{eq:main recall}
    \begin{split}
        \underset{a\in\mathbb{R}^n}{\min} & ~ C(a_1,\dots,a_n)\\
        \text{ s.t. } & Q^Ta\leq M, K_N(a)\succeq 0, M(a)\succeq 0
    \end{split}
\end{equation}
For notational convenience, we have combined all the affine inequalities involving $a$ in one convex polytope constraint $Q^Ta\leq M$ for a matrix $Q$ and a vector $M$. The inequality $\leq$ is understood element-wise. And we use the short notation $K_n(a)=-n^2M(a)+K(a)$ for all $n$

As we have seen, the objective function $C$ is precisely:
\begin{equation}
	C:a\mapsto \underset{z\in\mathbb{T}}{\max}~\Big|\sum_{m+n\neq0}c_n(f)^Tc_m(v)z^{n+m}\Big|
\end{equation}
This is equivalent to a semi-infinite programming problem (an optimization problem with an infinity numbers of contraints):
\begin{equation}
	\begin{array}{cc}
		\underset{\theta}{\min} & \theta\\
		\text{ s.t. } & \forall z\in\mathbb{T},\left\{\begin{array}{l}\theta\geq\sum_{m+n\neq0}c_n(f)^Tc_m(v)z^{n+m}\\\theta\geq-\sum_{m+n\neq0}c_n(f)^Tc_m(v)z^{n+m}\end{array}\right.
	\end{array}
\end{equation}
As consequence, the problem (\ref{eq:main recall}) is in fact equivalent to a semi-infinite programming problem:
\begin{equation}\label{eq:semi inf}
	\begin{array}{cc}
		\underset{a,\theta,c_m(v)}{\min} & \theta\\
		\text{ s.t. } & \left\{\begin{array}{l}
			Q^Ta\leq M, -N^2M(a)+K(a)\succeq 0, M(a)\succeq 0\\
			K_m(a)c_m(v)=imc_m(f),\forall m\\
			\theta\geq\sum_{m+n\neq0}c_n(f)^Tc_m(v)z^{n+m},\forall z\in\mathbb{T}\\
			\theta\geq-\sum_{m+n\neq0}c_n(f)^Tc_m(v)z^{n+m},\forall z\in\mathbb{T}
		\end{array}\right.
	\end{array}
\end{equation}
The variables $c_m(v)$ are added as optimization variables to avoid an expression with inverse matrices, since $c_m(v)=K_m(a)^\dagger imc_m(f)$. This is practically not convenient since $c_m(v)$ are results of finite element discretization and they have large dimension. Thus, two difficulties remain in this semi-infinite programming reformulation :
\begin{itemize}
	\item we need to replace $c_m(v)$ and the constraints involving $c_m(v)$ by PMI/LMI involving variables with smaller dimensions,
	\item we need to find finite reformulation of the last two infinit contraints.
\end{itemize}

\subsection{SDP equivalence}
We will use the theorem \ref{trm:sdp gram} in the subsection to transform the two semi-infinite contraints of the problem \ref{eq:semi inf} into two sets of constraints applied on Hermitian sdp matrices.

Let's defined $p(z)=\theta-\sum_{m+n\neq0}c_n(f)^Tc_m(v)z^{n+m}$ for all $z\in\mathbb{T}$. By rearrange terms in the summation, we note that
\begin{equation}
	p(z)=\sum_{l=-2N}^{2N}g_lz^l
\end{equation}
and the sequence of coefficents $g_l$ is:
\begin{equation}
	\left\{\begin{array}{l}
		g_0=\theta\\
		g_l=-\sum_{n+m=l}c_n(f)^Tc_m(v)\text{ if }l\neq 0
	\end{array}\right.
\end{equation}
By theorem $\ref{trm:sdp gram}$, $p$ is nonnegative on $\mathbb{T}$ iff there is a $(2N+1)\times(2N+1)$ SDP Hermitian matrix $Q$ such that $p(z)=\psi(z)^*Q\psi(z)$ and $Q$ satisfies:
\begin{equation}
	\left\{\begin{array}{l}
		g_0=\tr{Q}\\
		g_l=\tr\paren{\Lambda_{l,2N+1}Q}\text{ if }l\neq 0
	\end{array}\right.
\end{equation}
Using the same idea, we transform the other semi infinite constraints, but there is opposite sign in front of $g_l$ when $l\neq0$.

We can thus state the equivalent problem of (\ref{eq:semi inf}) but with two additional Hermitian SDP variables:
\begin{equation}\label{eq:sdp-cmv}
	\begin{array}{cc}
		\underset{a,\theta,\mathcal{Q}_1,\mathcal{Q}_2,c_m(V)}{\min} & \theta\\
		\text{ s.t. } & \left\{\begin{array}{l}
			Q^Ta\leq M, -N^2M(a)+K(a)\succeq 0, M(a)\succeq 0\\
			K_m(a)c_m(v)=imc_m(f),\forall m\\
			\theta=\tr\paren{\mathcal{Q}_1}=\tr\paren{\mathcal{Q}_2}\\
			\sum_{n+m=l}c_n(f)^Tc_m(v)=\tr\paren{\Lambda_{l,2N+1}\mathcal{Q}_1}=-\tr\paren{\Lambda_{l,2N+1}\mathcal{Q}_2},\forall l\neq 0\\
			\mathcal{Q}_1\succeq 0, \mathcal{Q}_2\succeq 0
		\end{array}\right.
	\end{array}
\end{equation}

\subsection{PMI reformulation of constraints involving $c_m(v)$}\label{sc:PMI rf}
In the formulation (\ref{eq:sdp-cmv}), there are still large dimensional variables $c_m(v)$ that are results from finite element discretization. In the paper \cite{tyburec_global_2021}, the author has used the Schur's complement lemma, see the recall \ref{lm:Schur}, to remove the inverse matrices from the optimization problem. 

This lemme should also be applied here but there is a big difficulty to apply it directly mainly related to the presence of summation $\sum_{n+m=l}c_n(f)^Tc_m(v)$ in the constraints. To remove this difficulty, we seek to transform linearly the linear equalities contraints using DFT and see if something can be simplified.

By looking at the constraints:
\begin{equation}
	\left\{\begin{array}{l}
		g_0=\theta=\tr\paren{\mathcal{Q}_1}=\tr\paren{\mathcal{Q}_2}\\
		g_l=\sum_{n+m=l}c_n(f)^Tc_m(v)=\tr\paren{\Lambda_{l,2N+1}\mathcal{Q}_1}=-\tr\paren{\Lambda_{l,2N+1}\mathcal{Q}_2},\forall l\neq 0
	\end{array}\right.
\end{equation} 
We note that $\sum_{n+m=l}c_n(f)^Tc_m(v)$ corresponds to the a convolution product (see subsection (\ref{sc:dft})). Since $l$ goes from $-2N$ to $2N$ and $c_n(f)$ is defined only for $n=N,\dots,N$, we can extend $c_n(f)$ and $c_n(v)$ to $n=-2N$ and $2N$ by completing with $0$. Let's note indifferently $c(f)$ and $c(v)$ the extended sequence of complex vectors. This extension doesnot change the convolution $\sum_{n+m=l}c_n(f)^Tc_m(v)$. Ane let's note by $\hat{c}(.)$ their DFT sequence.

Now if we apply the DFT to the contraints involving $\mathcal{Q}_1$, we will see:
\begin{equation}
	\tr\paren{\Theta_{l,2N+1}\mathcal{Q}_1}=\hat{g}_l,\forall l
\end{equation}
Where $\Theta_{l,2N+1}$ is DFT sequence of the family $\Lambda_{l,2N+1}$ and $\forall l$
\begin{equation}
	\Theta_{l,2N+1}=\sum_{m=-2N}^{2N}\Lambda_{m,2N+1}\exp\paren{\frac{-2i\pi ml}{4N+1}}
\end{equation}
Similarly, by the relation between convolution-elementwise pairing under DFT,
\begin{equation}
	\hat{g}_l=\theta+\hat{c}_l(f)^T\hat{c}_l(v),\forall l
\end{equation}

By using this transformed equality constraints, the problem (\ref{eq:sdp-cmv}) becomes:
\begin{equation}
	\begin{array}{cc}
		\underset{a,\theta,\mathcal{Q}_1,\mathcal{Q}_2,c_m(v)}{\min} & \theta\\
		\text{ s.t. } & \left\{\begin{array}{l}
			Q^Ta\leq M, -N^2M(a)+K(a)\succeq 0, M(a)\succeq 0\\
			K_m(a)c_m(v)=imc_m(f),\forall m\\
			\theta+\hat{c}_l(f)^T\hat{c}_l(v)=\tr\paren{\Theta_{l,2N+1}\mathcal{Q}_1}\forall l\\
			\theta-\hat{c}_l(f)^T\hat{c}_l(v)=\tr\paren{\Theta_{l,2N+1}\mathcal{Q}_2}\forall l\\
			\mathcal{Q}_1\succeq 0, \mathcal{Q}_2\succeq 0
		\end{array}\right.
	\end{array}
\end{equation}

Since for all $a$, $c(v)$ depends linearly on $c(f)$ through the constaints $K_mc_m(v)=imc_m(f)$ and since DFT is linear, $\hat{c}(v)$ should also depends linearly on $\hat{c}(f)$. There should exist linear operators $\mathcal{L}(a)$ and $\mathcal{D}$ such that $\mathcal{L}(a)\hat{c}(v)=\mathcal{D}\hat{c}(f)$. Indeed, there are $\mathcal{L}(a)$ and $\mathcal{D}$. And they satisfy :
\begin{itemize}
	\item $\mathcal{L}(a)$ and $\mathcal{D}$ are block Toeplitz matrices with dimension $(4N+1)\times(4N+1)$ and each block shares the same dimension as $K_n(a)$. If we regard $\hat{c}(.)$ as block column vector taking $\hat{c}_l(.)$ as blocks.
	\item $\mathcal{L}(a)$ is real symmetric. $\mathcal{D}$ is constant and real antisymmetric and only depends on the number of harmonics. 
	\item $\mathcal{L}(a)$ is sdp if all the $K_n(a)$ are sdp.
	\item $\mathcal{L}(a)$ is polynomial in $a$ with degree no larger than that of $K_n(a)$.
	\item $\mathcal{L}(a)$ and $\mathcal{D}$ commute (surprisingly).
\end{itemize}
Those facts are proven through some tedious computations and are postponed in the annexe. 

Thus the problem (\ref{eq:sdp-cmv}) becomes
\begin{equation}\label{eq:sdp dft}
	\begin{array}{cc}
		\underset{a,\theta,\mathcal{Q}_1,\mathcal{Q}_2,\hat{c}(v)}{\min} & \theta\\
		\text{ s.t. } & \left\{\begin{array}{l}
			Q^Ta\leq M, -N^2M(a)+K(a)\succeq 0, M(a)\succeq 0\\
			\mathcal{L}(a)\hat{c}(v)=\mathcal{D}\hat{c}(f)\\
			\theta+\hat{c}_l(f)^T\hat{c}_l(v)=\tr\paren{\Theta_{l,2N+1}\mathcal{Q}_1}\forall l\\
			\theta-\hat{c}_l(f)^T\hat{c}_l(v)=\tr\paren{\Theta_{l,2N+1}\mathcal{Q}_2}\forall l\\
			\mathcal{Q}_1\succeq 0, \mathcal{Q}_2\succeq 0
		\end{array}\right.
	\end{array}
\end{equation}

Before applying Schur's lemma, we make some clarifications :
\begin{itemize}
	\item the DFT components $\hat{c}_l(f),\hat{c}_l(v)$ are real vectors. Indeed, since $c_m(f)=\overline{c_{-m}(f)}$ by the symmetry condition, $$
		\hat{c}_l(f)=\sum_{m}c_m(f)\exp\paren{\frac{-2i\pi ml}{4N+1}}=c_0(f)+\sum_{m\geq1}c_{-m}(f)\exp\paren{\frac{2i\pi ml}{4N+1}}+c_{m}(f)\exp\paren{\frac{-2i\pi ml}{4N+1}}
	$$
	and we note that all the terms in the summation are real.
	\item the matrices $\Theta_{l,2N+1}$ are Hermitian. In fact, $$
		\Theta_{l,2N+1}^T=\sum_{m}\Lambda_{m,2N+1}^T\exp\paren{\frac{-2i\pi ml}{4N+1}}=\sum_{m}\Lambda_{-m,2N+1}\exp\paren{\frac{-2i\pi ml}{4N+1}}=\sum_{m}\Lambda_{m,2N+1}\exp\paren{\frac{2i\pi ml}{4N+1}}
	$$
	the last step is by changing the index $m=-m$, and we note that $\Theta_{l,2N+1}^T=\overline{\Theta_{l,2N+1}}$.
	\item we can check that $\tr\paren{\Theta_{l,2N+1}\mathcal{Q}_i}$ are indeed real, using the fact that both of the matrices are Hermitian.
	\item we can identify $\hat{c}(.)$ by a column vector if we stack all the vectors $\hat{c}_m(.)$ in a column. As consequence, $\hat{c}(f)^T\hat{c}(v)=\sum_{l=-2N}^{2N}\hat{c}_l(f)^T\hat{c}_l(v)$ and $\mathcal{L}(a)$ and $\mathcal{D}$ can be represented by block matrices.
\end{itemize}

%Now let's transform the objective function so that Schur's complement lemma can be applied. For all feasible points, we note that for all $l$:
%\begin{equation}
	%\begin{split}
		%&\theta=\frac{1}{2}\tr\paren{\Theta_{l,2N+1}(\mathcal{Q}_1+\mathcal{Q}_2)}\\
		%&\hat{c}_l(f)^T\hat{c}_l(v)=\frac{1}{2}\tr\paren{\Theta_{l,2N+1}(\mathcal{Q}_1-\mathcal{Q}_2)}
	%\end{split}
%\end{equation}
%Thus:
%\begin{equation}
	%\forall l, \theta=\hat{c}_l(f)^T\hat{c}_l(v)+\tr\paren{\Theta_{l,2N+1}\mathcal{Q}_2}
%\end{equation}
%By summing for each $l$ and normalize, we obtain:
%\begin{equation}
	%\theta=\frac{1}{4N+1}\hat{c}(f)^T\hat{c}(v)+\tr\paren{\paren{\frac{1}{4N+1}\sum_{l=-2N}^{2N}\Theta_{l,2N+1}}\mathcal{Q}_2}
%\end{equation}
%And we have:
%\begin{equation}
	%\frac{1}{4N+1}\sum_{l=-2N}^{2N}\Theta_{l,2N+1}=\frac{1}{4N+1}\sum_{l}\Theta_{l,2N+1}\exp\paren{\frac{2i\pi 0l}{4N+1}}
%\end{equation}
%We recognize the inverse DFT of $\Theta_{m,2N+1}$. Thus $\frac{1}{4N+1}\sum_{l=-2N}^{2N}\Theta_{l,2N+1}=\Lambda_{0,2N+1}=I$, is the identity. As consequence, for all feasible point, the objective function is :
%\begin{equation}
	%\theta=\frac{1}{4N+1}\hat{c}(f)^T\hat{c}(v)+\tr\paren{\mathcal{Q}_2}
%\end{equation}
%The problem (\ref{eq:sdp dft}) is thus equivalent to:
%\begin{equation}
	%\begin{array}{cc}
		%\underset{a,\theta,\mathcal{Q}_1,\mathcal{Q}_2,\hat{c}(v)}{\min} & \frac{1}{4N+1}\hat{c}(f)^T\hat{c}(v)+\tr\paren{\mathcal{Q}_2}\\
		%\text{ s.t. } & \left\{\begin{array}{l}
			%Q^Ta\leq M, -N^2M(a)+K(a)\succeq 0, M(a)\succeq 0\\
			%\hat{c}(v)=\mathcal{D}^{1/2}\mathcal{L}(a)^\dagger\mathcal{D}^{1/2}\hat{c}(f)\\
			%\theta+\hat{c}_l(f)^T\hat{c}_l(v)=\tr\paren{\Theta_{l,2N+1}\mathcal{Q}_1}\forall l\\
			%\theta-\hat{c}_l(f)^T\hat{c}_l(v)=\tr\paren{\Theta_{l,2N+1}\mathcal{Q}_2}\forall l\\
			%\mathcal{Q}_1\succeq 0, \mathcal{Q}_2\succeq 0
		%\end{array}\right.
	%\end{array}
%\end{equation}
