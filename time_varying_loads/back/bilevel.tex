\section{Bilevel POP reformulation}
\subsection{Solving the matrix differentiable equation}
Being a linear system of ordinary differentiable equation, the solution $V$ of (\ref{eq:mde}) is periodic and has the same number of harmonics, thus $V(t)=\sum_{n=-N}^Nc_n(V)e^{int}$. Using (\ref{eq:mde}), we obtain:
\begin{equation}
    \left\{\begin{array}{ccc}
         K(a)c_0(V)&=&0  \\
         (-n^2M(a)+K(a))c_n(V)&=&-inc_n(f) 
    \end{array}\right.
\end{equation}
We can check handily that $(c_n(V))_n$ satisfies the symmetry condition. By the definite positiveness of each of the $-n^2M(a)+K(a)$, $c_n(V)$ are all well defined. We compute now the power received by structure:
\begin{equation}\label{eqn:power}
        f(t)^TV(t)=\sum_{n,m}c_n(f)^Tc_m(V)e^{i(n+m)t}
\end{equation}
In particular, for $n+m=0$, by Plancherel's Theorem,
$$
\sum_{n=-N}^N c_n(f)c_{-n}(V)=\sum_{n=-N}^Nc_n(f)^T\overline{c_n(V)}=\frac{1}{2\pi}\int_{0}^{2\pi}f(t)^TV(t)\mathrm{d}t.
$$
We can show that this integral is $0$ by using the matrix differential equation. In addition, we consider only the solution of (\ref{eq:mde}) where $c_0(V)=0$. In fact, if this term is non zero, there is a constant velocity at each nodal points and thus the structure is in rigid body motion that we don't consider. By using the reparameterization $z=e^{it}\in\mathbb{T}$ which is in the set of unit complex number, we obtain:
\begin{equation}
f(t)^TV(t)=\sum_{m+n\neq0}c_n(f)^Tc_m(V)z^{n+m}
\end{equation}
One can check that the summation is real, indeed, if $c_n(f)^Tc_m(V)z^{n+m}$ is in the summation, it's conjugate also by the symmetry condition.

\subsection{Minimizing $C$ as min-max problem and its semi-infinite programming reformulation}
We recall that the main optimization problem to solve is :
\begin{equation}\label{eq:main recall}
    \begin{split}
        \underset{a\in\mathbb{R}^n}{\min} & ~ C(a_1,\dots,a_n)\\
        \text{ s.t. } & Q^Ta\leq M, -N^2M(a)+K(a)\succeq 0, M(a)\succeq 0
    \end{split}
\end{equation}
For notational convenience, we have combined all the affine inequalities involving $a$ in one convex polytope constraint $Q^Ta\leq M$ for a matrix $Q$ and a vector $M$. The inequality $\leq$ is understood element-wise. As we have seen in the previous section, the objective function $C$ is precisely:
\begin{equation}
	C:a\mapsto \underset{z\in\mathbb{T}}{\max}~\Big|\sum_{m+n\neq0}c_n(f)^Tc_m(V)z^{n+m}\Big|
\end{equation}
This is equivalent to a semi-infinite programming problem (an optimization problem with an infinity numbers of contraints):
\begin{equation}
	\begin{array}{cc}
		\underset{\theta}{\min} & \theta\\
		\text{ s.t. } & \forall z\in\mathbb{T},\left\{\begin{array}{l}\theta\geq\sum_{m+n\neq0}c_n(f)^Tc_m(V)z^{n+m}\\\theta\geq-\sum_{m+n\neq0}c_n(f)^Tc_m(V)z^{n+m}\end{array}\right.
	\end{array}
\end{equation}
As consequence, the problem (\ref{eq:main recall}) is in fact equivalent to a semi-infinite programming problem:
\begin{equation}\label{eq:semi inf}
	\begin{array}{cc}
		\underset{a,\theta,c_m(V)}{\min} & \theta\\
		\text{ s.t. } & \left\{\begin{array}{l}
			Q^Ta\leq M, -N^2M(a)+K(a)\succeq 0, M(a)\succeq 0\\
			K_m(a)c_m(V)=imc_m(f),\forall m\geq 0\\
			\theta\geq\sum_{m+n\neq0}c_n(f)^Tc_m(V)z^{n+m},\forall z\in\mathbb{T}\\
			\theta\geq-\sum_{m+n\neq0}c_n(f)^Tc_m(V)z^{n+m},\forall z\in\mathbb{T}
		\end{array}\right.
	\end{array}
\end{equation}
The variables $c_m(V)$ are added as optimization variables to avoid an expression with inverse matrices, since $c_m(V)=K_m(a)^\dagger imc_m(f)$. This is practically not convenient since $c_m(V)$ are results of finite element discretization and they have large dimension. Thus, two difficulties remain in this semi-infinite programming reformulation :
\begin{itemize}
	\item we need to replace $c_m(V)$ and the constraints involving $c_m(V)$ by small dimensional PMI/LMI,
	\item we need to find finite reformulation of the last two infinit contraints.
\end{itemize}
We first solve the second issue in section \ref{sc:application sos}. The PMI/LMI reformulation is solved in the section that follows.

\subsection{Positive certificate by SOS and its SDP equivalence}\label{sc:application sos}
The infinite constraints in (\ref{eq:semi inf}) are equivalent of certifying if a trigonometric polynomial is nonnegative. As discussed in (\ref{sc:pos trigo}) and (\ref{sc:lassere trigo}), there is a strong result characterizing the \textbf{nonnegativeness} of univariate trigonometric polynomial : it is nonnegative \textit{iff} it's SOS. The SOS-ness can be checked by SDP programming.

By rearranging terms, the trigonometric polynomial $f(z)=\theta-\sum_{m+n\neq0}c_n(f)^Tc_m(V)z^{n+m}$ becomes:$$
	f(z)=\sum_{l=-2N}^{2N}g_lz^l
$$
where
\begin{align*}
	g_0&=\theta\\
	g_l&=\sum_{n+m=l}c_n(f)^Tc_m(V)
\end{align*}
By introducing the vector composed by basis monomial $\Psi(z)=(1,\dots,z^{2N})^T$, the polynomial $f$ is nonnegative on $\mathbb{T}$ \textit{iff} there is a SDP Hermitian matrix $\mathcal{Q}$ such that:$$
	f(z)=\Psi(z)^*\mathcal{Q}\Psi(z)
$$
The matrix $\mathcal{Q}$ is constrainted so that the coefficients of the two sides of the equation are identified. To see more easily the constraints, we first introduce the matrices $\Lambda_{l,d}$ for $d\geq 1$ and for $|l|\leq d-1$ such that $\forall(i,j)$ $$
\paren{\Lambda_{l,d}}_{i,j}=\left\{\begin{array}{l}
	1 \text{ if } i-j=l\\
	0 \text{ otherwise}
\end{array}\right.
$$
In particular, $\Lambda_{0,d}$ is the $d\times d$ identity matrix. Using these matrices, we note that:$$
	\Psi(z)\Psi(z)^*=\sum_{l=-2N}^{2N}\Lambda_{l,2N+1}z^l
$$
Thus we observe:$$
	\Psi(z)^*\mathcal{Q}\Psi(z)=\tr\paren{\Psi(z)\Psi(z)^*\mathcal{Q}}=\sum_{l=-2N}^{2N}\tr\paren{\Lambda_{l,2N+1}\mathcal{Q}}z^l
$$

As consequence, $f$ is nonnegative on $\mathbb{T}$ \textit{iff} there is a SDP Hermitian matrix such that
\begin{align*}
	\theta&=\tr\paren{\mathcal{Q}}\\
	\sum_{n+m=l}c_n(f)^Tc_m(V)&=\tr\paren{\Lambda_{l,2N+1}\mathcal{Q}},\forall l\geq 1
\end{align*}

At this stage, the problem (\ref{eq:semi inf}) is equivalent to :
\begin{equation}\label{eq:sdp-cmv}
	\begin{array}{cc}
		\underset{a,\theta,\mathcal{Q}_1,\mathcal{Q}_2,c_m(V)}{\min} & \theta\\
		\text{ s.t. } & \left\{\begin{array}{l}
			Q^Ta\leq M, -N^2M(a)+K(a)\succeq 0, M(a)\succeq 0\\
			K_m(a)c_m(V)=imc_m(f),\forall m\geq 0\\
			\theta=\tr\paren{\mathcal{Q}_1}=\tr\paren{\mathcal{Q}_2}\\
			\sum_{n+m=l}c_n(f)^Tc_m(V)=\tr\paren{\Lambda_{l,2N+1}\mathcal{Q}_1}=-\tr\paren{\Lambda_{l,2N+1}\mathcal{Q}_2},\forall l\geq 1\\
			\mathcal{Q}_1\succeq 0, \mathcal{Q}_2\succeq 0
		\end{array}\right.
	\end{array}
\end{equation}

\subsection{PMI reformulation of constraints involving $c_m(V)$}
The variables $c_m(V)$ have large dimension, in order to make the minimization (\ref{eq:sdp-cmv}) praticable, we should replace $c_m(V)$ by PMI/LMI variables. For each $l$, let's note by $\Delta_l\subset\{-N,\dots,N\}^2$ subset of indices such that $(n,m)\in\Delta_l\iff n+m=l$ and $\delta_l=|\Delta_l|$ the cardinality of $\Delta_l$.

By stacking $c_{l-m}(f)$ and $c_{m}(V)$ in a column vector, we note that $\forall l\geq 1$, $$
g_{l}=\sum_{m}c_{l-m}(f)^Tc_m(V)=\paren{c_{l-m}(f)}^T\paren{c_{m}(V)}
$$
By the previous notation there are $\delta_l$ terms in the summation of $g_l$. With a notation of block diagonal matrix, we note that:
\begin{equation}
	(c_{m}(V))=\paren{K_m(a)^\dagger imc_{m}(f)}=i\begin{pmatrix}\ddots&&\\
		&K_m(a)^\dagger&\\
		&&\ddots\end{pmatrix}\paren{mc_{m}(f)}
\end{equation}
Where on put all $K_m(a)^\dagger$ in a $\delta_l\times\delta_l$ block diagonal matrix. Since the pseudo inverse of a block diagonal matrix is a block diagonal matrix composed by pseudo inverse of each of the diagonal block, we obtain:
\begin{equation}
	\paren{c_{l-m}(f)}=i{\underbrace{\begin{pmatrix}\ddots&&\\
		&K_m(a)&\\
		&&\ddots\end{pmatrix}}_{=\mathcal{K}_l(a)}}^\dagger\paren{mc_{m}(f)}
\end{equation}
$\mathcal{K}_l(a)$ is a $\delta_l\times\delta_l$ block diagonal matrix. As consequence, we note that:
\begin{equation}
	g_{l}=i\paren{c_{l-m}(f)}^T\mathcal{K}_l(a)^\dagger\paren{mc_{m}(f)}
\end{equation}
Now seperating $c_m(f)=a_m(f)+ib_m(f)$ by real and complex part such that $a_{-m}(f)=a_m(f)$ and $-b_{-m}(f)=b_m(f)$ by symmetry condition, we obtain that:
\begin{align}
	\begin{split}
		g_{l}=&i\paren{a_{l-m}(f)+ib_{l-m}(f)}^T\mathcal{K}_l(a)^\dagger\paren{ma_{m}(f)+imb_{m}(f)}\\
		=&i\paren{a_{l-m}(f)}^T\mathcal{K}_l(a)^\dagger\paren{ma_{m}(f)}\\
		&-\paren{a_{l-m}(f)}^T\mathcal{K}_l(a)^\dagger\paren{mb_{m}(f)}\\
		&-\paren{b_{l-m}(f)}^T\mathcal{K}_l(a)^\dagger\paren{ma_{m}(f)}\\
		&-i\paren{b_{l-m}(f)}^T\mathcal{K}_l(a)^\dagger\paren{mb_{m}(f)}
	\end{split}
\end{align}
For instance, we introduce the symmetrizations : 
\begin{align*}
	2\paren{a_{l-m}(f)}^T\mathcal{K}_l(a)^\dagger\paren{ma_{m}(f)}=&\paren{a_{l-m}(f)+ma_{m}(f)}^T\mathcal{K}_l(a)^\dagger\paren{a_{l-m}(f)+ma_{m}(f)}\\
	&-\paren{a_{l-m}(f)}^T\mathcal{K}_l(a)^\dagger\paren{a_{l-m}(f)}\\
	&-\paren{ma_{m}(f)}^T\mathcal{K}_l(a)^\dagger\paren{ma_{m}(f)}
\end{align*}
\begin{align*}
	2\paren{a_{l-m}(f)}^T\mathcal{K}_l(a)^\dagger\paren{mb_{m}(f)}=&\paren{a_{l-m}(f)+mb_{m}(f)}^T\mathcal{K}_l(a)^\dagger\paren{a_{l-m}(f)+mb_{m}(f)}\\
	&-\paren{a_{l-m}(f)}^T\mathcal{K}_l(a)^\dagger\paren{a_{l-m}(f)}\\
	&-\paren{mb_{m}(f)}^T\mathcal{K}_l(a)^\dagger\paren{mb_{m}(f)}
\end{align*}
\begin{align*}
	2\paren{b_{l-m}(f)}^T\mathcal{K}_l(a)^\dagger\paren{ma_{m}(f)}=&\paren{b_{l-m}(f)+ma_{m}(f)}^T\mathcal{K}_l(a)^\dagger\paren{b_{l-m}(f)+ma_{m}(f)}\\
	&-\paren{b_{l-m}(f)}^T\mathcal{K}_l(a)^\dagger\paren{b_{l-m}(f)}\\
	&-\paren{ma_{m}(f)}^T\mathcal{K}_l(a)^\dagger\paren{ma_{m}(f)}
\end{align*}
\begin{align*}
	2\paren{b_{l-m}(f)}^T\mathcal{K}_l(a)^\dagger\paren{mb_{m}(f)}=&\paren{b_{l-m}(f)+mb_{m}(f)}^T\mathcal{K}_l(a)^\dagger\paren{b_{l-m}(f)+mb_{m}(f)}\\
	&-\paren{b_{l-m}(f)}^T\mathcal{K}_l(a)^\dagger\paren{b_{l-m}(f)}\\
	&-\paren{mb_{m}(f)}^T\mathcal{K}_l(a)^\dagger\paren{mb_{m}(f)}
\end{align*}
For $l\in\set{1,\dots,2N}$, we introduce for $i\in\set{1,\dots,8},\gamma_l^i$ such that:
\begin{align*}
	&\gamma_l^1=\argmin\set{\gamma\mid\begin{pmatrix}\gamma&\paren{a_{l-m}(f)+ma_{m}(f)}^T\\.&\mathcal{K}_l(a)\end{pmatrix}\succeq0}\\
	&\gamma_l^2=\argmin\set{\gamma\mid\begin{pmatrix}\gamma&\paren{a_{l-m}(f)+mb_{m}(f)}^T\\.&\mathcal{K}_l(a)\end{pmatrix}\succeq0}\\
	&\gamma_l^3=\argmin\set{\gamma\mid\begin{pmatrix}\gamma&\paren{b_{l-m}(f)+ma_{m}(f)}^T\\.&\mathcal{K}_l(a)\end{pmatrix}\succeq0}\\	
	&\gamma_l^4=\argmin\set{\gamma\mid\begin{pmatrix}\gamma&\paren{b_{l-m}(f)+mb_{m}(f)}^T\\.&\mathcal{K}_l(a)\end{pmatrix}\succeq0}\\	
	&\gamma_l^5=\argmin\set{\gamma\mid\begin{pmatrix}\gamma&\paren{a_{l-m}(f)}^T\\.&\mathcal{K}_l(a)\end{pmatrix}\succeq0}\\
	&\gamma_l^6=\argmin\set{\gamma\mid\begin{pmatrix}\gamma&\paren{ma_{m}(f)}^T\\.&\mathcal{K}_l(a)\end{pmatrix}\succeq0}\\
	&\gamma_l^7=\argmin\set{\gamma\mid\begin{pmatrix}\gamma&\paren{b_{l-m}(f)}^T\\.&\mathcal{K}_l(a)\end{pmatrix}\succeq0}\\
	&\gamma_l^8=\argmin\set{\gamma\mid\begin{pmatrix}\gamma&\paren{mb_{m}(f)}^T\\.&\mathcal{K}_l(a)\end{pmatrix}\succeq0}
\end{align*}
And so $g$ are reformulated as :
$$
	g_{l}=-\frac{1}{2}\paren{\gamma_l^2+\gamma_l^3-\gamma_l^5-\gamma_l^6-\gamma_l^7-\gamma_l^8}+\frac{i}{2}\paren{\gamma_l^1-\gamma_l^4-\gamma_l^5-\gamma_l^6+\gamma_l^7+\gamma_l^8}
$$
For notational convenience, we let $\gamma_l^T=(\gamma_l^i)_{i=1\dots,8}$ and $\lambda^*=\paren{\frac{i}{2},-\frac{1}{2},-\frac{1}{2},-\frac{i}{2},\frac{1-i}{2},\frac{1-i}{2},\frac{1+i}{2},\frac{1+i}{2}}$, we have:
$$
	g_{l}=\lambda^*\gamma_l
$$
\subsection{Bilevel POP reformulation}
We can finally state the Bilevel POP reformulation of the problem (\ref{eq:sdp-cmv}):
\begin{equation}
	\begin{array}{cc}
		\underset{a,\theta,\mathcal{Q}_1,\mathcal{Q}_2,\gamma^i}{\min} & \theta\\
		\text{ s.t. } & \left\{\begin{array}{l}
			Q^Ta\leq M, -N^2M(a)+K(a)\succeq 0, M(a)\succeq 0\\
			\theta=\tr\paren{\mathcal{Q}_1}=\tr\paren{\mathcal{Q}_2}\\
			\lambda^*\gamma_l=\tr\paren{\Lambda_{l,2N+1}\mathcal{Q}_1}=-\tr\paren{\Lambda_{l,2N+1}\mathcal{Q}_2},\forall l\geq 1\\
			\mathcal{Q}_1\succeq 0, \mathcal{Q}_2\succeq 0\\
			(\gamma^i)\in\Gamma(a)
		\end{array}\right.
	\end{array}
\end{equation}

This formulation requires two sdp Hermitian matrix, to simply this, we can see the previous problem as :
\begin{equation}\label{eq:two step min}
	\begin{array}{cc}
		\underset{a,\theta,\gamma^i}{\min} & \mathcal{J}(\theta, \gamma^i)\\
		\text{ s.t. } & \left\{\begin{array}{l}
			Q^Ta\leq M, -N^2M(a)+K(a)\succeq 0, M(a)\succeq 0\\
			(\gamma^i)\in\Gamma(a)
		\end{array}\right.
	\end{array}
\end{equation}
where $\mathcal{J}$ stands for a parametric SDP problem :$$
	\mathcal{J}:\theta,\gamma^i\mapsto\begin{array}{cc}
		\underset{\mathcal{Q}_1,\mathcal{Q}_2}{\min} & \theta\\
		\text{ s.t. }& \left\{\begin{array}{l}
			\theta=\tr\paren{\mathcal{Q}_1}=\tr\paren{\mathcal{Q}_2}\\
			\lambda^*\gamma_l=\tr\paren{\Lambda_{l,2N+1}\mathcal{Q}_1}=-\tr\paren{\Lambda_{l,2N+1}\mathcal{Q}_2},\forall l\geq 1\\
			\mathcal{Q}_1\succeq 0, \mathcal{Q}_2\succeq 0\\
		\end{array}\right.
	\end{array}
$$
We look for a dual formulation of the parametric problem, with real varibles. \textcolor{red}{Strong duality ? Well it usually happens for SDP programming...}. The complexity are found mainly in the constraints $\lambda^*\gamma_l=\tr\paren{\Lambda_{l,2N+1}\mathcal{Q}_1}=-\tr\paren{\Lambda_{l,2N+1}\mathcal{Q}_2}$, if we split $\lambda^*\gamma_l$ into real and imaginary part and use the fact that $\Lambda_{-l,d}=\Lambda_{l,d}^T$ and $\mathcal{Q}_i$ are Hermitian, we notice that:
\begin{align*}
	\Re\tr\paren{\Lambda_{l,2N+1}\mathcal{Q}_i}=\tr\paren{\frac{\Lambda_{l,2N+1}+\Lambda_{-l,2N+1}}{2}\mathcal{Q}_i}\\
	\Im\tr\paren{\Lambda_{l,2N+1}\mathcal{Q}_i}=\tr\paren{\frac{\Lambda_{l,2N+1}-\Lambda_{-l,2N+1}}{2i}\mathcal{Q}_i}
\end{align*}
As consequence, we can state the following equivalent problem:
\begin{equation}
	\begin{array}{cc}
		\underset{\mathcal{Q}_1,\mathcal{Q}_2}{\min} & \theta\\
		\text{ s.t. }& \left\{\begin{array}{l}
			\theta=\tr\paren{\mathcal{Q}_1}=\tr\paren{\mathcal{Q}_2}\\
			\Re\lambda^*\gamma_l=\tr\paren{\frac{\Lambda_{l,2N+1}+\Lambda_{-l,2N+1}}{2}\mathcal{Q}_1}=-\tr\paren{\frac{\Lambda_{l,2N+1}+\Lambda_{-l,2N+1}}{2}\mathcal{Q}_2},\forall l\geq 1\\
			\Im\lambda^*\gamma_l=\tr\paren{\frac{\Lambda_{l,2N+1}-\Lambda_{-l,2N+1}}{2i}\mathcal{Q}_1}=-\tr\paren{\frac{\Lambda_{l,2N+1}-\Lambda_{-l,2N+1}}{2i}\mathcal{Q}_2},\forall l\geq 1\\
			\mathcal{Q}_1\succeq 0, \mathcal{Q}_2\succeq 0\\
		\end{array}\right.
	\end{array}
\end{equation}
Whose dual is:
\begin{equation}
	\begin{array}{cc}
		\underset{\mu_0,\mu_1,z^1,\dots,z^4}{\max} & (1-\mu_0-\mu_1)\theta+\sum_{l=1}^{2N}(z^1_l+z^2_l)\Re\lambda^*\gamma_l+(z^3_l+z^4_l)\Im\lambda^*\gamma_l\\
		\text{ s.t. }& \left\{\begin{array}{l}
			\mu_0I-\sum_{l=1}^{2N}\frac{\Lambda_{l,2N+1}+\Lambda_{-l,2N+1}}{2}z_l^1+\frac{\Lambda_{l,2N+1}-\Lambda_{-l,2N+1}}{2i}z_l^3\succeq 0\\
			\mu_1I+\sum_{l=1}^{2N}\frac{\Lambda_{l,2N+1}+\Lambda_{-l,2N+1}}{2}z_l^2+\frac{\Lambda_{l,2N+1}-\Lambda_{-l,2N+1}}{2i}z_l^4\succeq 0
		\end{array}\right.
	\end{array}
\end{equation}
If strong duality holds for each of the feasible $\theta,\gamma^i$, $\mathcal{J}(\theta,\gamma^i)$ takes the same value as the dual and thus the problem (\ref{eq:two step min}) becomes:
\begin{equation}
	\begin{array}{cc}
		\underset{a,\theta,\gamma^i,\mu_i,z^i}{\min} & (1-\mu_0-\mu_1)\theta+\sum_{l=1}^{2N}(z^1_l+z^2_l)\Re\lambda^*\gamma_l+(z^3_l+z^4_l)\Im\lambda^*\gamma_l\\
		\text{ s.t. } & \left\{\begin{array}{l}
			Q^Ta\leq M, -N^2M(a)+K(a)\succeq 0, M(a)\succeq 0\\
			(\gamma^i)\in\Gamma(a)\\
			(\mu_i,z^i)\in Z(\theta,\gamma^i)=\argmax\{\dots\}
		\end{array}\right.
	\end{array}
\end{equation}

The number of variables is as consequence:$$\#(a)+\#(\theta)+\#(\gamma^i)+\#(\mu_i)+\#(z^i)=r+1+8\times 2N+2+4\times 2N$$
