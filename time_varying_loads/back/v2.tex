\documentclass[11pt,a4paper]{article}

\voffset=-1.5cm \hoffset=-1.4cm \textwidth=16cm \textheight=22.0cm

\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{mathrsfs}
\usepackage{enumerate}
\usepackage{lscape}
\usepackage{longtable}
\usepackage{rotating}
\usepackage{multirow}
\usepackage{color}
\usepackage{xcolor}
\usepackage{url}
\usepackage{subfigure}
\usepackage{rotating}
\usepackage{hyperref}

\usepackage[english]{babel}
\usepackage{biblatex}
\usepackage{blkarray}% http://ctan.org/pkg/blkarray

\addbibresource{../data/main.bib}

\hypersetup{
    colorlinks=true, %set true if you want colored links
    linktoc=all,     %set to all if you want both sections and subsections linked
    linkcolor=blue,  %choose some color if you want links to stand out
}
\newcommand{\matindex}[1]{\mbox{\scriptsize#1}}% Matrix index
\newtheorem{theorem}{Theorem}[section]
\newtheorem{acknowledgment}{Acknowledgment}[section]
\usepackage[ruled,vlined,noline,linesnumbered]{algorithm2e}
\newtheorem{axiom}{Axiom}[section]
\newtheorem{case}{Case}[section]
\newtheorem{claim}{Claim}[section]

\newtheorem{conclusion}{Conclusion}[section]
\newtheorem{condition}{Condition}[section]
\newtheorem{conjecture}{Conjecture}[section]
\newtheorem{corollary}{Corollary}[section]
\newtheorem{criterion}{Criterion}[section]
\newtheorem{definition}{Definition}[section]
\newtheorem{example}{Example}[section]
\newtheorem{exercise}{Exercise}[section]
\newtheorem{lemma}{Lemma}[section]
\newtheorem{notation}{Notation}[section]
\newtheorem{problem}{Problem}[section]
\newtheorem{proposition}{Proposition}[section]
\newtheorem{remark}{Remark}[section]
\newtheorem{solution}{Solution}[section]
\newtheorem{assumption}{Assumption}[section]
\newtheorem{summary}{Summary}[section]
\newtheorem{note}{Note}[section]
\newtheorem{doubt}{Doubt}[section]
\newtheorem{properties}{Properties}[section]
\newenvironment{proof}[1][Proof]{\textbf{#1.} }{\ \rule{0.5em}{0.5em} \vspace{1ex}}

\newcommand{\udef}{\mathrel{\mathop:}=}
\newcommand{\defu}{=\mathrel{\mathop:}}
\newcommand{\dd}{{\rm d}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\C}{\mathbb{C}}
\newcommand{\de}{\mathrm{d}}
\usepackage{dsfont}
\newcommand{\1}{\mathds{1}}
\newcommand{\partialc}{\partial^c} 
\newcommand{\parder}[2]{\frac{\partial#1}{\partial#2}}
\newcommand{\der}[2]{\frac{\de#1}{\de#2}}
% Def abs e norm

\newcommand{\MT}[1]{\textcolor{cyan}{[Marek comment]: #1}}

\usepackage{algorithmic}

\setlength{\unitlength}{1mm}

\def\real{\R}

\DeclareMathOperator{\argmax}{argmax}
\DeclareMathOperator{\argmin}{argmin}
\DeclareMathOperator{\range}{Range}
\DeclareMathOperator{\kernel}{Ker}
\DeclareMathOperator{\cl}{cl}

\addbibresource{../data/main.bib}

\begin{document}

\title{Structural Engineering for Timevarying Loads \\ with a Finite Number of Harmonics}

% Saman Khorramian,Vyacheslav Kungurtsev, Jakub Marecek
\author{ Saman Khorramian, Shenyuan Ma, Allahkaram Shafie,  \\ Vyacheslav Kungurtsev, Marek Tyburec, Jakub Marecek
\thanks{All authors are at the Czech Technical University.}}% <-this % stops a space
\maketitle

\begin{abstract}

\end{abstract}

 \section{Introduction} 
Throughout, $M(a)$ and $K(a)$ are matrices and $f(t)$ is a vector function.
 These are assumptions:
 \begin{itemize}
	\item[\it (i)]
The vector function $f(t)$ is the vector of forces applied to nodes.
	\item[\it (ii)]
The mass matrices of the truss: $M(a)=a_1M_1+\dots+a_nM_n$ and $\forall i, M_i\succeq 0$ and $a_i \geq 0$ is the cross section aera of the truss.
	\item[\it (iii)]
The stiffness matrices of the truss: $K(a)=a_1K_1+\dots+a_nK_n$ and $\forall i, K_i\succeq 0$.
	\item[\it (iv)]
The vector function $V(t)$ is the vector of nodal velocities and is obtained from the following Matrix Differential Equation: 
\begin{equation}\label{eq:mde}
    M(a)V^{\prime \prime}(t)+K(a)V(t)=f^{\prime}(t)
\end{equation}
	\item[\it (v)]
$C(a_1,\dots,a_n):=\text{limsup}_{t \rightarrow \infty}{f(t)}^{T}V(t)$ (Peak steady-state power delivered to the stucture)
\item[\it (vi)]
The mass of the structure $q_1a_1+\dots+q_na_n$ is lower than $m$.
\item[\it (vii)]
The components of the vector function $f(t)$ have finite Fourier expansions. There is a sequence of complex vectors $(c_n(f))_{n=-N,\dots,N}$ such that
$$
f(t)=\sum_{n=-N}^{N}c_n(f)e^{int}.
$$
In addtion, $f$ being a real valued function, the symmetry condition of the Fourier coefficients is satisfied, namely, $c_n(f)=\overline{c_{-n}(f)}$.
\item[\it (viii)]
We assume that the largest frequency of the load $f$ is inferior to the smallest natural frequency of the structure, which implies $K(a)-N^2M(a)\succ0$.
\end{itemize}
With these assumptions, we want to solve the following minimization problem:
\begin{align}\label{eq:main}
    \begin{split}
        \underset{a\in\mathbb{R}^n}{\min} & ~ C(a_1,\dots,a_n)\\
        \text{ s.t. } & \left\{\begin{array}{c}
             \forall i, a_i \geq 0\\
             a_1q_1+\dots+a_nq_n \leq m\\
             -N^2M(a)+K(a)\succ 0
        \end{array}\right.
    \end{split}
\end{align}
The problam (\ref{eq:main}) will be reformulated as a bilevel polynomial optimization problem and we will apply the results of \cite{jeyakumar_convergent_2016} to propose an algorithm to search for optimal cross section aeras design to minimize the peak power received by the structure.

\section{Bilevel POP reformulation}
\subsection{Solving the matrix differentiable equation}
Being a linear system of ordinary differentiable equation, the solution $V$ of (\ref{eq:mde}) is periodic and has the same number of harmonics, thus $V(t)=\sum_{n=-N}^Nc_n(V)e^{int}$. Using (\ref{eq:mde}), we obtain:
\begin{equation}
    \left\{\begin{array}{ccc}
         K(a)c_0(V)&=&0  \\
         (-n^2M(a)+K(a))c_n(V)&=&-inc_n(f) 
    \end{array}\right.
\end{equation}
We can check handily that $(c_n(V))_n$ satisfies the symmetry condition. By the definite positiveness of each of the $-n^2M(a)+K(a)$, $c_n(V)$ are all well defined. We compute now the power received by structure:
\begin{align}\label{eqn:power}
    \begin{split}
        f(t)^TV(t)&=\sum_{n,m}c_n(f)^Tc_m(V)e^{i(n+m)t}\\
                &=\sum_{l=-2N}^{2N}\sum_{n+m=l}c_n(f)^Tc_m(V)e^{ilt}.
    \end{split}
\end{align}
In particular, for $l=0$, by Plancherel's Theorem,
$$
\sum_{n=-N}^N c_n(f)c_{-n}(V)=\sum_{n=-N}^Nc_n(f)^T\overline{c_n(V)}=\frac{1}{2\pi}\int_{0}^{2\pi}f(t)^TV(t)\mathrm{d}t.
$$
We can show that this integral is $0$ by using the matrix differential equation. 

Now we can put the equation (\ref{eqn:power}) into the form of a trigonometric polynomial with real coefficients. Let $g(t)=f(t)^TV(t)$ and $g(t)=a_0(g)+\sum_{l=1}^{2N}a_l(g)\cos(lt)+b_l(g)\sin(lt)$, by the relation between complex and real Fourier coefficients, we derive :
\begin{equation}
    \left\{\begin{array}{c}
         a_0(g)=c_0(g)=0\\
         a_l(g)=2\Re(c_l(g))=2\Re(\sum_{n+m=l}c_n(f)^Tc_m(V))\\
         b_l(g)=-2\Im(c_l(g))=-2\Im(\sum_{n+m=l}c_n(f)^Tc_m(V))
    \end{array}\right..
\end{equation}
Furthermore, let $c_n(f)=A_n(f)+B_n(f)i$, by symmetric condition, $A_{-n}=A_n$ and $B_{-n}=-B_n$ and let $\Gamma_m(a)=-m^2M(a)+K(a)$, we have:
\begin{align}
    \begin{split}
        \sum_{n+m=l}c_n(f)^Tc_m(V)&=\sum_{n+m=l}(A_n(f)+iB_n(f))^T\Gamma_m(a)^{-1}m(iA_m(f)-B_m(f))\\
        &=\sum_{n+m=l}m\big(-B_n(f)^T\Gamma_m(a)^{-1}A_m(f)-A_n(f)^T\Gamma_m(a)^{-1}B_m(f)\big)+\\
        &i\sum_{n+m=l}m\big(A_n(f)^T\Gamma_m(a)^{-1}A_m(f)-B_n(f)^T\Gamma_m(a)^{-1}B_m(f)\big)
    \end{split}
\end{align}
Which implies:
\begin{equation}\label{eq:albl}
    \left\{\begin{array}{cc}
         a_l(g)&=2\sum_{n+m=l}m\big(-B_n(f)^T\Gamma_m(a)^{-1}A_m(f)-A_n(f)^T\Gamma_m(a)^{-1}B_m(f)\big) \\
         b_l(g)&=-2\sum_{n+m=l}m\big(A_n(f)^T\Gamma_m(a)^{-1}A_m(f)-B_n(f)^T\Gamma_m(a)^{-1}B_m(f)\big) 
    \end{array}\right.
\end{equation}

We try now to remove the inverse matrices from previous expression. Since the inverse is a polynomial of original matrix up to a constant (which is the determinant), there exists matrices $\mathcal{C}_n(a)$ for all $n$ depending polynomially on $a$ such that :
$$
\Gamma_n(a)^{-1}=\frac{1}{\det(-n^2M(a)+K(a))}\mathcal{C}_n(a).
$$
The degree of polynomial matrices $\mathcal{C}_n$ can be up to $d$, which is equal to the dimension of the mass and stiffness matrices. This approach is mathematically strong but impracticable. 

Moreover, but the positiveness of $\Gamma_n(a)$, $\mathcal{C}_n(a)$ is also sdp. Since it commutes with $\Gamma_n(a)$ as a matrix commutes with its inverse and $\mathcal{C}_n(a)$ is the inverse multiplied by a scalar. We conclude that $\mathcal{C}_n(a)$ is diagonalized in the same basis as $\Gamma_n(a)$ is diagonalized. Let $w$ be a eigenvector of $\Gamma_n(a)$ with eigenvalue $\omega^2>0$, then:
$$
\mathcal{C}_n(a)w=\det(-n^2M(a)+K(a))\Gamma_n(a)^{-1}w=\frac{\det(-n^2M(a)+K(a))}{\omega^2}w.
$$
Let $(\omega_{n,j}^2(a))_{j=1,\dots,d}$ be the sequence of eigenvalues of $\Gamma_n(a)$, we conclude from the previous computation that the spectrum of $\mathcal{C}_n(a)$ is:
$$
\text{sp}(\mathcal{C}_n(a))=\{\forall j\in\{1,\dots,d\},\prod_{i\neq j}\omega_{n,i}^2(a)\}.
$$
\subsection{Bilevel POP}
We recall that the main optimization problem to solve is :
\begin{align*}
    \begin{split}
        \underset{a\in\mathbb{R}^n}{\min} & ~ C(a_1,\dots,a_n)\\
        \text{ s.t. } & \left\{\begin{array}{c}
             \forall i, a_i \geq 0\\
             a_1q_1+\dots+a_nq_n \leq m
        \end{array}\right.
    \end{split}
\end{align*}
As we have seen in the previous sub-section, the objective function is the maximal value of a trigonometric polynomial, parameterized by $a$:
\begin{equation*}
C(a)=\underset{t\in[0,2\pi]}{\max}\sum_{l=1}^{2N}a_l(g)[a]\cos(lt)+b_l(g)[a]\sin(lt)
\end{equation*}
By trigonometric identities, we know that $\cos^2(t)+\sin^2(t)=1$ and there exists Bivariate polynomials $P_l$ and $Q_l$ such that :
\begin{equation}
    \forall l\geq 2,\left\{\begin{array}{cc}
         \cos(lt)&=P_l(\cos(t), \sin(t))  \\
         \sin(lt)&=Q_l(\cos(t), \sin(t)) 
    \end{array}\right.
\end{equation}
They can be obtained by applying Newton's binomial formula to
$$
\cos(lt)+i\sin(lt)=(\cos(t)+i\sin(t))^l,
$$
where $i$ denote the complex unit $i^2=-1$, or they can be derived inductively such that :
$$
\left\{\begin{array}{c}
    P_1(\cos(t),\sin(t))=\cos(t)\\
    Q_1(\cos(t),\sin(t))=\sin(t)\\
    P_{l+1}(\cos(t),\sin(t))=P_l(\cos(t),\sin(t))\cos(t)-Q_l(\cos(t),\sin(t))\sin(t)\\
    Q_{l+1}(\cos(t),\sin(t))=P_l(\cos(t),\sin(t))\sin(t)+Q_l(\cos(t),\sin(t))\cos(t)
\end{array}\right.
$$
Thus $C(a)$ can be reformulated as the following fractional optimization problem due to the denominators $\det(-n^2M(a)+K(a))$:
\begin{equation}\label{eq:ca_pop}
C(a)=\left\{\begin{array}{cc}
     \underset{(x,y)\in\mathbb{R}^2}{\max} & \sum_{l=1}^{2N}a_l(g)P_l(x,y)+b_l(g)Q_l(x,y)  \\
     \text{ s.t. } & x^2+y^2=1 
\end{array}\right.
\end{equation}
The equality constraint $x^2+y^2=1$ can be replace by $x^2+y^2\leq 1$. In fact, due to the complex differentiability of the function $z\mapsto z^l$ for all $l$, $P_l$ and $Q_l$ satisfy Cauchy-Riemann equations and since they are smooth functions, they are solution of the Laplace equation to which maximum value principle applies, the maximum over the unit ball is taken on the boundary. The function being maximized for each feasible $a$ is a linear combination of $P_l$ and $Q_l$ which is still solution of the Laplace equation. Thus we obtain :
\begin{equation}
C(a)=\left\{\begin{array}{cc}
     \underset{(x,y)\in\mathbb{R}^2}{\max} & \sum_{m=1}^{2N}a_l(g)P_l(x,y)+b_l(g)Q_l(x,y)  \\
     \text{ s.t. } & x^2+y^2 \leq 1.
\end{array}\right.
\end{equation}
This is an important observation since the results in \cite{jeyakumar_convergent_2016} for non convex Bilevel POP require non empty interior of the feasible set.

Finally, we introduce additional variables $p\in\mathbb{R}^{N}$ satisfying the equality constraints $p_n\det(-n^2M(a)+K(a))=1$, the main problem thus becomes a bilevel POP:
{\everymath={\displaystyle}
\begin{equation}\label{eq:original pop}
    \begin{array}{cc}
         \underset{a,p,x,y}{\min}& \sum_{l=1}^{2N}a_l(g)P_l(x,y)+b_l(g)Q_l(x,y) \\ 
         \text{ s.t. }& \left\{\begin{array}{c}
              a\geq 0, a^Tq\leq m,-N^2M(a)+K(a)\succ 0,\\
              \forall n, p_n\det(-n^2M(a)+K(a))=1,\\
              (x,y)\in\underset{u,v}{\argmax}\{\sum_{l=1}^{2N}a_l(g)P_l(u,v)+b_l(g)Q_l(u,v), u^2+v^2\leq 1\} 
         \end{array}\right.
    \end{array}
\end{equation}
}
where $a_l(g)$ and $b_l(g)$ depends polynomially on $a$ and $p$.
\subsection{Compactify the feasible set}
Note that the Bilevel POP as defined in (\ref{eq:original pop}) doesn't have a compact feasible set mainly due to the equality constraints $p_n\det(-n^2M(a)+K(a))=1$. When $\det(-n^2M(a)+K(a))$ has small eigenvalue, $p_n$ can be very large. We would like to derive some bounds to $p_n$ by assuming that there is indeed a global solution. The derived bounds (independant of the solution) will be added to the feasible set so that it becomes compact.
\subsubsection{Lower bounds of $p_n$}
Recall that for all $n$, $-n^2M(a)+K(a)$ is a $d\times d$ definite positive matrice thus $\det(-n^2M(a)+K(a))\leq \lambda_n(a)^d$ where $\lambda_l$ is the largest eigenvalue of $\det(-n^2M(a)+K(a))$, which depends continuously on $a$, that belongs to a compact set $\mathcal{A}=\{a\geq 0, a^Tq\leq m\}$. Thus $\lambda_n(a)$ reaches the maximal value $\lambda_n^*>0$ over the set of feasible $a$. Thus 
$$
\forall n, p_n\geq\frac{1}{\lambda_n^{*d}}
$$
\subsubsection{Upper bounds of $p_n$}
We assume that $(a^*,p^*,x^*,y^*)$ is in fact a global solution of (\ref{eq:original pop}), then for a fixed feasible point $(a^0,p^0,x^0,y^0)$ and $(u,v)\in\{u^2+v^2=1\}$ it satisfies:
\begin{align}
    \begin{split}
        &\sum_{m=1}^{2N}a_m(g)[a^0,p^0]P_m(x^0,y^0)+b_m(g)[a^0,p^0]Q_m(x^0,y^0)\\
        \geq &\sum_{m=1}^{2N}a_m(g)[a^*,p^*]P_m(x^*,y^*)+b_m(g)[a^*,p^*]Q_m(x^*,y^*)\\
        \geq &\sum_{m=1}^{2N}a_m(g)[a^*,p^*]P_m(u,v)+b_m(g)[a^*,p^*]Q_m(u,v).
    \end{split}
\end{align}
The first inequality is due to the global optimality of $(a^*,p^*,x^*,y^*)$ and the second inequality is because the $(x^*,y^*)$ is the solution of the lower level problem at $(a^*,p^*)$. By the computation in (\ref{sec:ca}), the inequalities are linear with respect to $p_l$. By using some notationally abusive expressions, we observe that $a_m(g)$ and $b_m(g)$ are linear combinaitions of terms $\epsilon_k^1(f)^T\epsilon_m^2(V)$ where $\epsilon^i$ are replaced by $a$ or $b$. Those terms depends linearly on $p_l$. Thus there is a vector function $(a,u,v)\mapsto W(a,u,v)$ such that $$
\sum_{m=1}^{2N}a_m(g)[a^*,p^*]P_m(u,v)+b_m(g)[a^*,p^*]Q_m(u,v)=W(a^*,u,v)^Tp^*
$$

\section{Example}
\subsection{Example in 1D}
We consider a toy example where $f$ and $V$ are real scalar functions: $M(a,b)=a+b,~K(a,b)=2a+3b,~q_1=5,~q_2=7,~m=12,~f(t)=3-\sin t+4\cos t+2\sin 2t-\cos 2t$
\subsection{Example in 2D}
We consider $f$ vector function in $\mathbb{R}^2$.

\section{Ideals}
\subsection{Remove matrix inverse}
In our discussion, so far, we rely on the inverse of the matrices $-k^2M(a)+K(a)$ to express the objective function. This may have those problems :
\begin{itemize}
    \item they introduce large degree polynomials. Typically, if $f$ is $d$ dimensional, the inverse of the matrices $-k^2M(a)+K(a)$ introduce polynomial of degree $d$. 
    \item It's not garanteed to have those inverses for larger scale problem, even if it's possible, it would be necessary to exploit sparsity.
    \item $-k^2M(a)+K(a)$ could be singular for more general case, as discussed in \cite{tyburec_global_2021}. Again, calculating explicitly the pseudo inverse could be impossible due to the scale of the problem.
\end{itemize}

We might be able to improve our method by using Schur's complement, since it converts some types of expression that involves inverse of matrices into LMI or PMI. Let $M\succeq 0$ with pseudo inverse $M^\dagger$ and let $f$ be a vector in the range of $M$. Then the quadratic form $f\mapsto f^TM^{\dagger}f$ can be seen as the solution of the minimization problem :
$$
f^TM^{\dagger}f=\underset{\lambda}{\min}\{\lambda | \lambda-f^T M^{\dagger}f \geq 0, f\in\range(M),M\succeq 0\}
$$
The constraints are equivalent to a single matrice inequality, by virtus of Schur's decomposition :
$$
\lambda-f^T M^{\dagger}f \geq 0, f\in\range(M),M\succeq 0 \iff \begin{bmatrix}
    \lambda & f^T \\
    f & M
\end{bmatrix}\succeq 0
$$
This is very interesting in the case where $M$ and $f$ are polynomials, since the matrix inequality that we derive in the end is under the form of LMI or PMI whose degrees donot depend on the dimension of the matrix.

Consider now the bilinear form defined by $M^\dagger$, for all pair of vectors $f,g$, we are interested in the expression $g^T M^\dagger f$. $M$ being a symmetric matrix, $M^\dagger$ is also a symmetric matrix, thus:
$$
g^T M^\dagger f =\frac{1}{2}\Big((f+g)^TM^\dagger(f+g)-g^TM^\dagger g^T-f^T M^\dagger f\Big)
$$
for each term of the right-hand side of the identity, we introduce $\lambda_1,\lambda_2,\lambda_3$ in a similar manner such that :
$$
g^T M^\dagger f=\frac{1}{2}(\lambda_1-\lambda_2-\lambda_3).
$$

This idea, especially the case of bilinear form, could be applied to the expression of $f(t)^T v(t)$. We will be possibly dealing with a bilevel POP problem but with several lower level problems and we are in the case of general constraints $h(x,w)$ as described in \cite{jeyakumar_convergent_2016}.

\subsection{Dealing with higher frequency load}
The discussion of previous subsub section is only valide for sdp matrices. If we add the constraints $K(a)-k^2M(a)\succeq 0$ for all $k$, we expect that highest frequency of the load is inferior to the smallest natural frequency of the structure. Indeed, only the constraint $K(a)-N^2M(a)$ is required to ensure sdp-ness of every matrix, if $N$ is the highest frequency number. To see this, we note that :
$$
K-k^2M=K-(k+1)^2M+\underbrace{(2k+1)M}_{\text{is sdp matrix}},
$$
then $K(a)-k^2M(a)\succeq K(a)-(k+1)^2M(a)$, by induction, $K(a)-k^2M(a)\succeq K(a)-N^2M(a)$

To bypass this limitation, we might propose to find an exact formula or an approximation of the pseudo inverse of $K(a)-k^2M(a)$ using pseudo-inverse of $K$ and $M$, which are sdp matrices. Next step, we apply the idea previously discussed. 

\MT{The case when the resonance frequency is higher than the frequencies of the loads obtained by Fourier expansion is not entirely general but in several applications such cases occur. Thus, while $K-k^2M\succeq0$ is limiting, it still makes sense to discuss/try computationally.}

\subsection{LP expression of the lower problem}
The lower problem can be reduced to optimization of a linear function over a non linear feasible set. Let $(a_i)\in\mathbb{R}^n$ be the vector of optimization variables, if $f$ is composed by $N$ harmonics, then there are sequences of functions $\alpha_k$ and $\beta_k$ for $k=1,...,2N$ such that:
$$
f(t)^Tv(t)=\sum_{k=1}^{2N} \alpha_k(a)\cos(kt)+\beta_k(a)\sin(kt)
$$

The lower level problem is to solve for each feasible $a$
$$
C(a)=\underset{t}{\max} ~ f(t)^Tv(t),
$$
the first idea was to replace $\cos(kt)$ and $\sin(kt)$ by polynomials in terms of $\cos(t)$ and $\sin(t)$. Now we propose to replace each of the $\cos(kt)$ and $\sin(kt)$ by a pair of variables $x_k$ and $y_k$ and add equality constraints to copy exactly the behaviour of trigonometric functions. Considering $x,y\in\mathbb{R}^{2N}$, we propose the following problem :
\begin{align}
    \begin{split}
        \underset{x,y}{\max} & ~\alpha(a)^Tx+\beta(a)^Ty \\
        \text{ s.t. } & \left\{\begin{array}{c}
             x_1^2+y_1^2=1\\
             x_{i+1} = x_{i}x_1-y_{i}y_{1}, \forall i=1,\dots,2N-1\\
             y_{i+1} = x_{i}y_1+y_{i}x_1,\forall i=1,\dots,2N-1
        \end{array}\right.
    \end{split}
\end{align}
which consists of maximizaing a linear function over a non linear feasible set. If there is indeed a solution, we can find $t\in[0,2\pi]$ such that $\cos(jt)=x_j,\sin(jt)=y_j$ for every $j$.

It seems that this new formulation can be solved more efficiently, even if the number of variables and of equality constraints increased. In exchange, the objective function is linear in $x,y$.

\subsection{Maximizing absolute value}\label{sec:max abs}
In fact, in the litterature, the peak power is defined as the maximal value of the absolute value of $f(t)^TV(t)$, this is ignored in the current draft but it's possible to fix it.

Let's define an abstract constrainted optimization problem :
\begin{equation}\label{eq:max absf}
    \begin{array}{cc}
        \underset{x}{\max} & |f(x)|  \\
         \text{ s.t. } & x\in X
    \end{array}
\end{equation}

The first approach is due to the monotony of $x\mapsto x^2$ for positive $x$, to maximize the absolute value it suffits to maximize $f(x)^2$.

On the other hand, we intent to give another equivalent to the problem of maximizing absolute value. We observe that for any real number $k$, the aboslute value $|k|$ is the result of the following optimization problem:
$$
\begin{array}{cc}
    \underset{p\in\mathbb{R}}{\max} & pk  \\
     \text{ s.t. } & -1\leq p \leq 1
\end{array}
$$
Which is a linear programming problem over a convex set, the solution is taken on the extremal point of the convex set, $-1$ or $1$ in this case and we know that $|k|=\max\{-k,k\}$. As a result, the following problem :
\begin{equation}\label{eq:max pfx}
    \begin{array}{cc}
        \underset{p\in\mathbb{R},x}{\max} & pf(x)  \\
         \text{ s.t. } & \left\{\begin{array}{c}
              -1\leq p \leq 1  \\
              x\in X 
         \end{array}\right.
    \end{array}
\end{equation}
is equivalent to the problem of maximizing absolute value of $f$ over $X$. If $(p^*,x^*)$ is a global solution of (\ref{eq:max pfx}), we will prove two things:
\begin{itemize}
    \item the optimal value $p^*f(x^*)$ is equal to $|f(x^*)|$,
    \item $x^*$ is a global solution of (\ref{eq:max absf}).
\end{itemize}

Considering $p\in[-1,1]$, there is :
$$
pf(x^*)\leq p^*f(x^*).
$$
By taking the maximum value over $p$ of both side, we see that :
$$
|f(x^*)|\leq p^*f(x^*)
$$
On the other hand, 
$$
\left\{\begin{array}{c}
     -1\leq p^* \leq 1  \\
     -|f(x^*)|\leq f(x^*) \leq |f(x^*)| 
\end{array}\right.\implies p^*f(x^*)\leq |f(x^*)|.
$$
We conclude that $p^*f(x^*)=|f(x^*)|$. 

Now consider any feasible $(p,x)$, we have:
$$
pf(x)\leq p^*f(x^*)=|f(x^*)|.
$$
Again by taking max over $p$, we can see that :
$$
\forall x\in X, |f(x)|\leq |f(x^*)|
$$
So $x^*$ is a global solution of (\ref{eq:max absf}).

In the context of polynomial optimization, when $f$ is a polynomial, we can maximize its absolute value over $X$ at the cost of introducing one more variable $p$, rather than double the degree of the polynomial.
\subsection{fast computation of $f^TV$}
As shown in \cite{lofberg_coefficients_2004}, a finite real valued Fourier serie with $N$ harmonics can be equivalently represented by its sampled values at at least $2N+1$ equidistant points in $[-\pi,\pi]$. For instance, if $f(t)=\sum_{n=-N}^{N}c_n(f)e^{int}$ then:
\begin{equation}
    f(t)=\sum_{k=-N}^Nf(k\tau)K_{2N+1}(t-k\tau),
\end{equation}
where :
\begin{itemize}
    \item $\tau=\frac{2\pi}{2N+1}$, the sampled points
    \item $K_{m}$ is the $m$-th Dirichlet kernel and it satisfies:
    $$
    K_{m}(t)=\frac{1}{m}\frac{\sin\frac{mt}{2}}{\sin{\frac{t}{2}}}
    $$
    and $K_m(0)=1$ and $K_m(2k\pi/m)=0$ otherwise $k\neq 0$. Thus $K_{2N+1}(t-k\tau)$ is interpolating iff $t=k\tau$.
\end{itemize}
This gives an efficient way to compute $f(t)^TV(t)$, which is a real scale finite Fourier serie with $2N$ harmonics. We need to sample $f$ and $V$ at $\tau=\frac{2k\pi}{4N+1}$ for $k=-2N,\dots,2N$, which can be generated by Fast Fourier transform and we perform $4N+1$ inner products $f(k\tau)^TV(k\tau)$. In fact:
\begin{equation}
    \begin{bmatrix}
    f(-2N\tau)^T\\
    \vdots\\
    f(0)^T\\
    \vdots\\
    f(2N\tau)^T
    \end{bmatrix}= \begin{bmatrix}
        e^{-2N\tau\times (-N) i}&\dots&1&\dots&e^{-2N\tau\times N i}\\
        \vdots & ~ & \vdots & ~ & \vdots \\
        1 & \dots & 1 & \dots & 1 \\
        \vdots & ~ & \vdots & ~ & \vdots \\
        e^{2N\tau\times (-N) i}&\dots&1&\dots&e^{2N\tau\times N i}
    \end{bmatrix}\begin{bmatrix}
        c_{-N}(f)^T\\
        \vdots\\
        c_{0}(f)^T\\
        \vdots\\
        c_{N}(f)^T
    \end{bmatrix}
\end{equation}
Let's note $E\in\mathbb{C}^{(4N+1)\times(2N+1)}$ such that $E_{k,l}=e^{k\tau li}$

\subsection{Postivity certificate of trigonometric polynomial}
Let's consider the complex polynomial :
$$
f(z)=\sum_{k}c_k z^k
$$
defined for $z\in\mathbb{T}^n=\{z\in\mathbb{C}^n,\forall i,|z_i|=1\}$, the complex unit torus of dimension $n$ and for $k=(k_1,\dots,k_n)\in\mathbb{Z}^n$ multiindices which can take negative values. The monomial $z^k=z_1^{k_1}\dots z_n^{k_n}$ is defined in a similar way as in real polynomial. The sequence of coefficients $(c_k)_{k\in\mathbb{Z}^n}$ has only finitely many non zero terms thus the degree of the polynomial $f$ is maximum of $\{|k|, c_k\neq 0\}$.

We are particularly interested in complex polynomials that are real on $\mathbb{T}^n$ for which we want a postive certificate similar to Putinar's positivstellensatz. The coefficient sequence $\{c_k\}$ must satisfy the symmetry condition that is $$
\forall k\in\mathbb{Z}^n,c_{-k}=\overline{c_k}.
$$
In addition to symmetry condition, we restrict $z$ to a subset of $\mathbb{T}^n$ that corresponds to the common zeros of a given family of real trigonometric polynomial $\forall i\in\{1,\dots,m\},Q_i(z)=0$.

We cite the result proven in \cite{megretski_positivity_2003} that gives a positivity certificate for a real trigonometric polynomial on the set $Z_Q=\{z\in\mathbb{T}^n:\forall 1\leq i\leq m, Q_i(z)=0\}$ by using SOS polynomials.

\begin{theorem}[Positive certificate of trigonometric polynomial]\label{trm:positive}
Let $f(z)=\sum_{k}c_k z^k$ be a real trigonometric polynomial for all $z\in\mathbb{T}^n$. 

$f$ is strictly positive on $Z_Q$ iff there exisits $H_i,1\leq i\leq m$, real trigonometric polynomials and a SOS trigonometric polynomials $\sigma=\sum_{j=1}^r |V_j(z)|^2$ such that:
$$
\forall z\in\mathbb{T}^n,f(z)=\sum_{i}H_i(z)Q_i(z)+\sigma(z).
$$
\end{theorem}
In fact, this positive certificate is nothing new compared to Putinar's positivstellensatz. If we reparametrize $ z=(e^{j\omega_1},\dots,e^{j\omega_n})$, then $f$ becomes a real polynomial in terms of $\cos(k.\omega)$ and $\sin(k.\omega)$. Moreover, by introducing $x_i=\cos(\omega_i)$ and $y_i=\sin(\omega_i)$ and by trigonometric identities, $f$ becomes a polynomial of $(x,y)\in\mathbb{R}^n\times\mathbb{R}^n$. This argument also holds for $Q_i$, thus we would look for a positive certificate $f(x,y)>0$ for all $(x,y)$ in the following basic semi-algebraic set:
$$
\mathcal{K}=\{(x,y)\in\mathbb{R}^{2n}:x_1^2+y_1^2=1,\dots,x_n^2+y_n^2=1,Q_1(x,y)=0,\dots,Q_m(x,y)=0\}.
$$
$\mathcal{K}$ being compact, Putinar's positivstellensatz holds. However, the theorem (\ref{trm:positive}) preserves the complex structure and could be more efficient as we would like to give the Lasserre's hierachy to minimize $f$ on $\mathbb{T}^n$ with constraint $z\in Z_Q$.

\subsection{Lasserre's hierarchy for minimization of trigonometric polynomial}
We want to solve the problem:
\begin{equation}
    f^*=\begin{array}{cc}
       \underset{z\in\mathbb{T}^n}{\min}  &  f(z)\\
        \text{ s.t. } & Q_i(z)=0,\forall i=1,\dots,m 
    \end{array},
\end{equation}
which is equivalent to find the maximum of lower bound of $f$:
\begin{equation}
    \lambda^*=\begin{array}{cc}
       \underset{\lambda}{\sup}  &  \lambda\\
        \text{ s.t. } & f(z)-\lambda > 0,\forall z\in Z_Q
    \end{array},
\end{equation}
Using the (\ref{trm:positive}) we can build the following hierarchy of SDP relaxation for all $k$ such that $2k\geq \deg(f)$:
\begin{equation}\label{eqn:Torus hierarchy}
    \lambda_k^*=\begin{array}{cc}
       \underset{\lambda}{\sup}  &  \lambda\\
        \text{ s.t. } & \begin{array}{c}
             f(z)-\lambda = \sum_i H^k_i(z) Q_i(z)+\sigma^k(z),  \\
             \max\{\deg(H_i^kQ_i),\deg(\sigma^k)\}\leq 2k 
        \end{array} 
    \end{array}
\end{equation}
Using the theorem (\ref{trm:positive}), we might prove that $\lambda_k^*$ converge increasingly to $\lambda^*$ thus to $f^*$. \textcolor{red}{Shen : We postpone the proof.}

Now we apply this result, combined with the discussion from the subsection (\ref{sec:max abs}) to the problem of finding the peak steady-state power. We call that we want to solve:
\begin{equation}
    \underset{t\in[0,2\pi]}{\max} |f(t)^TV(t)|,
\end{equation}
where $f(t)=\sum_n c_n(f)e^{int}$ and $V(t)=\sum_n c_n(V)e^{int}$. By the discussion in subsection (\ref{sec:max abs}), it is equivalent to :
\begin{equation}
    \begin{array}{cc}
         \underset{t\in[0,2\pi],p}{\max}& pf(t)^TV(t) \\
         \text{ s.t. } & -1\leq p\leq 1 
    \end{array}.
\end{equation}
Now we reparametrize the optimization variables:
$$
\forall z=(z_1,z_2)\in\mathbb{T}^2,\left\{\begin{array}{cc}
     e^{it}=&z_1  \\
     p=&\frac{z_2+z_2^{-1}}{2} 
\end{array}\right.
$$
which gives:
\begin{equation}\label{eq:max power torus}
         \underset{z}{\min}  -\frac{z_2+z_2^{-1}}{2}\sum_{n,m}c_n(f)^T c_m(V)z_1^{n+m} 
\end{equation}
Which is solvable by the hierarchy (\ref{eqn:Torus hierarchy}). Moreover, the variable $p$ can be even tightened such that $p\in\{-1,1\}$ as we have seen in the proof in the subsection (\ref{sec:max abs}) that $p$ is only expected to reach extremal value. To deal with this constraint, we add $Q(z)=(\frac{z_2+z_2^{-1}}{2})^2-1$ in the minimization (\ref{eq:max power torus}):
\begin{equation}
    \begin{array}{cc}
        \underset{z}{\min}  &-\frac{z_2+z_2^{-1}}{2}\sum_{n,m}c_n(f)^T c_m(V)z_1^{n+m}\\
        \text{ s.t. } &Q(z)=0
    \end{array} 
\end{equation}
\subsection{Duality point of view of the original problem}
Minimizing the peak power can be seen as the minimization of the $L_\infty$ norm if we consider the problem in a suitable functional analysis context. More precisely, the solution $v$ of the equation:
$$
M(a)v''+K(a)v=f'
$$
makes sense if we place $f$ in a good functional space. In the case of finite number of harmonics, $f$ is $C^\infty$ so this problem can be postponed. And the original problem of minimizing peak power is infact:
\begin{equation}\label{eqn:l infty norm minimization}
    \begin{array}{cc}
        \underset{a,v}{\min} & ||f^Tv||_{\infty}\\
         \text{ s.t. }& a\geq 0, a^Tq,M(a)\succeq 0, K(a)\succeq0\\
         &M(a)v''+K(a)v=f' 
    \end{array} 
\end{equation}
This is very hard because of the $||.||_\infty$ norm, so here we try to introduce a dual point of view of the problem (\ref{eqn:l infty norm minimization}) and we will try to see if something can be simplified. In order to do so, we define the Legendre-Fenchel transform (or the conjugate function) of a function, even though, this is more usual for a convex function but here nothing is told about $a\mapsto C(a)=||f^Tv||_\infty$.
\begin{definition}[Legrendre-Fenchel Transform]
For a function $f$, for whenever it exists finite value, we define its Legrendre-Fenchel transform or conjugate function $f^*$ as:
$$
f^*(x)=\underset{y}{\max}~x^Ty-f(y)
$$
\end{definition}
In general, when we minimize a function $f$ with constraint $Ax\leq b$, its dual involves the conjugate of $f$ naturally. Indeed, by the Lagrangian :$$
L(x,\mu)=f(x)+\mu^T(Ax-b)\implies g(\mu)=\underset{x}{\min}\{f(x)+(A^T\mu)^Tx\}-b^T\mu
$$
While we note that:$$
\underset{x}{\min}\{f(x)+(A^T\mu)^Tx\}=-\underset{x}{\max}\{(-A^T\mu)^Tx-f(x)\}=-f^*(-A^T\mu)
$$
So the dual problem is :$$
    \begin{array}{cc}
        \underset{\mu}{\max} & g(\mu)=-f^*(-A^T\mu)-b^T\mu\\
         \text{ s.t. }& \left\{ \begin{array}{cc}
              \mu \geq 0  \\
              -A^T\mu \in X^* 
         \end{array}\right.
    \end{array} 
$$
$X^*$ is the set for which the conjugate $f^*(x)$ is finite.

Now let's try if we can find the dual of (\ref{eqn:l infty norm minimization}), for this, we need to find the conjugate $C^*$ and see whenever it's finite. The intuition behind is that each norm, seen as function, becomes its conjugate norm after taking Legendre transform, more precisely, $||.||_\infty$ becomes $||.||_1$ and $X^*$ are usually described by a bound on the $L^1$ norm which is much simplier and can be relaxed by a bound on $L^2$ norm. This relaxation is possible since $f^Tv$ is supported in $[0,2\pi]$ and since $L^1(\Omega)\subset L^2(\Omega)\subset L^{\infty}(\Omega)$ if $\Omega$ is bounded. And a $L^2$ norm bound can be (hopely) described by Fourier coefficients. This approach might help in the future if we want to consider infinite number of harmonics or something ...

We should first transform $C$ into a suitable form:
\begin{align}
        C(a)=&
             \underset{t\in[-\pi,\pi]}{\max} ~ |f(t)^Tv(t)| ~
             \text{ s.t. }M(a)v''(t)+K(a)v(t)=f'(t), \forall t
        \\
        =&\underset{\lambda}{\min} ~  \lambda ~
             \text{ s.t. } \left\{\begin{array}{c}
                M(a)v''(t)+K(a)v(t)=f'(t), \forall t  \\
                \lambda \geq |f(t)^Tv(t)|, \forall t
             \end{array}\right. 
\end{align}
Then:\begin{align}
        C^*(a)=&\underset{b}{\max}~a^Tb-\underset{\lambda}{\min}\{\lambda,\dots\}\\
        &=\underset{b,\lambda}{\max}~a^Tb-\lambda ~ \text{ s.t. } \left\{\begin{array}{c}
                M(b)v''(t)+K(b)v(t)=f'(t), \forall t  \\
                \lambda \geq |f(t)^Tv(t)|, \forall t
             \end{array}\right. 
\end{align}
\newpage

\printbibliography

\end{document}


