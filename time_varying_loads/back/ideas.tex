\section{Ideas}
\subsection{Remove matrix inverse}\label{sc:inverse removal}
In our discussion, so far, we rely on the inverse of the matrices $-k^2M(a)+K(a)$ to express the objective function. This may have those problems :
\begin{itemize}
    \item they introduce large degree polynomials. Typically, if $f$ is $d$ dimensional, the inverse of the matrices $-k^2M(a)+K(a)$ introduces polynomial of degree $d$. 
    \item It's not garanteed to have those inverses for larger scale problem, even if it's possible, it would be necessary to exploit sparsity.
    \item $-k^2M(a)+K(a)$ could be singular for more general case, as discussed in \cite{tyburec_global_2021}. Again, calculating explicitly the pseudo inverse could be impossible due to the scale of the problem.
\end{itemize}

In \cite{tyburec_global_2021}, the following trick by Schur complement is introduced to remove the inverse of a matrix in a minimization problem. Let's consider the following optimization problem, where $f(x)$ and $M(x)$ are vector and matrix functions depending polynomially on $x$ :
\begin{equation}
	\begin{array}{cc}
		\underset{x\in\mathbb{R}}{\min} & ~ f(x)M(x)^{\dagger} f(x)\\
		\text{ s.t. } & Ax\leq b, M(x)\succeq 0, f(x)\in\range(M(x))
	\end{array}
\end{equation}
By introducing a slack varible $c$, we get :
\begin{equation}
	\begin{array}{cc}
		\underset{x\in\mathbb{R}^n,c}{\min} & ~ c\\
		\text{ s.t. } & \left\{\begin{array}{c}
			Ax \leq b\\
			M(x) \succeq 0, c\geq f(x)M(x)^{\dagger} f(x) \\
			f(x) \in\range(M(x))\\
		\end{array}\right.
	\end{array}
\end{equation}
Notice that the constraints $M(x)\succeq 0, f(x)\in\range(M(x)),c\geq f(x)M(x)^{\dagger} f(x)$ hold \textit{iff} we have the following PMI:
\begin{equation}
	\begin{pmatrix}
		c & f(x)^T \\
		f(x) & M(x)
	\end{pmatrix}\succeq 0
\end{equation}
which is a classic result by Schur complement. This trick allowed to replace the pseudo inverse of $M(x)^\dagger$ by an equivalent formulation with a PMI.

In our current minimization problem, we are in a more general case where there are $f(x)$ and $g(x)$ two different polynomial vectors of $x$:
\begin{equation}
	\begin{array}{cc}
		\underset{x\in\mathbb{R}}{\min} & ~ f(x)M(x)^{\dagger} g(x)\\
		\text{ s.t. } & Ax\leq b, M(x)\succeq 0, f(x),g(x)\in\range(M(x))
	\end{array}
\end{equation}
The idea of introducing slack variable $c$ doesnot seem to hold anymore. In fact, the objective function $f(x)M(x)^{\dagger} g(x)$ can be equivalently rewritten as :
\begin{equation}
	f(x)M(x)^{\dagger} g(x)=\frac{1}{2}\Big((f(x)+g(x))^TM(x)^\dagger(f(x)+g(x))-f(x)^TM(x)^\dagger f(x)-g(x)^TM(x)^\dagger g(x)\Big)
\end{equation}
If we introduce $c_1,c_2,c_3$ for each of the terms $(f(x)+g(x))^TM(x)^\dagger(f(x)+g(x))$, $f(x)^TM(x)^\dagger f(x)$ and $g(x)^TM(x)^\dagger g(x)$, the new minimization problem is not equivalent to the original problem:
\begin{equation}
	\begin{array}{cc}
		\underset{x\in\mathbb{R}^n,c_1,c_2,c_3}{\min} & ~ \frac{1}{2}(c_1-c_2-c_3)\\
		\text{ s.t. } & \left\{\begin{array}{c}
			Ax \leq b\\
			\begin{pmatrix}
				c_1 & f(x)^T+g(x)^T \\
				f(x)+g(x) & M(x)
			\end{pmatrix}\succeq 0 \\
			\begin{pmatrix}
				c_2 & f(x)^T \\
				f(x) & M(x)
			\end{pmatrix}\succeq 0 \\
			\begin{pmatrix}
				c_3 & g(x)^T \\
				g(x) & M(x)
			\end{pmatrix}\succeq 0 
		\end{array}\right.
	\end{array}
\end{equation}
The new optimizaiton problem is equivalent to the original problem \textit{iff} at one feasible solution $x^*,c_1^*,c_2^*,c_3^*$ there is :
\begin{equation}
	\left\{\begin{array}{cc}
		c_1^*=&(f(x^*)+g(x^*))^TM(x^*)^\dagger(f(x^*)+g(x^*))\\
		c_2^*=&f(x^*)^TM(x^*)^\dagger f(x^*)\\
		c_3^*=&g(x^*)^TM(x^*)^\dagger g(x^*)
	\end{array}\right.
\end{equation}
Otherwise the value of the new objective function is not in the range of the original objective funtion. However, for the PMI reformulated problem, $c_2$ and $c_3$ are with negative sign in the objective function, if $c_i$ are unbounded above, the minimum value is infinite and if we give artificial upper bounds to $c_i$, $c_2^*$ and $c_3^*$ will reach the artificial upper bounds and their value will not match with $f(x^*)^TM(x^*)^\dagger f(x^*)$ and $g(x^*)^TM(x^*)^\dagger g(x^*)$.

However, it is possible to regard the reformulated problem as Bilevel optimization problem:
\begin{equation}
	\begin{array}{cc}
		\underset{x\in\mathbb{R}^n,c_1,c_2,c_3}{\min} & ~ \frac{1}{2}(c_1-c_2-c_3)\\
		\text{ s.t. } & \left\{\begin{array}{c}
			Ax \leq b,\begin{pmatrix}
				c_1 & f(x)^T+g(x)^T \\
				f(x)+g(x) & M(x)
			\end{pmatrix}\succeq 0 \\
			c_2\in\mathcal{C}_2(x)=\argmin\{\gamma_2|\begin{pmatrix}\gamma_2 & f(x)^T \\ f(x) & M(x)\end{pmatrix}\succeq 0\} \\
			c_3\in\mathcal{C}_3(x)=\argmin\{\gamma_3|\begin{pmatrix}\gamma_3 & g(x)^T \\ g(x) & M(x)\end{pmatrix}\succeq 0\}
		\end{array}\right.
	\end{array}
\end{equation}
This problem can be solve by the Bilevel optimization method proposed by \cite{jeyakumar_convergent_2016}. The constraints of type $\begin{pmatrix}\gamma_2 & f(x)^T \\ f(x) & M(x)\end{pmatrix}\succeq 0$ are not necessarily convex, we should refer to the solution described in section 4 of \cite{jeyakumar_convergent_2016}. To do so, We introduce the parametric optimizaiton problems:
$$
x\mapsto\mathcal{I}(x)=\min\{\gamma|\begin{pmatrix}\gamma & f(x)^T \\ f(x) & M(x)\end{pmatrix}\succeq 0\}
$$
and
$$
x\mapsto\mathcal{J}(x)=\min\{\gamma|\begin{pmatrix}\gamma & g(x)^T \\ g(x) & M(x)\end{pmatrix}\succeq 0\}
$$
and we relax the Bilevel optimization problem for a given $\epsilon$:
\begin{equation}
	\begin{array}{cc}
		\underset{x\in\mathbb{R}^n,c_1,c_2,c_3}{\min} & ~ \frac{1}{2}(c_1-c_2-c_3)\\
		\text{ s.t. } & \left\{\begin{array}{c}
			Ax \leq b,\begin{pmatrix}
				c_1 & f(x)^T+g(x)^T \\
				f(x)+g(x) & M(x)
			\end{pmatrix}\succeq 0 \\
			c_2-\mathcal{I}(x)\leq \epsilon \\
			c_3-\mathcal{J}(x)\leq \epsilon
		\end{array}\right.
	\end{array}
\end{equation}
$\mathcal{I}$ and $\mathcal{J}$ are not polynomial in general, we should look for their $k$-th polynomial relaxations $\mathcal{I}_k$ and $\mathcal{J}_k$ by the methodes of \cite{lasserre_jointmarginal_2009}. Under compactness assumptions, we can used the Theorem 4.7 of \cite{jeyakumar_convergent_2016} to prove the convergence when $k\rightarrow\infty,\epsilon\rightarrow 0$ of the following problem:
\begin{equation}\label{eqn:bilinear bilevel}
	\begin{array}{cc}
		\underset{x\in\mathbb{R}^n,c_1,c_2,c_3}{\min} & ~ \frac{1}{2}(c_1-c_2-c_3)\\
		\text{ s.t. } & \left\{\begin{array}{c}
			Ax \leq b\\
			\begin{pmatrix}
				c_1 & f(x)^T+g(x)^T \\
				f(x)+g(x) & M(x)
			\end{pmatrix}\succeq 0, \begin{pmatrix}
				c_2 & f(x)^T \\
				f(x) & M(x)
			\end{pmatrix}\succeq 0, \begin{pmatrix}
				c_3 & g(x)^T \\
				g(x) & M(x)
			\end{pmatrix}\succeq 0,\\
			c_2-\mathcal{I}_k(x)\leq \epsilon \\
			c_3-\mathcal{J}_k(x)\leq \epsilon
		\end{array}\right.
	\end{array}
\end{equation}
Note that the constraints such as $\begin{pmatrix}
				c_1 & f(x)^T+g(x)^T \\
				f(x)+g(x) & M(x)
			\end{pmatrix}\succeq 0$ lack of compactness. $c_1$ can be arbitarily large while insuring the positiveness of the PMI. We can introduce artificial upper bounds if we compute the upper bounds of $(f(x)^T+g(x)^T)M(x)^\dagger(f(x)+g(x))$ for all feasible $x$.

Moreover, the two constraints 
\begin{align}
	c_2\in\mathcal{C}_2(x)&=\argmin\{\gamma_2|\begin{pmatrix}\gamma_2 & f(x)^T \\ f(x) & M(x)\end{pmatrix}\succeq 0\} \\
	c_3\in\mathcal{C}_3(x)&=\argmin\{\gamma_3|\begin{pmatrix}\gamma_3 & g(x)^T \\ g(x) & M(x)\end{pmatrix}\succeq 0\}
\end{align}
can be in fact contracted into on single constraints:
\begin{equation}
	(c_2,c_3)\in\mathcal{C}(x)=\argmin\{\gamma_2+\gamma_3|\begin{pmatrix}\gamma_2 & f(x)^T \\ f(x) & M(x)\end{pmatrix}\succeq 0, \begin{pmatrix}\gamma_3 & g(x)^T \\ g(x) & M(x)\end{pmatrix}\succeq 0\}
\end{equation}
In general, when we want to minimize the sum of functions with respect to "independant" variables, each of the function is minimized with respect to its respective variables. More recisely, a solution of the problem:
\begin{equation}
	\begin{array}{cc}
		\underset{x_1,\dots,x_n}{\min} & ~ \sum_{i}^n f_i(x_i) \\
		\text{ s.t. } & \forall i, x_i\in X_i
	\end{array}
\end{equation}
is given by $x*$ such that each of the $x_i^*$ is the solution of 
\begin{equation}
	\begin{array}{cc}
		\underset{x_i}{\min} & ~ f_i(x_i) \\
		\text{ s.t. } & x_i\in X_i
	\end{array}
\end{equation}
As consequence,
\begin{align}
	\begin{split}
	& c_2\in\mathcal{C}_2(x)=\argmin\{\gamma_2|\begin{pmatrix}\gamma_2 & f(x)^T \\ f(x) & M(x)\end{pmatrix}\succeq 0\},c_3\in\mathcal{C}_3(x)=\argmin\{\gamma_3|\begin{pmatrix}\gamma_3 & g(x)^T \\ g(x) & M(x)\end{pmatrix}\succeq 0\}\\
	\iff & 	(c_2,c_3)\in\mathcal{C}(x)=\argmin\{\gamma_2+\gamma_3|\begin{pmatrix}\gamma_2 & f(x)^T \\ f(x) & M(x)\end{pmatrix}\succeq 0, \begin{pmatrix}\gamma_3 & g(x)^T \\ g(x) & M(x)\end{pmatrix}\succeq 0\}
	\end{split}
\end{align} 

The problem (\ref{eqn:bilinear bilevel}) can be thus equivalently expressed by:
\begin{equation}
	\begin{array}{cc}
		\underset{x\in\mathbb{R}^n,c_1,c_2,c_3}{\min} & ~ \frac{1}{2}(c_1-c_2-c_3)\\
		\text{ s.t. } & \left\{\begin{array}{c}
			Ax \leq b\\
			\begin{pmatrix}
				c_1 & f(x)^T+g(x)^T \\
				f(x)+g(x) & M(x)
			\end{pmatrix}\succeq 0, \begin{pmatrix}
				c_2 & f(x)^T \\
				f(x) & M(x)
			\end{pmatrix}\succeq 0, \begin{pmatrix}
				c_3 & g(x)^T \\
				g(x) & M(x)
			\end{pmatrix}\succeq 0,\\
			c_2+c_3-\mathcal{H}_k(x)\leq \epsilon
		\end{array}\right.
	\end{array}
\end{equation}
where $\mathcal{H}_k$ is the $k$-th polynomial relaxation of the parametric optimization problem:
\begin{equation}
	\mathcal{H} ~:~ x\mapsto \underset{\gamma_2,\gamma_3}{\min}\{\gamma_2+\gamma_3|\begin{pmatrix}
				\gamma_2 & f(x)^T \\
				f(x) & M(x)
			\end{pmatrix}\succeq 0,\begin{pmatrix}
				\gamma_3 & g(x)^T \\
				g(x) & M(x)
			\end{pmatrix}\succeq 0\}
\end{equation}
\subsection{Dealing with higher frequency load}
The discussion of previous subsub section is only valide for sdp matrices. If we add the constraints $K(a)-k^2M(a)\succeq 0$ for all $k$, we expect that highest frequency of the load is inferior to the smallest natural frequency of the structure. Indeed, only the constraint $K(a)-N^2M(a)$ is required to ensure sdp-ness of every matrix, if $N$ is the highest frequency number. To see this, we note that :
$$
K-k^2M=K-(k+1)^2M+\underbrace{(2k+1)M}_{\text{is sdp matrix}},
$$
then $K(a)-k^2M(a)\succeq K(a)-(k+1)^2M(a)$, by induction, $K(a)-k^2M(a)\succeq K(a)-N^2M(a)$

To bypass this limitation, we might propose to find an exact formula or an approximation of the pseudo inverse of $K(a)-k^2M(a)$ using pseudo-inverse of $K$ and $M$, which are sdp matrices. Next step, we apply the idea previously discussed. 

\MT{The case when the resonance frequency is higher than the frequencies of the loads obtained by Fourier expansion is not entirely general but in several applications such cases occur. Thus, while $K-k^2M\succeq0$ is limiting, it still makes sense to discuss/try computationally.}

\subsection{LP expression of the lower problem}
The lower problem can be reduced to optimization of a linear function over a non linear feasible set. Let $(a_i)\in\mathbb{R}^n$ be the vector of optimization variables, if $f$ is composed by $N$ harmonics, then there are sequences of functions $\alpha_k$ and $\beta_k$ for $k=1,...,2N$ such that:
$$
f(t)^Tv(t)=\sum_{k=1}^{2N} \alpha_k(a)\cos(kt)+\beta_k(a)\sin(kt)
$$

The lower level problem is to solve for each feasible $a$
$$
C(a)=\underset{t}{\max} ~ f(t)^Tv(t),
$$
the first idea was to replace $\cos(kt)$ and $\sin(kt)$ by polynomials in terms of $\cos(t)$ and $\sin(t)$. Now we propose to replace each of the $\cos(kt)$ and $\sin(kt)$ by a pair of variables $x_k$ and $y_k$ and add equality constraints to copy exactly the behaviour of trigonometric functions. Considering $x,y\in\mathbb{R}^{2N}$, we propose the following problem :
\begin{align}
    \begin{split}
        \underset{x,y}{\max} & ~\alpha(a)^Tx+\beta(a)^Ty \\
        \text{ s.t. } & \left\{\begin{array}{c}
             x_1^2+y_1^2=1\\
             x_{i+1} = x_{i}x_1-y_{i}y_{1}, \forall i=1,\dots,2N-1\\
             y_{i+1} = x_{i}y_1+y_{i}x_1,\forall i=1,\dots,2N-1
        \end{array}\right.
    \end{split}
\end{align}
which consists of maximizaing a linear function over a non linear feasible set. If there is indeed a solution, we can find $t\in[0,2\pi]$ such that $\cos(jt)=x_j,\sin(jt)=y_j$ for every $j$.

It seems that this new formulation can be solved more efficiently, even if the number of variables and of equality constraints increased. In exchange, the objective function is linear in $x,y$.

\subsection{Maximizing absolute value}\label{sec:max abs}
In fact, in the litterature, the peak power is defined as the maximal value of the absolute value of $f(t)^TV(t)$, this is ignored in the current draft but it's possible to fix it.

Let's define an abstract constrainted optimization problem :
\begin{equation}\label{eq:max absf}
    \begin{array}{cc}
        \underset{x}{\max} & |f(x)|  \\
         \text{ s.t. } & x\in X
    \end{array}
\end{equation}

The first approach is due to the monotony of $x\mapsto x^2$ for positive $x$, to maximize the absolute value it suffits to maximize $f(x)^2$.

On the other hand, we intent to give another equivalent to the problem of maximizing absolute value. We observe that for any real number $k$, the aboslute value $|k|$ is the result of the following optimization problem:
$$
\begin{array}{cc}
    \underset{p\in\mathbb{R}}{\max} & pk  \\
     \text{ s.t. } & -1\leq p \leq 1
\end{array}
$$
Which is a linear programming problem over a convex set, the solution is taken on the extremal point of the convex set, $-1$ or $1$ in this case and we know that $|k|=\max\{-k,k\}$. As a result, the following problem :
\begin{equation}\label{eq:max pfx}
    \begin{array}{cc}
        \underset{p\in\mathbb{R},x}{\max} & pf(x)  \\
         \text{ s.t. } & \left\{\begin{array}{c}
              -1\leq p \leq 1  \\
              x\in X 
         \end{array}\right.
    \end{array}
\end{equation}
is equivalent to the problem of maximizing absolute value of $f$ over $X$. If $(p^*,x^*)$ is a global solution of (\ref{eq:max pfx}), we will prove two things:
\begin{itemize}
    \item the optimal value $p^*f(x^*)$ is equal to $|f(x^*)|$,
    \item $x^*$ is a global solution of (\ref{eq:max absf}).
\end{itemize}

Considering $p\in[-1,1]$, there is :
$$
pf(x^*)\leq p^*f(x^*).
$$
By taking the maximum value over $p$ of both side, we see that :
$$
|f(x^*)|\leq p^*f(x^*)
$$
On the other hand, 
$$
\left\{\begin{array}{c}
     -1\leq p^* \leq 1  \\
     -|f(x^*)|\leq f(x^*) \leq |f(x^*)| 
\end{array}\right.\implies p^*f(x^*)\leq |f(x^*)|.
$$
We conclude that $p^*f(x^*)=|f(x^*)|$. 

Now consider any feasible $(p,x)$, we have:
$$
pf(x)\leq p^*f(x^*)=|f(x^*)|.
$$
Again by taking max over $p$, we can see that :
$$
\forall x\in X, |f(x)|\leq |f(x^*)|
$$
So $x^*$ is a global solution of (\ref{eq:max absf}).

In the context of polynomial optimization, when $f$ is a polynomial, we can maximize its absolute value over $X$ at the cost of introducing one more variable $p$, rather than double the degree of the polynomial.
\subsection{fast computation of $f^TV$}
As shown in \cite{lofberg_coefficients_2004}, a finite real valued Fourier serie with $N$ harmonics can be equivalently represented by its sampled values at at least $2N+1$ equidistant points in $[-\pi,\pi]$. For instance, if $f(t)=\sum_{n=-N}^{N}c_n(f)e^{int}$ then:
\begin{equation}
    f(t)=\sum_{k=-N}^Nf(k\tau)K_{2N+1}(t-k\tau),
\end{equation}
where :
\begin{itemize}
    \item $\tau=\frac{2\pi}{2N+1}$, the sampled points
    \item $K_{m}$ is the $m$-th Dirichlet kernel and it satisfies:
    $$
    K_{m}(t)=\frac{1}{m}\frac{\sin\frac{mt}{2}}{\sin{\frac{t}{2}}}
    $$
    and $K_m(0)=1$ and $K_m(2k\pi/m)=0$ otherwise $k\neq 0$. Thus $K_{2N+1}(t-k\tau)$ is interpolating iff $t=k\tau$.
\end{itemize}
This gives an efficient way to compute $f(t)^TV(t)$, which is a real scale finite Fourier serie with $2N$ harmonics. We need to sample $f$ and $V$ at $\tau=\frac{2k\pi}{4N+1}$ for $k=-2N,\dots,2N$, which can be generated by Fast Fourier transform and we perform $4N+1$ inner products $f(k\tau)^TV(k\tau)$. In fact:
\begin{equation}
    \begin{bmatrix}
    f(-2N\tau)^T\\
    \vdots\\
    f(0)^T\\
    \vdots\\
    f(2N\tau)^T
    \end{bmatrix}= \begin{bmatrix}
        e^{-2N\tau\times (-N) i}&\dots&1&\dots&e^{-2N\tau\times N i}\\
        \vdots & ~ & \vdots & ~ & \vdots \\
        1 & \dots & 1 & \dots & 1 \\
        \vdots & ~ & \vdots & ~ & \vdots \\
        e^{2N\tau\times (-N) i}&\dots&1&\dots&e^{2N\tau\times N i}
    \end{bmatrix}\begin{bmatrix}
        c_{-N}(f)^T\\
        \vdots\\
        c_{0}(f)^T\\
        \vdots\\
        c_{N}(f)^T
    \end{bmatrix}
\end{equation}
Let's note $E\in\mathbb{C}^{(4N+1)\times(2N+1)}$ such that $E_{k,l}=e^{k\tau li}$

\subsection{Postivity certificate of trigonometric polynomial}\label{sc:pos trigo}
Let's consider the complex polynomial :
$$
f(z)=\sum_{k}c_k z^k
$$
defined for $z\in\mathbb{T}^n=\{z\in\mathbb{C}^n,\forall i,|z_i|=1\}$, the complex unit torus of dimension $n$ and for $k=(k_1,\dots,k_n)\in\mathbb{Z}^n$ multiindices which can take negative values. The monomial $z^k=z_1^{k_1}\dots z_n^{k_n}$ is defined in a similar way as in real polynomial. The sequence of coefficients $(c_k)_{k\in\mathbb{Z}^n}$ has only finitely many non zero terms thus the degree of the polynomial $f$ is maximum of $\{|k|, c_k\neq 0\}$.

We are particularly interested in complex polynomials that are real on $\mathbb{T}^n$ for which we want a postive certificate similar to Putinar's positivstellensatz. The coefficient sequence $\{c_k\}$ must satisfy the symmetry condition that is $$
\forall k\in\mathbb{Z}^n,c_{-k}=\overline{c_k}.
$$
In addition to symmetry condition, we restrict $z$ to a subset of $\mathbb{T}^n$ that corresponds to the common zeros of a given family of real trigonometric polynomial $\forall i\in\{1,\dots,m\},Q_i(z)=0$.

We cite the result proven in \cite{megretski_positivity_2003} that gives a positivity certificate for a real trigonometric polynomial on the set $Z_Q=\{z\in\mathbb{T}^n:\forall 1\leq i\leq m, Q_i(z)=0\}$ by using SOS polynomials.

\begin{theorem}[Positive certificate of trigonometric polynomial]\label{trm:positive}
Let $f(z)=\sum_{k}c_k z^k$ be a real trigonometric polynomial for all $z\in\mathbb{T}^n$. 

$f$ is strictly positive on $Z_Q$ iff there exisits $H_i,1\leq i\leq m$, real trigonometric polynomials and a SOS trigonometric polynomials $\sigma=\sum_{j=1}^r |V_j(z)|^2$ such that:
$$
\forall z\in\mathbb{T}^n,f(z)=\sum_{i}H_i(z)Q_i(z)+\sigma(z).
$$
\end{theorem}
In fact, this positive certificate is nothing new compared to Putinar's positivstellensatz. If we reparametrize $ z=(e^{j\omega_1},\dots,e^{j\omega_n})$, then $f$ becomes a real polynomial in terms of $\cos(k.\omega)$ and $\sin(k.\omega)$. Moreover, by introducing $x_i=\cos(\omega_i)$ and $y_i=\sin(\omega_i)$ and by trigonometric identities, $f$ becomes a polynomial of $(x,y)\in\mathbb{R}^n\times\mathbb{R}^n$. This argument also holds for $Q_i$, thus we would look for a positive certificate $f(x,y)>0$ for all $(x,y)$ in the following basic semi-algebraic set:
$$
\mathcal{K}=\{(x,y)\in\mathbb{R}^{2n}:x_1^2+y_1^2=1,\dots,x_n^2+y_n^2=1,Q_1(x,y)=0,\dots,Q_m(x,y)=0\}.
$$
$\mathcal{K}$ being compact, Putinar's positivstellensatz holds. However, the theorem (\ref{trm:positive}) preserves the complex structure and could be more efficient as we would like to give the Lasserre's hierachy to minimize $f$ on $\mathbb{T}^n$ with constraint $z\in Z_Q$.

In particular, if we consider real valued trigonometric polynomials on $\mathbb{T}^n$ with $n\in\{1,2\}$, they are \textbf{nonnegative} \textit{iff} they are SOS. In contrary to real polynomials, the equivalence between SOS-ness and nonnegativeness holds only for univariate real polynomial. The proof when $n=1$ is relatively simple (see Theorem 1.1 of \cite{dumitrescu_positive_2017}) while the proof when $n=2$ is more difficult but in \cite{schmudgen_moment_2017}, we mentionned briefly the proof in the section 15.2.

\subsection{Lasserre's hierarchy for minimization of trigonometric polynomial}\label{sc:lassere trigo}
We want to solve the problem:
\begin{equation}
    f^*=\begin{array}{cc}
       \underset{z\in\mathbb{T}^n}{\min}  &  f(z)\\
        \text{ s.t. } & Q_i(z)=0,\forall i=1,\dots,m 
    \end{array},
\end{equation}
which is equivalent to find the maximum of lower bound of $f$:
\begin{equation}
    \lambda^*=\begin{array}{cc}
       \underset{\lambda}{\sup}  &  \lambda\\
        \text{ s.t. } & f(z)-\lambda > 0,\forall z\in Z_Q
    \end{array},
\end{equation}
Using the (\ref{trm:positive}) we can build the following hierarchy of SDP relaxation for all $k$ such that $2k\geq \deg(f)$:
\begin{equation}\label{eqn:Torus hierarchy}
    \lambda_k^*=\begin{array}{cc}
       \underset{\lambda}{\sup}  &  \lambda\\
        \text{ s.t. } & \begin{array}{c}
             f(z)-\lambda = \sum_i H^k_i(z) Q_i(z)+\sigma^k(z),  \\
             \max\{\deg(H_i^kQ_i),\deg(\sigma^k)\}\leq 2k 
        \end{array} 
    \end{array}
\end{equation}
Using the theorem (\ref{trm:positive}), we might prove that $\lambda_k^*$ converge increasingly to $\lambda^*$ thus to $f^*$. \textcolor{red}{Shen : We postpone the proof.}

Now we apply this result, combined with the discussion from the subsection (\ref{sec:max abs}) to the problem of finding the peak steady-state power. We call that we want to solve:
\begin{equation}
    \underset{t\in[0,2\pi]}{\max} |f(t)^TV(t)|,
\end{equation}
where $f(t)=\sum_n c_n(f)e^{int}$ and $V(t)=\sum_n c_n(V)e^{int}$. By the discussion in subsection (\ref{sec:max abs}), it is equivalent to :
\begin{equation}
    \begin{array}{cc}
         \underset{t\in[0,2\pi],p}{\max}& pf(t)^TV(t) \\
         \text{ s.t. } & -1\leq p\leq 1 
    \end{array}.
\end{equation}
Now we reparametrize the optimization variables:
$$
\forall z=(z_1,z_2)\in\mathbb{T}^2,\left\{\begin{array}{cc}
     e^{it}=&z_1  \\
     p=&\frac{z_2+z_2^{-1}}{2} 
\end{array}\right.
$$
which gives:
\begin{equation}\label{eq:max power torus}
         \underset{z}{\max}  \frac{z_2+z_2^{-1}}{2}\sum_{n,m}c_n(f)^T c_m(V)z_1^{n+m} 
\end{equation}
This is equivalent of finding the minimal upper bounds:
\begin{align}
	\begin{split}
        \underset{\theta}{\min}  ~&~ \theta \\
        \text{ s.t. } & \theta-\frac{z_2+z_2^{-1}}{2}\sum_{n,m}c_n(f)^T c_m(V)z_1^{n+m}\geq 0,\forall(z_1,z_2)\in\mathbb{T}^2
	\end{split}
\end{align}
As we have seen in the previous subsection, for $n$=2, a real trigonometric polynomial is nonnegative \textit{iff} it's SOS. Thus this problem is reduced to a SDP program.

%\subsection{Duality point of view of the original problem}
%Minimizing the peak power can be seen as the minimization of the $L_\infty$ norm if we consider the problem in a suitable functional analysis context. More precisely, the solution $v$ of the equation:
%$$
%M(a)v''+K(a)v=f'
%$$
%makes sense if we place $f$ in a good functional space. In the case of finite number of harmonics, $f$ is $C^\infty$ so this problem can be postponed. And the original problem of minimizing peak power is infact:
%\begin{equation}\label{eqn:l infty norm minimization}
    %\begin{array}{cc}
        %\underset{a,v}{\min} & ||f^Tv||_{\infty}\\
         %\text{ s.t. }& a\geq 0, a^Tq,M(a)\succeq 0, K(a)\succeq0\\
         %&M(a)v''+K(a)v=f' 
    %\end{array} 
%\end{equation}
%This is very hard because of the $||.||_\infty$ norm, so here we try to introduce a dual point of view of the problem (\ref{eqn:l infty norm minimization}) and we will try to see if something can be simplified. In order to do so, we define the Legendre-Fenchel transform (or the conjugate function) of a function, even though, this is more usual for a convex function but here nothing is told about $a\mapsto C(a)=||f^Tv||_\infty$.
%\begin{definition}[Legrendre-Fenchel Transform]
%For a function $f$, for whenever it exists finite value, we define its Legrendre-Fenchel transform or conjugate function $f^*$ as:
%$$
%f^*(x)=\underset{y}{\max}~x^Ty-f(y)
%$$
%\end{definition}
%In general, when we minimize a function $f$ with constraint $Ax\leq b$, its dual involves the conjugate of $f$ naturally. Indeed, by the Lagrangian :$$
%L(x,\mu)=f(x)+\mu^T(Ax-b)\implies g(\mu)=\underset{x}{\min}\{f(x)+(A^T\mu)^Tx\}-b^T\mu
%$$
%While we note that:$$
%\underset{x}{\min}\{f(x)+(A^T\mu)^Tx\}=-\underset{x}{\max}\{(-A^T\mu)^Tx-f(x)\}=-f^*(-A^T\mu)
%$$
%So the dual problem is :$$
    %\begin{array}{cc}
        %\underset{\mu}{\max} & g(\mu)=-f^*(-A^T\mu)-b^T\mu\\
         %\text{ s.t. }& \left\{ \begin{array}{cc}
              %\mu \geq 0  \\
              %-A^T\mu \in X^* 
         %\end{array}\right.
    %\end{array} 
%$$
%$X^*$ is the set for which the conjugate $f^*(x)$ is finite.

%Now let's try if we can find the dual of (\ref{eqn:l infty norm minimization}), for this, we need to find the conjugate $C^*$ and see whenever it's finite. The intuition behind is that each norm, seen as function, becomes its conjugate norm after taking Legendre transform, more precisely, $||.||_\infty$ becomes $||.||_1$ and $X^*$ are usually described by a bound on the $L^1$ norm which is much simplier and can be relaxed by a bound on $L^2$ norm. This relaxation is possible since $f^Tv$ is supported in $[0,2\pi]$ and since $L^1(\Omega)\subset L^2(\Omega)\subset L^{\infty}(\Omega)$ if $\Omega$ is bounded. And a $L^2$ norm bound can be (hopely) described by Fourier coefficients. This approach might help in the future if we want to consider infinite number of harmonics or something ...

%We should first transform $C$ into a suitable form:
%\begin{align}
        %C(a)=&
             %\underset{t\in[-\pi,\pi]}{\max} ~ |f(t)^Tv(t)| ~
             %\text{ s.t. }M(a)v''(t)+K(a)v(t)=f'(t), \forall t
        %\\
        %=&\underset{\lambda}{\min} ~  \lambda ~
             %\text{ s.t. } \left\{\begin{array}{c}
                %M(a)v''(t)+K(a)v(t)=f'(t), \forall t  \\
                %\lambda \geq |f(t)^Tv(t)|, \forall t
             %\end{array}\right. 
%\end{align}
%Then:\begin{align}
        %C^*(a)=&\underset{b}{\max}~a^Tb-\underset{\lambda}{\min}\{\lambda,\dots\}\\
        %&=\underset{b,\lambda}{\max}~a^Tb-\lambda ~ \text{ s.t. } \left\{\begin{array}{c}
                %M(b)v''(t)+K(b)v(t)=f'(t), \forall t  \\
                %\lambda \geq |f(t)^Tv(t)|, \forall t
             %\end{array}\right. 
%\end{align}
