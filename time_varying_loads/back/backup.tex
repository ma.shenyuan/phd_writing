Our plan to solve this problem has 2 steps:\\
\textbf {First Step}:
At this step, we try to solve the simple form of the problem, that is $M(a), K(a), f(t) \in \mathbb{R}$. With these assumptions, the matrix differential equation will become an ordinary differential equation and as a result $V(t) \in \mathbb{R}$ as well.\textbf {(Section 2)}\\ 
\textbf {Second Step}:
After completing the first step, we will try to prove the general case by taking ideas from the articles that include matrix differential equations (papers like this one introduced by Prof. Mareček:\href {https://research.ibm.com/publications/frequency-domain-methods-and-polynomial-optimization-for-optimal-periodic-control-of-linear-plants}{https://research.ibm.com/publications/frequency-domain-methods-and-polynomial-optimization-for-optimal-periodic-control-of-linear-plants})\textbf {(Section 3)}.

\section{$M(a), K(a), f(t) \in \mathbb{R}$}
In this section, we assume $M(a)$ and $K(a)$ to be real numbers and $f(t)$ to be a real function. Assumption $(vii)$ says that the function $f(t)$ is as follows:
$$f_N(t):=e_\circ + \sum_{k=1}^{N} e_{k}\cos(kt)+b_{k}\sin(kt)$$
where $e_\circ, e_1,\cdots,e_N \in \mathbb{R}$ and $b_1,\cdots,b_N \in \mathbb{R}$. So, by the differential equation in the assumption $(iv)$, $V(t)$ is as follows: 
$$V_N(t):=\sum_{k=1}^{N} \frac{k}{K(a)-k^2M(a)}(b_{k}\cos(kt)-e_{k}\sin(kt)).$$
Then, because $V(t)f(t)$ is a combination of $\sin$ functions and $\cos$ functions, so we have
$$\text{limsup}_{t \rightarrow \infty}{f(t)}^{T}V(t)=\text{Max}_{t \in [-\pi,\pi]}V(t)f(t)$$
On the other hand, by considering $\sin(t)=x$ and $\cos(t)=\sqrt{1-x^2}$ as well as considering a suitable polynomial approximation for $\sqrt{1-x^2}$ and using the following trigonometric identities:
\begin{align*}
&\sin(2t)=2 \sin t \cos t,\\
&\cos(2t)=\cos^2 t-\sin^2 t,\\
&\sin(3t)=3\sin t-4\sin^3 t,\\
&\cos(3t)=4\cos^3 t-3\cos t,
\end{align*}
it is possible to work with its polynomial approximation instead of $V(t)f(t)$. Besides this discussion, alternatively, this article \href{https://arxiv.org/pdf/1603.05905.pdf}{https://arxiv.org/pdf/1603.05905.pdf} can be employed to reach a polynomial equivalent of $V(t)f(t)$ (recommended by Prof. Mareček). Therefore, without loss of generality, we consider $V(t)f(t)$  to be polynomial. The reason for preferring to work with polynomials is that polynomials have been addressed more in scientific articles and our hands will probably be more open to solving the problem. All in all, we are dealing with the following bilevel polynomial optimization problem:
\begin{align*}
 &{\text{minimize}}_{(a_1,\dots,a_n)} C(a_1,\dots,a_n)\\
&~~~~~~~~~~q_1a_1+\dots+q_na_n \leq m\\
s.t~~&C(a_1,\dots,a_n)=\text{Max}_{t \in [-\pi,\pi]}V(t)f(t)
\tag{$*$}
\end{align*}
Two approaches to solving this problem can be imagined. They are discussed in subsections 2.1 and 2.2. In subsection 2.3, we clarify these methods with an example. 

\subsection{Approach 1}
This approach is directly dealing with the problem. First, we try to solve the Max problem (the lower-level optimization), which is a parametric POP, and then we come to the upper level, which is now several constrained optimization problems. This parametric POP paper \href{https://arxiv.org/pdf/1003.4915.pdf}{https://arxiv.org/pdf/1003.4915.pdf} was recommended by Prof. Henrion.
\paragraph{Step 1:}
get $n,m,N \in \mathbb{N}$ from input. 

\paragraph{Step 2:}
get $e_\circ, e_1,\cdots,e_N$ and $b_1,\cdots,b_N$ from input.
%%%%%%%%%%%%%
%%%%%%%%%%\
\paragraph{Step 3:}
\begin{align*}
&f_N(t):=e_\circ + \sum_{k=1}^{N} e_{k}\cos(kt)+b_{k}\sin(kt). 
\end{align*}

\
\paragraph{Step 4:}
get $M_1,M_2,\cdots,M_n \in \mathbb{R}$ from input.
\paragraph{Step 5:}
get $K_1,K_2,\cdots,K_n \in \mathbb{R}$ from input.
\paragraph{Step 6:}
Consider $a_1, a_2,\dots, a_n$ as variables.
\paragraph{Step 7:}
$$a:=(a_1,\dots ,a_n).$$
\paragraph{Step 8:}
$$M(a):=a_1M_1+\cdots +a_nM_n.$$
\paragraph{Step 9:}
$$K(a):=a_1K_1+\cdots +a_nK_n.$$
\paragraph{Step 10:}
get $q_1,q_2,\cdots,q_n \in \mathbb{R}$ from input.
\paragraph{Step 11:}
Consider $C_1, C_2,\cdots, C_N$ as variables. 
\paragraph{Step 12:}
\begin{align*}
&V_N(t):=\sum_{k=1}^{N} C_k(b_{k}\cos(kt)-e_{k}\sin(kt)). 
\end{align*}
\begin{itemize}
\item[ ]
{\small{\textit{
``$V_N(t)$ is the simplest solution to the following differential equation: 
$$M(a)V^{\prime \prime}(t)+K(a)V(t)=f^{\prime}_N(t)$$
assuming $C_k:=\frac{k}{K(a)-k^2M(a)}; ~1\leq k\leq N$"
}}}
\end{itemize}
%%%%%%%%%%%%%%%%%%%%%
\paragraph{Step 13:}
determine a partition $\lbrace A_1,\cdots,A_r \rbrace$ of $\mathbb{R}^N$ and functions $P_1,\cdots,P_r$ from $\mathbb{R}^N$ to $\mathbb{R}$ such that 
$$\max_{~~~~~~t \in [-\pi,\pi]}f_N(t)V_N(t)=\begin{cases}
P_1(C_1,\cdots ,C_N) & (C_1,\cdots ,C_N) \in A_1,\\
P_2(C_1,\cdots ,C_N) & (C_1,\cdots ,C_N) \in A_2,\\
\vdots & \vdots \\
P_r(C_1,\cdots ,C_N) & (C_1,\cdots ,C_N) \in
A_r,
\end{cases}$$

\paragraph{Step 14:}
For every $i \in \lbrace1,\cdots,r\rbrace$, determine a solution $(a_1,\cdots ,a_n) \in \mathbb{R}^n $ of the following system and call it 
$(a^i_1,\cdots ,a^i_n)$:
$$\min ~ P_i(C_1,\cdots ,C_N)$$
%%%%%%%%%%%%%%%%%\
such that:
\begin{align*}
& q_1a_1+\cdots+ q_na_n \leq m,\\
&(C_1,\cdots ,C_N) \in A_i,\\
&C_k:=\frac{k}{K(a)-k^2M(a)}; ~1\leq k\leq N.
\end{align*}

\paragraph{Step 15:}
determine $j \in \lbrace1,\cdots,r\rbrace$ such that 
 \begin{align*} 
 P_j(\frac{1}{K(a)-M(a)},\cdots ,\frac{N}{K(a)-N^2M(a)}) \big|_{(a^j_1,\cdots ,a^j_n)}= \min \lbrace & P_i(\frac{1}{K(a)-M(a)},\cdots ,\frac{N}{K(a)-N^2M(a)}) \big|_{(a^i_1,\cdots ,a^i_n)}\big|\\
 &~i \in \lbrace1,\cdots,r\rbrace\rbrace.
\end{align*}
\paragraph{Step 16:}
give $(a^j_1,\cdots ,a^j_n)$ as output.
\\
\\
\textbf {Discussion 1}\\
It seems steps 9 and 10 are challenging. Can the algorithm be put into practice?\\
\\
\textbf {Discussion 2}\\
We say that $f(t):=e_\circ + \sum_{k=1}^{N} e_{k}\cos(kt)+b_{k}\sin(kt)$ is K-spars whenever $K=card\lbrace k: (e_k,b_k)\neq(0,0)\rbrace$. It should be mentioned that for 1-sparse $f(t)$’s we have a solution that can be put into practice by Matlab.
\subsection{Approach 2 }
In this approach, we search in bi-level optimization articles, maybe we can find a type of problem that will be helping in solving the problem (*).
 
\subsection{Clarification of the approaches }
\section{$M(a), K(a)$ are matrices and $f(t)$ is a vector function }
