import numpy as np
import numpy.linalg as npl
import numpy.random as npr

from matplotlib import pyplot as plt
np.set_printoptions(precision=3)

N = 2
dim = 4*N+1


# ~ compute w
# ~ we "wrap" w vector as w0 w1 w2 w-2 w-1
w = np.zeros(dim)
w[:dim//2+1]=1/np.arange(1,2*N+2)[::-1]
w[dim:dim//2:-1]=w[1:dim//2+1]

# ~ compute Lambda
L = np.zeros([dim, 2*N+1, 2*N+1])

L[0,:,:]=np.eye(2*N+1)

for i in range(1,dim//2+1):
	L[i,:,:]=np.diag(np.ones(2*N+1-i),-i)
	L[-i,:,:]=L[i,:,:].T

# ~ compute Theta*w
# ~ => compute L*w (element wise) => DFT(L*w)
ThetaW = np.fft.fft(L*w[:,None,None], axis=0)
hatw = np.fft.fft(w)
Theta= np.fft.fft(L, axis=0)

eigT = npl.eigvals(ThetaW[0,:,:])
lam = np.min(eigT.real)
print(lam)

# ~ plt.figure()
# ~ plt.stem(eigT.real)
# ~ plt.show()

z = np.zeros(2*N+1, dtype='F')
q = np.zeros(dim, dtype='F')

z.real = 1-2*npr.random(2*N+1)
z.imag = 1-2*npr.random(2*N+1)

z /= npl.norm(z)
zH = np.conjugate(z)

for i in range(dim):
	q[i] = np.conjugate(z).T.dot(ThetaW[i,:,:]@z)-lam

print(q.real)
