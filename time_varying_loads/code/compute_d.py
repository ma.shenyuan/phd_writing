import numpy as np
import numpy.linalg as npl
from matplotlib import pyplot as plt
np.set_printoptions(precision=3)

N = 2
P = 2
dim = 2*(N+P)+1

d = np.zeros(dim, 'F')
d[1:N+1] = np.arange(1,N+1)
d[dim-N:]= d[N:0:-1]
d[1:N+1]*= 1j
d[dim-N:]*= -1j

dhat = np.fft.fft(d).real
D = np.zeros((dim,dim))

print(dhat)
for i in range(dim):
	D[i,:]=np.roll(dhat,i)
	
