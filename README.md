# Phd_writing

This is the repo to store all my phd writing. This repo will be composed mainly by .tex files and if necessary some simple scripts to generate image or data.

When a new writing project is created, there will be a makefile inside of each project to help you to compile the pdf with pdflatex.
